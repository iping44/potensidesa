-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 20, 2017 at 06:01 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_potensidesa`
--

-- --------------------------------------------------------

--
-- Table structure for table `industri_table_kabupaten`
--

CREATE TABLE IF NOT EXISTS `industri_table_kabupaten` (
  `id_kabupaten` int(10) NOT NULL AUTO_INCREMENT,
  `nama_kabupaten` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_kabupaten`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `industri_table_kabupaten`
--

INSERT INTO `industri_table_kabupaten` (`id_kabupaten`, `nama_kabupaten`, `keterangan`) VALUES
(1, 'Provinsi', 'Provinsi'),
(2, 'Kendari', 'Kota Kendari'),
(3, 'Konawe Selatan', 'Kab. Konawe Selatan'),
(5, 'Buton', 'Kab. Buton'),
(6, 'Bau-bau', 'Kota Bau-bau'),
(7, 'Wakatobi', 'Kab. Wakatobi'),
(8, 'Muna', 'Kab. Muna'),
(9, 'Bombana', 'Kab. Bombana'),
(10, 'Buton Utara', 'Kab. Buton Utara'),
(11, 'Konawe', 'Kab. Konawe'),
(12, 'Konawe Utara', 'Kab. Konawe Utara'),
(13, 'Kolaka', 'Kab. Kolaka'),
(14, 'Kolaka Utara', 'Kab. Kolaka Utara'),
(15, 'Kolaka Timur', 'Kab. Kolaka Timur'),
(16, 'Konawe Kepulauan', 'Kab. Konawe Kepulauan');

-- --------------------------------------------------------

--
-- Table structure for table `table_about`
--

CREATE TABLE IF NOT EXISTS `table_about` (
  `id_about` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(50) NOT NULL,
  PRIMARY KEY (`id_about`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `table_about`
--

INSERT INTO `table_about` (`id_about`, `judul`, `isi`, `gambar`) VALUES
(4, 'Profil BAPPEDA Kab. Kolaka Timur', '<p><strong><span style="font-size:12px"><span style="font-size:14px">Tugas</span>&nbsp;&nbsp;</span></strong></p>\r\n\r\n<p style="margin-left:21.3pt">a. Menyusun kebijakan teknis, rencana, dan program, kelitbangan di pemerintahan provinsi dan pemerintahan kabupaten/kota di wilayahnya;</p>\r\n\r\n<p style="margin-left:21.3pt">b. Melaksanakan kelitbangan di pemerintahan provinsi;</p>\r\n\r\n<p style="margin-left:21.3pt">c. Melaksanakan pengkajian kebijakan lingkup urusan pemerintahan daerah;</p>\r\n\r\n<p style="margin-left:21.3pt">d. Melaksanakan fasilitasi dan melakukan inovasi daerah;</p>\r\n\r\n<p style="margin-left:21.3pt">e. Melakukan pemantauan, evaluasi dan pelaporan atas pelaksanaan kelitbangan;</p>\r\n\r\n<p style="margin-left:21.3pt">f. Melakukan koordinasi dan sinkronisasi pelaksanaan kelitbangan di pemerintahan provinsi, dan pemerintahan kabupaten/kota;</p>\r\n\r\n<p style="margin-left:21.3pt">g. Melakukan pembinaan kepada perangkat daerah pelaksana kelitbangan kabupaten/kota;</p>\r\n\r\n<p style="margin-left:21.3pt">h. Memberikan rekomendasi regulasi dan kebijakan kepada Gubernur dan perangkat daerah provinsi;</p>\r\n\r\n<p style="margin-left:21.3pt">i. Memastikan tersusunnya kebijakan dan/atau regulasi berbasis hasil kelitbangan di provinsi;</p>\r\n\r\n<p style="margin-left:21.3pt">j. Melaksanakan administrasi kelitbangan;</p>\r\n\r\n<p style="margin-left:21.3pt">&nbsp;k. Melaksanakan tugas lain yang diberikan oleh gubernur.</p>\r\n\r\n<h1><strong><span style="font-size:14px">Fungsi</span></strong></h1>\r\n\r\n<p style="margin-left:21.3pt">Berdasarkan Peraturan Menteri Dalam Negeri No.17 Tahun 2016 menyelenggarakan fungsi kelitbangan sebagai lembaga yang berwenang dan bertanggung jawab atas kelitbangan pemerintahan dalam negeri di Provinsi. Kelitbangan pemerintahan dimaksud meliputi kegiatan penelitian, pengembangan, pengkajian, penerapan, perekayasaan, &nbsp;pengoperasian, dan evaluasi kebijakan.</p>\r\n\r\n<p><span style="font-size:14px"><strong>Visi</strong></span></p>\r\n\r\n<p style="margin-left:28.35pt"><strong>&quot;</strong><strong>MEWUJUDKAN PENELITIAN</strong><strong>&nbsp; DAN </strong><strong>PENGEMBANGAN</strong><strong> DALAM</strong><strong> MENUNJANG </strong><strong>PENCAPAIAN </strong><strong>SULAWESI TENGGARA YANG SEJAHTERA, MANDIRI DAN BERDAYA SAING&nbsp;</strong><strong>TAHUN 2013-2018&rdquo;</strong></p>\r\n\r\n<p><span style="font-size:14px"><strong>Misi</strong></span></p>\r\n\r\n<p style="margin-left:28.35pt">Dalam rangka mewujudkan Visi Badan Penelitian dan Pengembangan Provinsi Sulawesi Tenggara, maka dirumuskan dan ditetapkanlah suatu Misi berdasarkan dengan tugas pokok dan fungsi Balitbang sebagai berikut :</p>\r\n\r\n<ol style="list-style-type:lower-alpha">\r\n	<li>Mendorong dan melakukan kegiatan penelitian dan pengembangan implementasi prioritas pembangunan di Sulawesi Tenggara;</li>\r\n	<li>Meningkatkan kuntitas dan kualitas sumber daya manusia bidang Iptek dan sarana prasarana Iptek;</li>\r\n	<li>Memanfaatkan fungsi koordinasi dan kerjasama kegiatan penelitian dan kajian pengembangan dalam lingkup Pemerintah Daerah dan pihak lainnya;&nbsp;</li>\r\n	<li>Memfasilitasi kegiatan penelitian dan kajian pengembangan kebijakan prioritas pembangunan di Sulawesi Tenggara;</li>\r\n</ol>\r\n\r\n<p><span style="font-size:14px"><strong>Tujuan dan Sasaran</strong></span></p>\r\n\r\n<p>Tujuan</p>\r\n\r\n<p>1) Terwujudnya sefektivitas, kualitas, dan kapasitas pelaksanaan litbang untuk perumusan kebijakan pembangunan daerah.</p>\r\n\r\n<p>2) Terwujudnya pengembangan sistem inovasi daerah dalam membangun daya saing dan kemandirian daerah.</p>\r\n\r\n<p>3) Tercapainya peningkatan kualitas sumber daya manusia dan prasarana IPTEK untuk mendorong terciptanya kemandirian dan daya saing di bidan IPTEK.</p>\r\n\r\n<p>4) Terwujudnya Balitbang sebagai pusat data dan informasi pembangunan IPTEK.</p>\r\n\r\n<p>5)&nbsp;Terciptanya koordinasi dan kerjasama dengan lembaga pemerintah, SKPD, &nbsp;Kab/Kota, &nbsp;PTN/PTS, lembaga penunjang iptek, dunia usaha, dan masyarakat dalam pelaksanaan litbang inovasi, diseminasi hasil hasil litbang.&nbsp; &nbsp; &nbsp; &nbsp;</p>\r\n\r\n<p>Sasaran</p>\r\n\r\n<p>1) Meningkatnya efektivitas, &nbsp;kualitas, dan kapasitas pelaksanaan litbang.</p>\r\n\r\n<p>2) Berkembangnya kegiatan inovasi, pelaku inovasi, dan hasil-hasil inovasi yang memiliki daya saing.</p>\r\n\r\n<p>3) Meningkatnya kompetensi SDM yang mampu mengembangkan &nbsp;dan mendaya gunakan hasil-hasil litbang untuk pengembangan iptek di daerah.</p>\r\n\r\n<p>4) Tersedianya sistem data dan informasi hasil-hasil riset/ litbang &nbsp;yang menunjang pembangunan iptek dan inovasi daerah.</p>\r\n\r\n<p>5) Meningkatnya koordinasi dan kerjasama dengan lembaga pemerintah, SKPD, &nbsp;Kab/Kota, PTN/ PTS, lembaga penunjang iptek, dunia usaha, dan masyarakat dalam pelaksanaan Litbang, inovasi, &nbsp;diseminasi hasil-hasil Litbang.</p>\r\n\r\n<p><strong>Strategi Kebijakan</strong></p>\r\n\r\n<p>Strategi<br />\r\n1) Menata sistem penentuan topik litbang yang akan dilaksanakan guna memperoleh kegiatan litbang prioritas secara lebih selektif.</p>\r\n\r\n<p>2)&nbsp;Memperkuat &nbsp;road map dan dokumen kebijakan SIDa, penguatan kelembagaan, dan integrasi SIDa dengan kebijakan RPJMD.</p>\r\n\r\n<p>3)&nbsp;Mengikutsertakan aparatur pada diklat teknis/ fungsional, rakor/seminar/ sosialisasi kegiatan teknis dan Penyediaan prasarana iptek.</p>\r\n\r\n<p>4) Menata pengelolaan, penyajian, dan pendayagunaan data dan informasi hasil-hasil riset/ litbang untuk pembangunan iptek.</p>\r\n\r\n<p>5) Mendorong kerjasama dengan instansi terkait terutama dalam upaya pemecahan masalah &nbsp;melalui pemanfaatan hasil-hasil Litbang.</p>\r\n\r\n<p>Kebijakan</p>\r\n\r\n<p>1) Melaksanakan kegiatan litbang yang berkaitan dengan isu-isu aktual program strategis Pemerintah Daerah.</p>\r\n\r\n<p>2) Meningkatkan kegiatan inovasi daerah secara terpadu, berkelanjutan melibatkan seluruh stakeholders terkait.</p>\r\n\r\n<p>3)&nbsp;Meningkatkan pembinaan dan pengembangan kompetensi SDM serta pemenuhan prasarana iptek.</p>\r\n\r\n<p>4)&nbsp;Meningkatkan pendayagunaan perpustakaan, website dan IT, jurnal publikasi, dan layanan izin penelitian.</p>\r\n\r\n<p>5)&nbsp;Membangun jejaring Kerjasama kelembagaan dengan pihak-pihak terkait untuk meningkatkan kesefahaman dan menciptakan sinergi dalam kegiatan Litbang.</p>\r\n\r\n<h1>&nbsp;</h1>\r\n', 'Informasi_organisasi_kelembagaan_Badan_Penelitian_');

-- --------------------------------------------------------

--
-- Table structure for table `table_agama`
--

CREATE TABLE IF NOT EXISTS `table_agama` (
  `desa_id` int(11) NOT NULL,
  `agama_id` int(11) NOT NULL AUTO_INCREMENT,
  `islam` int(11) NOT NULL,
  `kristen` int(11) NOT NULL,
  `hindu` int(11) NOT NULL,
  `budha` int(11) NOT NULL,
  `katolik` int(11) NOT NULL,
  `lain` int(11) NOT NULL,
  PRIMARY KEY (`agama_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `table_agama`
--

INSERT INTO `table_agama` (`desa_id`, `agama_id`, `islam`, `kristen`, `hindu`, `budha`, `katolik`, `lain`) VALUES
(2, 2, 11, 11, 12, 34, 11, 54),
(0, 4, 12, 323, 112, 121, 1212, 121),
(2, 5, 2132, 12321, 434, 432, 34, 3243);

-- --------------------------------------------------------

--
-- Table structure for table `table_agenda`
--

CREATE TABLE IF NOT EXISTS `table_agenda` (
  `id_agenda` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenisagenda` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `tanggal` date NOT NULL,
  `kegiatan` text NOT NULL,
  `tempat` varchar(200) NOT NULL,
  `download` text NOT NULL,
  `post_date` date NOT NULL,
  PRIMARY KEY (`id_agenda`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `table_agenda`
--

INSERT INTO `table_agenda` (`id_agenda`, `id_jenisagenda`, `judul`, `tanggal`, `kegiatan`, `tempat`, `download`, `post_date`) VALUES
(1, 1, 'Pelatihan Penelitian Pemula', '2017-02-15', 'Deskripsi', 'Plaza Inn Kendari', 'Pelatihan_Penelitian_Pemula.pdf', '2017-02-13'),
(2, 8, 'TUPOKSI UPTB Pusat Peraga IPTEK', '2017-02-01', '<p><strong>A. KEDUDUKAN, TUGAS, DAN FUNGSI </strong></p>\r\n\r\n<ol>\r\n	<li>Unit Pelaksana Teknis Badan Pusat Peragaan Ilmu Pengetahuan dan Teknologi yang selanjutnya disebut UPTB PP-IPTEK adalah unsur pelaksana teknis layanan umum di bidang ilmu pengetahuan dan teknologi yang berada di bawah dan bertanggung jawab kepada Kepala Badan Penelitian dan Pengembangan Provinsi Sulawesi Tenggara.</li>\r\n	<li>Unit Pelaksana Teknis Badan Pusat Peragaan Iptek (UPTB PP-IPTEK) Badan Penelitian dan Pengembangan Provinsi Sulawesi Tenggara mempunyai tugas melaksanakan pengelolaan di bidang peragaan dan program ilmu pengetahuan dan teknologi dalam rangka pemasyarakatan dan pembudayaan ilmu pengetahuan dan teknologi di Sulawesi Tenggara.</li>\r\n	<li>Dalam melaksanakan tugas, UPTB PP-IPTEK menyelenggarakan fungsi :\r\n	<ol style="list-style-type:lower-alpha">\r\n		<li>penyusunan arah kebijakan umum pengembangan pusat peragaan ilmu pengetahuan dan teknologi;</li>\r\n		<li>penyusunan kebijakan teknis peragaan ilmu pengetahuan dan teknologi;</li>\r\n		<li>penyusunan perencanaan program dan anggaran peragaan ilmu pengetahuan dan teknologi;</li>\r\n		<li>pelaksanaan operasional kegiatan peragaan ilmu pengetahuan dan teknologi;</li>\r\n		<li>pelaksanaan fasilitasi dan pelayanan umum peragaan ilmu pengetahuan dan teknologi;</li>\r\n		<li>penyebarluasan informasi dan pembudayaan peragaan ilmu pengetahuan dan teknologi;</li>\r\n		<li>pemantauan, evaluasi dan pelaporan atas pelaksanaan peragaan ilmu pengetahuan dan teknologi;</li>\r\n		<li>koordinasi dan harmonisasi pelaksanaan peragaan ilmu pengetahuan dan teknologi;</li>\r\n		<li>pelaksanaan pengembangan kapasitas kelembagaan;</li>\r\n		<li>pelaksanaan pengelolaan dan dukungan administrasi;</li>\r\n		<li>pelaksanaan fungsi lain yang diberikan oleh Kepala Badan terkait dengan tugas dan fungsi UPTB.</li>\r\n	</ol>\r\n	</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>B. SUSUNAN ORGANISASI </strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<ol>\r\n	<li>Susunan organisasi UPTB PP-IPTEK terdiri atas:\r\n	<ol style="list-style-type:lower-alpha">\r\n		<li>Kepala UPTB;</li>\r\n		<li>Sub Bagian Administrasi</li>\r\n		<li>Seksi Peragaan dan Program Pendidikan;</li>\r\n		<li>Seksi Informasi dan Kerjasama.</li>\r\n		<li>Kelompok Jabatan Fungsional</li>\r\n	</ol>\r\n	</li>\r\n	<li>Sub Bagian Administrasi, Seksi Peragaan dan Program Pendidikan, Seksi Informasi dan Kerjasama, dan Kelompok Jabatan Fungsional berada di bawah dan bertanggung jawab kepada Kepala UPTB.</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>C. TUGAS DAN FUNGSI </strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>1) KEPALA UPTB</strong></p>\r\n\r\n<p>Kepala UPTB PP-IPTEK mempunyai tugas memimpin, membina, mengkoordinasikan, merencanakan, serta menetapkan program kerja, tata kerja dan mengembangkan semua kegiatan berdasarkan fungsi teknis dibidang peragaan dan program ilmu pengetahuan dan teknologi dalam rangka pemasyarakatan dan pembudayaan ilmu pengetahuan dan teknologi sesuai dengan peraturan perundang-undangan yang berlaku serta bertanggung jawab atas terlaksananya tugas dan fungsi UPTB.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>2) SUB BAGIAN ADMINISTRASI</strong></p>\r\n\r\n<p><strong>a. Tugas</strong></p>\r\n\r\n<ol>\r\n	<li>Sub Bagian Administrasi mempunyai tugas melaksanakan perencanaan, monitoring dan evaluasi pengelolaan sarana/fasilitas, serta urusan keuangan, perlengkapan, kepegawaian, kerumahtanggaan dan ketatausahaan di lingkungan UPTB.</li>\r\n	<li>Sub Bagian Administrasi dipimpin oleh Kepala Sub Bagian yang berada di bawah dan bertanggung jawab kepada Kepala UPTB.</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p><strong>b. Fungsi</strong></p>\r\n\r\n<p>Dalam melaksanakan tugasnya, Sub Bagian Administrasi menyelenggarakan fungsi:</p>\r\n\r\n<ol>\r\n	<li>koordinasi penyusunan rencana program dan anggaran;</li>\r\n	<li>pengelolaan urusan keuangan, perbendaharaan, akuntansi dan penyusunan laporan keuangan;</li>\r\n	<li>pengelolaan ketatausahaan, pelaksanaan kerumahtanggaan, perlengkapan dan pengelolaan aset;</li>\r\n	<li>pengelolaan administrasi kepegawaian dan pembinaan jabatan fungsional, serta evaluasi kinerja Pegawai Aparatur Sipil Negara;</li>\r\n	<li>pelaksanaan pengelolaan pendapatan dan belanja;</li>\r\n	<li>pelaksanaan penyusunan kebijakan pengelolaan barang, asset tetap dan investasi;</li>\r\n	<li>pelaksanaan sistem informasi manajemen keuangan; dan</li>\r\n	<li>pelaksanaan fungsi lain yang diberikan oleh kepala badan sesuai dengan tugas dan fungsinya.</li>\r\n</ol>\r\n\r\n<p style="margin-left:28.35pt">&nbsp;</p>\r\n\r\n<p><strong>3) SEKSI PERAGAAN DAN PROGRAM PENDIDIKAN </strong></p>\r\n\r\n<p><strong>a. Tugas</strong></p>\r\n\r\n<ol>\r\n	<li>Seksi Peragaan dan Program Pendidikan mempunyai tugas:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (i) melakukan penyiapan bahan penyusunan rencana dan pelaksanaan kegiatan operasional peragaan di lingkungan UPTB PP-IPTEK, peragaan ilmu pengetahuan dan teknologi keliling, kepemanduan, pemeliharaan dan perbaikan alat peraga, serta melakukan monitoring, evaluasi dan penyusunan laporan kegiatan teknis operasional peragaan ilmu pengetahuan dan teknologi; (ii) melakukan penyiapan bahan penyusunan rencana dan melakukan kegiatan operasional program dan pengelolaan perpustakaan, serta melakukan monitoring, evaluasi dan penyusunan laporan kegiatan teknis operasional program.</li>\r\n	<li>Seksi Peragaan dan Program Pendidikan dipimpin oleh Kepala Seksi yang berada di bawah dan bertanggung jawab kepada Kepala UPTB.</li>\r\n</ol>\r\n\r\n<p><strong>b. Fungsi</strong></p>\r\n\r\n<p>Dalam melaksanakan tugasnya, Seksi Peragaan dan Program Pendidikan menyelenggarakan fungsi:</p>\r\n\r\n<ol>\r\n	<li>penyusunan kebijakan teknis, program, dan anggaran peragaan dan program pendidikan;</li>\r\n	<li>penyusunan rencana kegiatan teknis operasional peragaan ilmu pengetahuan dan teknologi;</li>\r\n	<li>penyusunan rencana kegiatan teknis operasional program pendidikan;</li>\r\n	<li>pelaksanaan kegiatan teknis operasional peragaan ilmu pengetahuan dan teknologi yang mengacu kepada Rencana Kegiatan dan Anggaran;</li>\r\n	<li>pelaksanaan kegiatan teknis operasional program pendidikan yang mengacu kepada Rencana Kegiatan dan Anggaran;</li>\r\n	<li>pelaksanaan kegiatan peragaan ilmu pengetahuan dan teknologi keliling;</li>\r\n	<li>pelaksanaan pemanduan &nbsp;penggunaan alat peraga ilmu pengetahuan dan teknologi;</li>\r\n	<li>pelaksanaan pemeliharaan dan perbaikan alat peraga ilmu pengetahuan dan teknologi;</li>\r\n	<li>pelaksanaan pengelolaan perpustakaan;</li>\r\n	<li>pelaksanaan koordinasi dan harmonisasi kegiatan peragaan dengan program pendidikan;</li>\r\n	<li>pelaksanaan perintisan dan fasilitasi pembangunan peragaan Iptek di Kabupaten/Kota bekerja sama dengan institusi terkait;</li>\r\n	<li>pelaksanaan monitoring, evaluasi dan penyusunan laporan kegiatan teknis operasional; dan</li>\r\n	<li>pelaksanaan fungsi lain yang diberikan oleh kepala badan sesuai dengan tugas dan fungsinya.</li>\r\n</ol>\r\n\r\n<p><strong>4) SEKSI INFORMASI DAN KERJASAMA</strong></p>\r\n\r\n<p><strong>a. Tugas</strong></p>\r\n\r\n<ol>\r\n	<li>Seksi Informasi dan Kerjasama mempunyai tugas melakukan penyiapan bahan penyusunan rencana dan pelaksanaan kegiatan informasi/promosi dan kerjasama, pelayanan pengunjung, kehumasan, perintisan dan fasilitasi pembangunan Peragaan Iptek di Kabupaten/Kota dan hubungan kerjasama dengan pihak terkait untuk pengembangan kelembagaan yang meliputi alat peraga, program, pelatihan personil dan pertukaran tenaga ahli, serta pelaksanaan monitoring, evaluasi dan penyusunan laporan kegiatan teknis informasi/promosi dan kerjasama.</li>\r\n	<li>Seksi Informasi dan Kerjasama dipimpin oleh Kepala Seksi yang berada di bawah dan bertanggung jawab kepada Kepala UPTB.</li>\r\n</ol>\r\n\r\n<p><strong>b. Fungsi</strong></p>\r\n\r\n<p>Dalam melaksanakan tugasnya, Seksi Informasi dan Kerjasama menyelenggarakan fungsi:</p>\r\n\r\n<ol>\r\n	<li>penyusunan kebijakan teknis, program, dan anggaran promosi dan kerjasama;</li>\r\n	<li>penyusunan rencana kegiatan informasi atau promosi dan kerjasama;</li>\r\n	<li>fasilitasi dan pelayanan pengunjung;</li>\r\n	<li>pelaksanaan kegiatan teknis operasional promosi dan kerjasama yang mengacu kepada Rencana Kegiatan dan Anggaran;</li>\r\n	<li>pelaksanaan koordinasi dan harmonisasi kegiatan promosi dan kerjasama;</li>\r\n	<li>pengelolaan, penyebarluasan informasi dan pembudayaan peragaan ilmu pengetahuan dan teknologi;</li>\r\n	<li>pelaksanaan kerjasama perintisan dan fasilitasi pembangunan peragaan Iptek di Kabupaten dan Kota;</li>\r\n	<li>pelaksanaan kerjasama dengan pihak terkait untuk pengembangan kelembagaan yang meliputi alat peraga, program, pelatihan personil dan pertukaran tenaga ahli;</li>\r\n	<li>pelaksanaan monitoring, evaluasi dan penyusunan laporan kegiatan teknis penyebarluasan informasi/promosi dan kerjasama; &nbsp;dan</li>\r\n	<li>pelaksanaan fungsi lain yang diberikan oleh kepala badan sesuai dengan tugas dan fungsinya.</li>\r\n</ol>\r\n', 'UPTB PP IPTEK Balitbang Prov.Sultra', '', '2017-02-28');

-- --------------------------------------------------------

--
-- Table structure for table `table_banner`
--

CREATE TABLE IF NOT EXISTS `table_banner` (
  `id_banner` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(100) NOT NULL,
  `download` text NOT NULL,
  PRIMARY KEY (`id_banner`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `table_banner`
--

INSERT INTO `table_banner` (`id_banner`, `judul`, `download`) VALUES
(1, 'Jurnal', 'Jurnal.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `table_berita`
--

CREATE TABLE IF NOT EXISTS `table_berita` (
  `id_berita` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` text NOT NULL,
  `judul` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `tanggal` date NOT NULL,
  `kategori` varchar(50) NOT NULL,
  PRIMARY KEY (`id_berita`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `table_berita`
--

INSERT INTO `table_berita` (`id_berita`, `gambar`, `judul`, `content`, `tanggal`, `kategori`) VALUES
(7, 'Usaha_Mete.png', 'Cita Rasa Unik Mete Kualitas Super', 'Siapa yang tidak kenal dengan oleh-oleh khas Sulawesi Tenggara yang satu ini. Rasanya yang gurih dan mampu membuat lidah tidak berhenti untuk mengecap rasanya yang lezat. Produk khas lokal ini menjadi salah satu andalan komoditi lokal yang selalu dibanggakan karena terkenal dengan kualitas super dan cita rasanya yang unik dan berbeda dari produk mete asal daerah lain di Indonesia. Dapat dijumpai diberbagai toko oleh-oleh khas Sulawesi Tenggara ataupun dapat langsung mengunjungi tempat usaha UMKM pengolahan mete di kendari.', '2017-02-10', 'Promosi UMKM'),
(8, 'Lokakarya_Kelitbangan_Tahun_2017.JPG', 'Lokakarya Kelitbangan Tahun 2017', '<p>Lokakarya Kelitbangan menjadi agenda pembuka awal tahun 2017 Badan Penelitian dan Pengembangan Provinsi Sulawesi Tenggara. Acara yang diadakan pada tanggal 31 Januari-01 Februari 2017 mengetengahkan tema pembahasan mengenai arah kelitbangan merujuk Permendagri No.17 Tahun 2016. Seminar Lokakarya yang dihadiri kurang lebih 50 peserta yang terdiri dari perwakilan litbang kota/kabupaten, SKPD terkait (stakeholder), Dewan Riset Daerah, Peneliti dan calon Peneliti memberikan ruang untuk mengamati dan mengkaji arah kelitbangan yang nantinya disepakati menjadi fokus kegiatan kelitbangan selama setahun kedepan. Acara ini menjadi agenda rutin tahunan litbang sebelum dimulainya kegiatan penelitian dan pengembangan di awal tahun. Acara yang berlangsung selama 2 hari di hotel Horison ini dibuka oleh Sekda Prov.Sultra Dr.H.Lukman Abunawas,SH,M.Si. Memberikan kata sambutan dengan mengharapkan eksistensi litbang untuk mendukung Sistem Inovasi Daerah dalam upaya memajukan perekonomian Sultra, beliau juga memberikan apresiasi terhadap pencapaian nyata kinerja litbang selama 9 tahun terakhir keberadaannya. Acara yang dirangkaikan dengan peluncuran perdana Jurnal Formasi yang menjadi salah satu kapasitas publikasi maupun diseminasi produk-produk ilmu pengetahuan dan teknologi serta inovasi daerah. Produk-produk ilmu pengetahuan dan teknologi serta inovasi daerah dimaksud diharapkan dapat bermanfaat sebagai input perumusan kebijakan (<em>Policy Based Research)</em>, pengembangan program maupun kegiatan pembangunan daerah, &nbsp;penguatan inovasi sektor publik (<em>New Public Management</em>) serta menjadi referensi pelaksanaan kegiatan-kegiatan praktis oleh pihak-pihak yang berkepentingan (<em>Stakeholder</em>) baik pemerintah daerah, perguruan tinggi, dunia usaha, maupun masyarakat. Pada Sesi ke II juga dilanjutkan dengan tanya jawab dan diskusi mengenai prosedur pelayanan terpadu yang dilaksanakan oleh Balitbang Prov.Sultra dengan peserta perwakilan litbang kota/kabupaten bersama&nbsp;stakeholder terkait. Bertindak sebagai narasumber Kepala Balitbang Prov.Sultra Ir.Sukanto Toding,M.SP,M.A.</p>\r\n\r\n<p><a href="http://balitbang.sulawesitenggaraprov.go.id/asset/uploads/files/Matriks%20%26%20Pelaku%20Kegiatan%20Kelitbangan.zip">http://balitbang.sulawesitenggaraprov.go.id/asset/uploads/files/Matriks%20%26%20Pelaku%20Kegiatan%20Kelitbangan.zip</a></p>\r\n\r\n<p><a href="http://balitbang.sulawesitenggaraprov.go.id/asset/uploads/files/TELAAHAN%20PERMENDAGRI%2017%202016.pptx">http://balitbang.sulawesitenggaraprov.go.id/asset/uploads/files/TELAAHAN%20PERMENDAGRI%2017%202016.pptx</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-02-16', 'Berita'),
(11, 'Seminar_Awal_(Sidang_TPM_I)_.jpg', 'Seminar Awal (Sidang TPM I) ', '<p>Agenda seminar awal (Sidang TPM I) dilaksanakan dengan menyajikan sekitar 25 judul penelitian untuk pemaparan peneliti tiap judul proposal yang dibawakan mengutarakan penjelasan mengenai arah tujuan dan sasaran penelitian yang akan dicapai sebagaimana termuat nantinya dalam laporan hasil penelitian. Standar kualitas mutu penelitian yang cukup tinggi diberikan oleh&nbsp;Tim Pengendali Mutu (TPM) terhadap hasil penelitian yang diharapkan kedepannya dapat menjadi sumber acuan dalam penyusunan kebijakan daerah.</p>\r\n', '2017-02-21', 'Berita'),
(12, 'Knowledge_Management_-_Strategi_Penguatan_Eksisten.jpg', 'Knowledge Management - Strategi Penguatan Eksisten', '<p><em>&ldquo;Perusahaan yang hebat tidak percaya pada kehebatan, tetapi percaya pada perbaikan dan perubahan yang berkesinambungan&rdquo;(Tom Peters)</em>. Kutipan kata-kata bijak yang diambil dari seorang pakar management berbagai perusahaan top dunia itu mungkin terdengar tidak asing lagi bagi para pengembang bisnis dan usaha. Mengkaji kutipan kata-kata bijak tersebut sedikit dapat memberi masukan yang cukup berharga mengenai strategi kerja bagi para pegawai negeri maupun karyawan swasta, disamping dapat pula menjadi bahan rujukan bagi para pemimpin lembaga ataupun perusahaan dalam pengambilan kebijakan strategi pencapaian kinerja lembaga/perusahaan kedepan. Kehebatan sumber daya manusia ataupun sarana dan prasarana pendukungnya tidak cukup menjadikan suatu lembaga atau perusahaan dapat maju dan bertahan dalam waktu yang lama. Kehebatan tersebut tidak akan berarti apapun jika tidak dibarengi dengan strategi <em>management</em> yang mumpuni untuk dapat mengarahkan dan mengatur serta mengelola faktor kehebatan itu secara maksimal. Dalam memahami dari sistem yang mampu diciptakan untuk dapat mengarahkan, mengatur dan mengelola faktor kehebatan tersebut, perbaikan dan perubahan yang berkesinambungan menjadi faktor pendukung kuat untuk memberikan pengaruh dalam menimbulkan suatu rangkaian kegiatan yang digunakan oleh organisasi lembaga atau perusahaan untuk mengidentifikasi, menciptakan, menjelaskan dan mendistribusikan pengetahuan yang nantinya akan digunakan kembali, diketahui dan dipelajari dalam sebuah organisasi lembaga atau perusahaan, yang biasa dikenal dengan istilah <em>knowledge management</em>. Penerapan konsep <em>knowledge management</em> meliputi keseluruhan dalam pengelolaan Sumber Daya Manusia (SDM) serta sarana dan prasarana yang mendukung terutama faktor penunjang Teknologi Informasi (IT) untuk pencapaian mewujudkan kinerja organisasi lembaga dan perusahaan yang semakin baik setiap tahunnya. Sebagaimana kita ketahui bersama saat ini negara Indonesia sudah berada dalam tahap awal penerapan sistem pasar bebas ASEAN (MEA) yang pada awalnya ditandai dengan disetujuinya MOU kerjasama antar negara dalam berbagai aspek kegiatan perekonomian. Salah satunya melihat yang telah marak terjadi belakangan ini fenomena masuknya tenaga kerja asing cina tanpa melalui proses seleksi yang ketat mengenai kompetensi pekerja asing tersebut untuk bekerja di dalam negeri. Mengamati perkembangan yang terjadi sejak diberlakukannya kerja sama pasar bebas ASEAN (MEA), negara-negara berkembang yang tergabung dalam kawasan MEA kurang memberi batasan untuk dapat mengatur dan membatasi tenaga kerja asing yang datang bekerja di dalam negeri serta kurang memberi kelonggaran hak tenaga kerja lokal untuk bekerja di luar negri. Melihat keadaan tersebut membuat kita tersadar mengenai masih banyaknya tenaga kerja lokal yang mempunyai kompetensi diatas tenaga kerja asing yang dimasukkan pihak investor ke dalam negeri tapi tidak mendapat kesempatan yang layak untuk bekerja di wilayah kerjasama kawasan MEA. Fenomena itu membawa pemikiran bahwa dalam menghadapi era persaingan bebasnya tenaga kerja asing yang masuk akankah tenaga kerja lokal di Indonesia dapat mampu bertahan dengan adanya kebijakan tersebut. Permasalahan tersebut tentu dapat menambah daftar persoalaan dalam negeri perihal angka pengangguran yang setiap tahunnya belum dapat diminimalisir dengan berbagai penambahan sektor lapangan kerja baru. Mengamati berbagai fenomena perkembangan dunia kerja tersebut kompetensi bukan lagi suatu hal yang terlalu istimewa untuk dibanggakan tetapi merupakan suatu kebutuhan yang tidak bisa ditawar lagi harus ada dalam pola pikir setiap calon tenaga kerja maupun para pegawai dan karyawan yang telah terserap dalam dunia kerja untuk sekiranya dituntut mampu memberikan andil yang kuat dan berpengaruh dalam pencapaian kinerja yang berkualitas kedepannya. Jika konsep <em>knowledge management </em>dapat diterapkan dalam pola pikir sistem kinerja tersebut kemungkinan harapan akan kompetensi yang berkualitas masih ada untuk dapat dipertahankan. Sebagaimana yang telah diungkapkan oleh <em>Cut Zurnali 2008</em> istilah <em>knowledge management</em> atau manajemen pengetahuan petama kali digunakan oleh <em>Wig</em> pada tahun 1986 saat menulis buku pertamanya dengan mengangkat topik <em>Knowledge Management Foundation </em>yang diterbitkan tahun 1993. Menurut <em>Wig 1999</em> membangun manajemen pengetahuan adalah sistematis,eksplisit, dan disengaja. Pembaharuan dan penerapan pengetahuan untuk memaksimalkan efektivitas pengetahuan organisasi yang nantinya akan terbentuk menjadi aset pengetahuan organisasi yang bersifat timbal balik. Berdasarkan pemaparan tersebut manajemen pengetahuan bukan hanya terbatas pada seberapa maksimal terbentuknya pengetahuan baru sehingga terkumpulnya informasi organisasi yang pada akhirnya tersimpan dalam database tetapi yang utama adalah proses transfer pengetahuan yang dapat memberikan perubahan yang signifikan sebagai solusi dari berbagai permasalahan yang masih saja selalu membelit perkembangan suatu lembaga/organisasi maupun perusahaan. Pemahaman perbedaaan antara bentuk pengetahuan baru, informasi, dengan database menjadi kunci utama untuk memahami Manajemen Pengetahuan (MP). Transfer pengetahuan merupakan salah satu aspek manajemen pengetahuan yang bersifat timbal balik dalam pengelolaan aset pengetahuan organisasi. Sehingga dalam hal ini penerapan manajemen pengetahuan bukan hanya terbatas pada penciptaan, pengumpulan dan penyimpanan pengetahuan dan informasi dalam database tetapi harus sampai pada proses transfer ilmu informasi kepada para <em>user stakeholder</em>. Sampai saat ini konsep tersebut selalu mendapat perhatian yang luas dan dikaji lebih mendalam mengenai strategi mengubah informasi dan aset intelektual menjadi nilai bertahan suatu organisasi. Manajemen pengetahuan bukan merupakan hal yang lebih baik tapi lebih mentikberatkan pada bagaimana melakukan hal-hal yang lebih baik. Kegiatan manajemen pengetahuan (MP) biasanya dikaitkan dengan tujuan organisasi seperti bagaimana cara atau sistem untuk dapat mencapai tujuan organisasi yang lebih baik setiap tahunnya dengan peningkatan kinerja, keunggulan kompetitif ataupun tingkat yang lebih tinggi yaitu inovasi, &nbsp;sehingga mampu mempertahankan keberadaan pentingnya organisasi tersebut untuk tetap berdiri. Perubahan dan perbaikan yang berkesinambungan secara sistematis dan terarah memberikan perkembangan yang tanpa disadari dapat membawa pemikiran kepada inovasi strategi kerja yang cerdas dan aplikatif. Keberadaan suatu organisasi dinilai dari seberapa peran penting organisasi tersebut untuk memberi dampak perubahan terhadap organisasi lain dan para pengguna produk layanan organisasi tersebut. Eksistensi organisasi dapat terukur dengan sistem penerapan Manajemen Pengetahuan yang dapat memberi manfaat sebagai sarana alat komunikasi staf dengan administrator, <em>Standar Operasional Procedure</em> (SOP) yang menjadi standar acuan baku dan menjadi alat pengawasan dalam proses bekerja sehingga dapat memaksimalkan kinerja yang efektif dan efisien dalam pencapaian kinerja yang berkualitas dan meminimalisir resiko pemborosan waktu dan anggaran. Badan Penelitian dan Pengembangan Prov.Sultra dalam hal ini sebagai organisasi yang mengusung tupoksi penelitian dan pengembangan sesuai dengan arah kebijakan pemerintah dalam Permendagri UU No.17 Tahun 2016 sebagai suatu organisasi yang mengedepankan penelitian,pengembangan dan penerapan IPTEK guna dilakukan inovasi di berbagai mata rantai pertambahan nilai produk/ jasa, serta inovasi dalam menyelesaikan berbagai masalah kekinian dan mengantisipasi masalah masa depan untuk kemajuan pembangunan bangsa. Salah satu faktor penting yang mendukung kemajuan pembangunan adalah pembangunan IPTEK dalam rangka mewujudkan daya saing. Sebagai bentuk eksistensi organisasi Badan Litbang Provinsi merupakan instansi yang berfokus pada pemecahan solusi atas isu dan topik permasalahan yang terjadi melalui kegiatan penelitian dan pengembangan dengan mengusung konsep &ldquo;Rumah IPTEK&rdquo; sebagai bentuk adaptasi konsep <em>Knowledge Management. </em>Konsep tersebut dibangun untuk suatu penguatan sistem organisasi lembaga sebagai perwujudan nilai eksistensi diri dan diharapkan berujung pada perubahan sistem yang mengarah pada peningkatan nilai mutu organisasi lembaga. Dengan memberikan ruang kepada berbagai instansi SKPD Provinsi/Kota/Kabupaten untuk berpartisipasi membawa berbagai isu/topik permasalahan yang menjadi konsentrasi pelaksanaan tujuan pokok dan fungsi di SKPD tersebut, yang selanjutnya akan ditindaklanjuti oleh Badan Penelitian dan Pengembangan Prov.Sultra dengan berbagai kegiatan penelitian, pengkajian, pengembangan, perekayasaan, penerapan, pengoperasian dan evaluasi kebijakan yang pada akhirnya memberikan kontribusi hasil kegiatan kelitbangan dalam bentuk rekomendasi ke berbagai SKPD yang bersangkutan. Penerapan sistem <em>Knowledge Managament</em> yang dilakukan oleh Balitbang Prov.Sultra ini merupakan sarana perwujudan revitalisasi lembaga sebagai suatu Badan yang diharapkan nantinya bukan hanya terbatas pada fungsi utamanya sebagaimana tertuang dalam Permendagri UU No.17 Tahun 2016 tapi dapat juga berfungsi sebagai suatu lembaga pendukung dan penunjang berbagai kegiatan pemerintahan lainnya melalui kegiatan fasilitasi, advokasi, asistensi, supervisi dan edukasi sehingga kedepannya diharapkan tidak hanya mampu menempati garda terdepan pemerintahan tetapi juga mampu mengiringi jalannya sistem pemerintahan dan dapat pula memberikan dukungan dalam pencapaian hasil akhir. Oleh karenanya itu penguatan sistem kelembagaan dengan mengambil konsep <em>Knowledge Management</em> mampu memberikan sinergi yang cukup kuat jika komponen utamanya yaitu <em>people</em>, <em>knowledge</em> dan <em>processe</em> dapat diarahkan dengan strategi yang handal untuk mencapai puncak kompetensi yang berkualitas dan berorientasi mutu.</p>\r\n\r\n<p>-end-</p>\r\n\r\n<p>cindy</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-03-01', 'Artikel'),
(13, 'EKSTRIMNYA_CUACA_KOTAKU.jpg', 'EKSTRIMNYA CUACA KOTAKU', '<p>Tentu masih segar dalam ingatan kita bersama saat peristiwa hujan lebat disertai angin kencang dengan intensitas cukup besar yang mengakibatkan tumbangnya pohon-pohon besar yang sudah berusia puluhan tahun disepanjang jalan poros kota Kendari. Dalam sepanjang sejarahnya peristiwa fenomena cuaca ekstrim tersebut baru pertama kalinya terjadi di kota kendari selain banjir meluas dan merata yang terjadi hampir disebagian tempat di wilayah perkotaan kendari sekitar tahun 2013 yang lalu. Fenomena cuaca ekstrim yang hampir menyita seluruh perhatian publik dalam dan luar kota kendari mengingatkan kita pada syair lagu tentang bencana alam yang mungkin bagi kita sebagai manusia biasa harus dapat selalu melakukan introspeksi diri terhadap semua kelakuan yang secara tidak langsung berdampak pada kemarahan alam. &nbsp;Apapun itu penyebabnya secara tanggap kita harus mampu membaca segala pertanda alam yang sudah dikirimkan oleh yang maha kuasa sebagai sistem alami peringatan secara dini terhadap bahaya kemarahan alam yang tidak sedikit pastinya akan mengakibatkan kerugian materi dan bahkan sampai harus merenggut banyak korban jiwa. Tentu hal tersebut sangat disayangkan mengingat tugas dan tanggung jawab pemerintah setempat dalam kapasitas untuk melindungi kepentingan hajat hidup masyarakatnya termasuk upaya pencegahan dan penanggulangan dini bahaya bencana alam. Mungkin tidak disadari cuaca ekstrim yang sekarang ini melanda wilayah Sulawesi Tenggara khususnya kota kendari yang ditandai dengan panjangnya waktu musim penghujan. Setiap harinya tentu masyarakat kota kendari sudah dapat melakukan prediksi munculnya awan cumulonimbus yang biasa kita lihat dan kenal sebagai awan mendung dan gelap yang tiba-tiba menaungi kota kendari selepas siang hari yang terik ataupun terkadang pada pagi hari hingga sore hari dan tidak lamapun hujan akan terus mengguyur sepanjang hari yang disertai angin dan kilatan petir serta bunyi guntur terus menerus. Awan cumulonimbus merupakan awan konvektif penghasil hujan lebat yang disertai angin kencang. Penyebaran awan konvektif yang menyelimuti kawasan Sulawesi Tenggara sepanjang bulan Maret ini tidak lepas dari fenomena peningkatan secara ekstrim suhu muka laut yang memanas sebagai bagian akibat dari fenomena dinamika atmosfer secara global, kelembaban udara yang relative berkisar antara 60%-80%, serta pemicu yang paling utama berdasarkan analisa pola <em>streamline</em> yaitu pola pergerakan angin yang membawa massa udara dari samudra pasifik yang melewati wilayah Sulawesi Utara dan terjadi pola <em>shareline</em> di perairan sebelah barat Sulawesi sehingga terjadi pola konvergensi tepat diatas wilayah kendari (bagian tenggara Sulawesi), yang dapat berperan dalam pembentukan awan-awan konvektif penghasil hujan lebat disertai angin kencang. Analisa tersebut didukung dengan penampakan citra satelit yang menunjukkan pengumpulan awan cumulonimbus telah terjadi sejak tanggal 01 Maret 2017, fenomena citra satelit tersebut menunjukkan terjadinya proses penyebaran awan-awan konvektif di wilayah Kendari yang dapat di amati setiap harinya kota kendari tidak terlepas dari guyuran hujan. Seluruh faktor penyebab tersebut dapat sedikit memberi masukan bagi masyarakat di wilayah pusat penyebaran awan konvektif khususnya masyarakat kota kendari untuk selalu berjaga-jaga dan mengantisipasi kemungkinan bahaya terbesar yang dapat diakibatkan peristiwa hujan terus menerus tersebut. Pengaturan saluran air termasuk pembersihan gorong-gorong selokan, longsornya tanah perbukitan ataupun di pegunungan, sampai pada kemungkinan terjadinya kembali peristiwa tumbangnya pohon yang sudah berusia puluhan tahun disepanjang ruas jalan kota yang telah diamati bersama akibat yang ditimbulkan hingga pada kejadian hancurnya rumah penduduk, macetnya lalu lintas pada ruas jalan, dan genangan air yang tinggi disepanjang poros jalan utama di kota kendari. Mungkin uraian akibat peristiwa hujan lebat disertai angin kencang yang telah melanda daerah kita dan selalu menjadi momok bagi masyarakat kota yang pernah mendapatkan gelar Adipura tersebut mampu secepatnya dibaca untuk segera melakukan upaya berbenah diri dalam pengaturan kembali tata kota dan masyarakat sehingga sistem penanggulangan secara dini bukan hanya menjadi tanggung jawab pemerintah setempat tapi sudah harus menjadi bagian dari kebutuhan masyarakat perkotaan melihat akibat yang dapat ditimbulkan dari cuaca ekstrim yang saat ini melanda wilayah Sulawesi Tenggara khususnya kota kendari akan langsung berdampak pada kerugian secara pribadi dan memungkinkan dalam nominal yang cukup besar hingga merenggut korban jiwa.</p>\r\n\r\n<p><strong>Penulis&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp; CINDY PUSPITAFURI&nbsp; S.Si</strong></p>\r\n\r\n<p><strong>Jabatan&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp; Staff Unit Pelaksana Teknis Badan Pusat Peraga IPTEK</strong></p>\r\n\r\n<p><strong>Instansi&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :&nbsp; Badan Penelitian dan Pengembangan Provinsi Sulawesi Tenggara</strong></p>\r\n', '2017-03-27', 'Artikel'),
(14, 'PENGEMBANGAN_KAWASAN_TECHNOPARK_KOLAKA_TIMUR.jpg', 'PENGEMBANGAN KAWASAN TECHNOPARK KOLAKA TIMUR', '<p>Rencana pengembangan kawasan technopark kali ini di pusatkan pada daerah penghasil kakao di Sulawesi Tenggara. Kawasan penghasil kakao yang terkenal dengan kawasan Lembaga Ekonomi Masyarakat (LEM Sejahtera) tersebut, sebagai motor penggerak utama kegiatan produksi kakao mulai dari proses pembudidayaannya hingga proses pasca panen sampai pada menghasilkan produk olahan coklat batang siap santap (Coltim) menjadi alasan utama Pemprov Sulawesi Tenggara dalam hal ini diinisiasi oleh Balitbang Prov.Sultra memilih kawasan tersebut menjadi salah satu kawasan pengembangan Technopark Kakao di Indonesia. Konsep tersebut tidak serta merta datang begitu saja mengingat potensi kawasan daerah tersebut kini menjadi sasaran utama program pemerintah nasional dalam menggerakkan sektor bidang perkebunan kakao di Indonesia. Bagi pemerintah daerah sendiri rencana pengembangan kawasan Kolaka Timur (KOLTIM) sebagai kawasan technopark kakao menjadi solusi mendasar dari issue yang telah menjadi polemik tersendiri bagi para petani coklat yang ada disana agar pelestarian dari potensi tanaman sektor unggulan daerah tersebut selalu akan terjaga ditengah persaingan banyaknya industri besar yang mencoba masuk menanamkan investasinya dalam bidang industri&nbsp;kakao. Balitbang SULTRA memasukkan agenda tersebut dalam kegiatan utama penelitian tahun 2017 ini dengan pencapaian hasil kinerja dalam bentuk Master Plan Pengembangan Kawasan KOLTIM Technopark. Melalui kegiatan <em>Focus Group Discussion</em>&nbsp;(FGD) yang diselenggarakan bekerja sama dengan lembaga terkait diantaranya BPPT, UHO, BI, BPTP dan Pemkab Kolaka Timur. Kegiatan FGD dimaksudkan untuk dapat menyamakan persepsi serta menggalang kerja sama (MOU) dalam mengupayakan tercapainya pembentukan kawasan &quot;KOLTIM Technopark&quot; dan menjamin keberlangsungan kawasan tersebut menjadi kawasan yang selalu unggul dalam persaingan produksi kakao baik dalam skala nasional maupun internasional.</p>\r\n\r\n<p>&nbsp;</p>\r\n', '2017-05-19', 'Berita'),
(15, 'PAMERAN_HASIL_KELITBANGAN_DAERAH_(HUT_SULTRA_ke_53).jpg', 'PAMERAN HASIL KELITBANGAN DAERAH (HUT SULTRA ke 53', '<p>Tentu masih segar dalam ingatan kita meriah&nbsp;dan ramainya pergelaran peringatan HUT Sultra ke 53. Bertempat di pelataran eks-MTQ, event yang dikuti oleh seluruh perwakilan kota kabupaten di Sulawesi Tenggara serta 33 SKPD Tingkat Provinsi Sulawesi Tenggara memberi rona hiburan tersendiri bagi masyarakat Sulawesi Tenggara. Konsep peringatan HUT Sultra ke 53 dengan mengambil tema Halo Sultra secara tidak langsung memperkenalkan perkembangan&nbsp;pembangunan daerah yang terbalut dalam kekayaan adat istiadat dan budaya mengusung kearifan lokal masyarakat Sulawesi Tenggara. Balitbang Prov.Sultra sendiri mengemas konsep event pameran dengan tema &quot;Rumah IPTEK&quot;. Memberikan ciri khas unik tersendiri dengan tidak hanya membawa contoh hasil kelitbangan dalam bentuk buku ataupun gambar tapi sekaligus membawa prototipe hasil kelitbangan yang telah ada dan menjadi topik unggulan kegiatan kelitbangan di Tahun 2017 ini. Kemasan yang unik juga tidak lupa diikutkan dengan balutan kuis yang menarik bagi siswa-siswi tingkat akhir SD dan SMP untuk mengukur sejauh mana pemahaman mengenal IPTEK sedari dini. Acara kuis yang banyak menarik perhatian pengunjung di setiap malam pegelaran pameran ini membawa 2 orang siswa/siswi sebagai pemenang dan berhak membawa pulang total hadiah uang tunai sebesar 1 juta rupiah.&nbsp;</p>\r\n', '2017-05-19', 'Berita'),
(16, 'Kultum_Penyejuk_Kalbu.JPG', 'Kultum Penyejuk Kalbu', '<p>Bulan suci Ramadhan menjadikan berkah tersendiri bagi umat muslim yang menjalaninya. Diantara 11 bulan lainnya di setiap tahun hanya dalam bulan Ramadhan ini dilipatgandakannya pahala setiap ummat muslim. Bulan yang menjadikan ummat melakukan upgrade keimanan, perbaikan sikap tutur kata hingga mampu mengontrol hawa nafsu sehingga tidak sia-sia dalam mendapatkan pahala puasa di bulan ramadhan ini. Sebagaimana halnya yang dilakukan oleh seluruh jajaran Pejabat dan Staff Balitbang Prov.Sultra yang menjalani rutinitas Kultum pengajian sebelum dimulainya aktivitas kantor. Peningkatan keimanan dan selalu melakukan perbaikan diri mejadikan kegiatan tersebut sebagai salah satu wadah&nbsp;aktualisasi diri sebagai&nbsp;proses pendekatan lebih kepada sang Khalik dalam&nbsp;menggapai FitrahNya.</p>\r\n', '2017-06-06', 'Berita');

-- --------------------------------------------------------

--
-- Table structure for table `table_desa`
--

CREATE TABLE IF NOT EXISTS `table_desa` (
  `desa_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_desa` varchar(100) NOT NULL,
  `jenis` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `kec_id` int(11) NOT NULL,
  `luas` int(11) NOT NULL,
  PRIMARY KEY (`desa_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `table_desa`
--

INSERT INTO `table_desa` (`desa_id`, `nama_desa`, `jenis`, `keterangan`, `kec_id`, `luas`) VALUES
(1, 'Dangia', 'Desa', 'Desa Dangia', 1, 1000),
(2, 'Gunung Jaya', 'Desa', 'Desa gunung Jaya', 1, 100);

-- --------------------------------------------------------

--
-- Table structure for table `table_desageografi`
--

CREATE TABLE IF NOT EXISTS `table_desageografi` (
  `desa_id` int(11) NOT NULL,
  `ketinggian` varchar(100) NOT NULL,
  `curah_hujan` varchar(100) NOT NULL,
  `topografi` varchar(100) NOT NULL,
  `iklim` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL,
  `desageografi_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`desageografi_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `table_desageografi`
--

INSERT INTO `table_desageografi` (`desa_id`, `ketinggian`, `curah_hujan`, `topografi`, `iklim`, `tahun`, `desageografi_id`) VALUES
(1, '120', '10', '10', '25', 2016, 1);

-- --------------------------------------------------------

--
-- Table structure for table `table_dokter`
--

CREATE TABLE IF NOT EXISTS `table_dokter` (
  `id_dokter` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dokter` varchar(50) NOT NULL,
  `divisi` varchar(50) NOT NULL,
  `deskripsi` varchar(150) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `fb` varchar(50) NOT NULL,
  `twitter` varchar(50) NOT NULL,
  `instagram` varchar(50) NOT NULL,
  PRIMARY KEY (`id_dokter`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `table_dokter`
--

INSERT INTO `table_dokter` (`id_dokter`, `nama_dokter`, `divisi`, `deskripsi`, `gambar`, `fb`, `twitter`, `instagram`) VALUES
(4, 'adfdg', 'egewg', 'etgrehrhy', 'adfdg.jpg', 'dgfdg', 'qeg', 'dgdhg'),
(5, 'fhfhtj', 'sfdgrf', 'asfwergy sfgrfh', 'fhfhtj.jpg', 'dgfh', 'ddv', 'dfd'),
(6, 'mnj', 'hvhj', 'gyg', 'mnj.jpg', 'j', 'd', 'f');

-- --------------------------------------------------------

--
-- Table structure for table `table_faq`
--

CREATE TABLE IF NOT EXISTS `table_faq` (
  `id_faq` int(11) NOT NULL AUTO_INCREMENT,
  `tanya` varchar(100) NOT NULL,
  `jawab` text NOT NULL,
  PRIMARY KEY (`id_faq`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `table_faq`
--

INSERT INTO `table_faq` (`id_faq`, `tanya`, `jawab`) VALUES
(1, 'Kenapa pattimura bisa tertangkap ? wkwk', 'takdir pak. Various versions have evolved over the years, sometimes by accident, sometimes on purpose Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text (injected humour and the like).'),
(3, 'Apa nama ibukota venezuela ?', 'Various versions have evolved over the years, sometimes by accident, sometimes on purpose Many deskt');

-- --------------------------------------------------------

--
-- Table structure for table `table_fasilitas`
--

CREATE TABLE IF NOT EXISTS `table_fasilitas` (
  `id_fasilitas` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` varchar(50) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `deskripsi` varchar(50) NOT NULL,
  PRIMARY KEY (`id_fasilitas`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=19 ;

--
-- Dumping data for table `table_fasilitas`
--

INSERT INTO `table_fasilitas` (`id_fasilitas`, `gambar`, `judul`, `deskripsi`) VALUES
(14, 'DIKTI.png', 'Kementerian Riset Teknologi Dan Pendidikan Tinggi ', 'http://www.dikti.go.id/'),
(15, 'Badan_Pengkajian_dan_Penerapan_Teknologi.png', 'Badan Pengkajian dan Penerapan Teknologi', 'http://www.bppt.go.id/'),
(16, 'Litbang_Kemendagri.png', 'Litbang Kemendagri', 'http://litbang.kemendagri.go.id'),
(17, 'LIPI.png', 'LIPI', 'http://lipi.go.id/'),
(18, 'Pusat_Peraga_IPTEK.png', 'Pusat Peraga IPTEK', 'http://ppiptek.ristekdikti.go.id/');

-- --------------------------------------------------------

--
-- Table structure for table `table_foto`
--

CREATE TABLE IF NOT EXISTS `table_foto` (
  `id_foto` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_foto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `table_foto`
--

INSERT INTO `table_foto` (`id_foto`, `judul`, `deskripsi`, `gambar`) VALUES
(2, 'Foto 2', 'Foto 2', 'Foto_2.jpg'),
(3, 'Foto 1', 'Foto 1', 'Foto_1.jpg'),
(4, 'Lokakarya Kelitbangan Tahun 2017', 'Kegiatan Seminar Lokakarya Kelitbangan Tahun 2017', 'XaFjMQVcqd.JPG'),
(5, 'FGD Pengembangan Koltim Technopark', 'FGD ', 'FGD_Pengembangan_Koltim_Technopark.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `table_group`
--

CREATE TABLE IF NOT EXISTS `table_group` (
  `group_id` varchar(50) NOT NULL,
  `group_name` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `active` char(1) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_group`
--

INSERT INTO `table_group` (`group_id`, `group_name`, `description`, `active`) VALUES
('group1000', 'Administrator', 'Full Access Administrator', 'Y'),
('group404', 'User', 'User', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `table_groupaccess`
--

CREATE TABLE IF NOT EXISTS `table_groupaccess` (
  `group_id` varchar(50) NOT NULL,
  `menu_id` varchar(50) NOT NULL,
  `c` char(1) NOT NULL,
  `r` char(1) NOT NULL,
  `u` char(1) NOT NULL,
  `d` char(1) NOT NULL,
  `s` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_groupaccess`
--

INSERT INTO `table_groupaccess` (`group_id`, `menu_id`, `c`, `r`, `u`, `d`, `s`) VALUES
('group1000', 'menu001', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu475', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group128', 'menu002', 'T', 'T', 'Y', 'Y', 'Y'),
('group1000', 'menu168', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu386', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu898', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu965', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group256', 'menu001', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group256', 'menu907', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group271', 'menu186', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group271', 'menu965', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu002', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu970', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group247', 'menu298', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu874', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu614', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group992', 'menu898', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group513', 'menu386', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group370', 'menu614', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group204', 'menu970', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group128', 'menu439', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group128', 'menu168', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group761', 'menu874', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group128', 'menu475', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu265', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu637', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu866', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group404', 'menu444', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group404', 'menu866', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu186', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group404', 'menu474', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu457', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group404', 'menu778', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group404', 'menu457', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu350', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu418', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu382', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu742', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu897', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu165', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu560', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu637', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu414', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu192', 'Y', 'Y', 'Y', 'Y', 'Y'),
('group1000', 'menu326', 'Y', 'Y', 'Y', 'Y', 'Y'),
('1', '- Nama Menu -', '-', '-', '-', '-', '-'),
('1', '- Nama Menu -', '-', '-', '-', '-', '-'),
('2', 'menu186', 'Y', 'Y', 'Y', 'Y', 'Y'),
('1', 'menu192', 'Y', 'T', 'Y', 'T', 'Y'),
('2', 'menu961', 'T', 'T', 'T', 'T', 'T');

-- --------------------------------------------------------

--
-- Table structure for table `table_info`
--

CREATE TABLE IF NOT EXISTS `table_info` (
  `id_info` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `tanggal` date NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `content` text NOT NULL,
  PRIMARY KEY (`id_info`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `table_info`
--

INSERT INTO `table_info` (`id_info`, `judul`, `tanggal`, `gambar`, `content`) VALUES
(1, 'Tes saja', '2016-12-10', 'Tes_saja.jpg', '<p style="text-align: justify;"><strong style="margin: 0px; padding: 0px; color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Lorem Ipsum</strong><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">&nbsp;adalah contoh teks atau dummy dalam industri percetakan dan penataan huruf atau typesetting. Lorem Ipsum telah menjadi standar contoh teks sejak tahun 1500an, saat seorang tukang cetak yang tidak dikenal mengambil sebuah kumpulan teks dan mengacaknya untuk menjadi sebuah buku contoh huruf. Ia tidak hanya bertahan selama 5 abad, tapi juga telah beralih ke penataan huruf elektronik, tanpa ada perubahan apapun. Ia mulai dipopulerkan pada tahun 1960 dengan diluncurkannya lembaran-lembaran Letraset yang menggunakan kalimat-kalimat dari Lorem Ipsum, dan seiring munculnya perangkat lunak Desktop Publishing seperti Aldus PageMaker juga memiliki versi Lorem Ipsum.</span></p>\r\n'),
(3, 'wkwkwk', '2016-12-11', 'wkwkwk.jpg', '<p style="text-align: justify;"><span style="color: rgb(0, 0, 0); font-family: &quot;Open Sans&quot;, Arial, sans-serif; font-size: 14px; text-align: justify;">Ada banyak variasi tulisan Lorem Ipsum yang tersedia, tapi kebanyakan sudah mengalami perubahan bentuk, entah karena unsur humor atau kalimat yang diacak hingga nampak sangat tidak masuk akal. Jika anda ingin menggunakan tulisan Lorem Ipsum, anda harus yakin tidak ada bagian yang memalukan yang tersembunyi di tengah naskah tersebut. Semua generator Lorem Ipsum di internet cenderung untuk mengulang bagian-bagian tertentu. Karena itu inilah generator pertama yang sebenarnya di internet. Ia menggunakan kamus perbendaharaan yang terdiri dari 200 kata Latin, yang digabung dengan banyak contoh struktur kalimat untuk menghasilkan Lorem Ipsun yang nampak masuk akal. Karena itu Lorem Ipsun yang dihasilkan akan selalu bebas dari pengulangan, unsur humor yang sengaja dimasukkan, kata yang tidak sesuai dengan karakteristiknya dan lain sebagainya.</span></p>\r\n');

-- --------------------------------------------------------

--
-- Table structure for table `table_jadwal`
--

CREATE TABLE IF NOT EXISTS `table_jadwal` (
  `id_jadwal` int(11) NOT NULL AUTO_INCREMENT,
  `nama_dokter` varchar(50) NOT NULL,
  `senin` varchar(50) NOT NULL,
  `selasa` varchar(50) NOT NULL,
  `rabu` varchar(50) NOT NULL,
  `kamis` varchar(50) NOT NULL,
  `jumat` varchar(50) NOT NULL,
  `sabtu` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jadwal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `table_jadwal`
--

INSERT INTO `table_jadwal` (`id_jadwal`, `nama_dokter`, `senin`, `selasa`, `rabu`, `kamis`, `jumat`, `sabtu`) VALUES
(1, 'Dr. Rian Agus Darmawan', '08.00-10.00', '', '', '08.00-10.00', '08.00-10.00', ''),
(2, 'Dr. Boyke', '', '', '10.00-12.00', '', '10.00-12.00', '10.00-12.00');

-- --------------------------------------------------------

--
-- Table structure for table `table_jamker`
--

CREATE TABLE IF NOT EXISTS `table_jamker` (
  `id_jamker` int(11) NOT NULL AUTO_INCREMENT,
  `senin` varchar(50) NOT NULL,
  `selasa` varchar(50) NOT NULL,
  `rabu` varchar(50) NOT NULL,
  `kamis` varchar(50) NOT NULL,
  `jumat` varchar(50) NOT NULL,
  `sabtu` varchar(50) NOT NULL,
  `minggu` varchar(50) NOT NULL,
  PRIMARY KEY (`id_jamker`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `table_jamker`
--

INSERT INTO `table_jamker` (`id_jamker`, `senin`, `selasa`, `rabu`, `kamis`, `jumat`, `sabtu`, `minggu`) VALUES
(1, '07:30-16:00', '07:30-16:00', '07:30-16:00', '07:30-16:00', '07:30-12:00', '-', '-');

-- --------------------------------------------------------

--
-- Table structure for table `table_jasakip`
--

CREATE TABLE IF NOT EXISTS `table_jasakip` (
  `id_jasakip` int(11) NOT NULL AUTO_INCREMENT,
  `id_jenisjasakip` int(11) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `download` text NOT NULL,
  PRIMARY KEY (`id_jasakip`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `table_jasakip`
--

INSERT INTO `table_jasakip` (`id_jasakip`, `id_jenisjasakip`, `judul`, `download`) VALUES
(1, 2, 'Laporan Kinerja SKPD TAHUN 2016', 'Laporan_Kinerja_SKPD_TAHUN_2016.pdf'),
(2, 1, 'Rencana Kerja 2016', 'Rencana_Kerja_2016.pdf'),
(3, 3, 'Rencana Strategi 2016', 'Rencana_Strategi_2016.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `table_jenisagenda`
--

CREATE TABLE IF NOT EXISTS `table_jenisagenda` (
  `id_jenisagenda` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenisagenda` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_jenisagenda`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `table_jenisagenda`
--

INSERT INTO `table_jenisagenda` (`id_jenisagenda`, `nama_jenisagenda`, `deskripsi`) VALUES
(1, 'Agenda Umum', 'Agenda Umum'),
(2, 'Kepala Balitbang', 'Kepala Balitbang'),
(3, 'Bidang Pemerintahan & Pengkajian Peraturan', 'Bidang Pemerintahan & Pengkajian Peraturan'),
(5, 'Bidang Sosial & Kependudukan', 'Bidang Sosial & Kependudukan'),
(6, 'Bidang Ekonomi & Pembangunan', 'Bidang Ekonomi & Pembangunan'),
(7, 'Bidang Inovasi & Tekhnologi', 'Bidang Inovasi & Tekhnologi'),
(8, 'UPTB Pusat Peraga IPTEK', 'UPTB Pusat Peraga IPTEK');

-- --------------------------------------------------------

--
-- Table structure for table `table_jenisjasakip`
--

CREATE TABLE IF NOT EXISTS `table_jenisjasakip` (
  `id_jenisjasakip` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenisjasakip` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_jenisjasakip`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `table_jenisjasakip`
--

INSERT INTO `table_jenisjasakip` (`id_jenisjasakip`, `nama_jenisjasakip`, `deskripsi`) VALUES
(1, 'Renja', 'Renja'),
(2, 'Lakip', 'Lakip'),
(3, 'Renstra', 'Renstra');

-- --------------------------------------------------------

--
-- Table structure for table `table_jenisjurnal`
--

CREATE TABLE IF NOT EXISTS `table_jenisjurnal` (
  `id_jenisjurnal` int(11) NOT NULL AUTO_INCREMENT,
  `nama_jenisjurnal` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_jenisjurnal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `table_jenisjurnal`
--

INSERT INTO `table_jenisjurnal` (`id_jenisjurnal`, `nama_jenisjurnal`, `deskripsi`) VALUES
(1, 'Cakrawala', 'Kategori Tulisan Cakrawala Indonesia'),
(2, 'Jurnal Penelitian', 'Jurnal Penelitian'),
(3, 'Hasil Penelitian Balitbang Sultra', 'Hasil Penelitian Balitbang Sultra'),
(4, 'Hasil Penelitian Forum Jaringan', 'Hasil Penelitian Forum Jaringan'),
(5, 'Peneliti', 'Peneliti'),
(6, 'Forum', 'Forum'),
(7, 'Kelitbangan', 'Kelitbangan');

-- --------------------------------------------------------

--
-- Table structure for table `table_jenis_pekerjaan`
--

CREATE TABLE IF NOT EXISTS `table_jenis_pekerjaan` (
  `jenis_pekerjaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_pekerjaan` varchar(100) NOT NULL,
  PRIMARY KEY (`jenis_pekerjaan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `table_jenis_pekerjaan`
--

INSERT INTO `table_jenis_pekerjaan` (`jenis_pekerjaan_id`, `jenis_pekerjaan`) VALUES
(1, 'cangkul');

-- --------------------------------------------------------

--
-- Table structure for table `table_jurnal`
--

CREATE TABLE IF NOT EXISTS `table_jurnal` (
  `id_jurnal` int(11) NOT NULL AUTO_INCREMENT,
  `edisi` varchar(100) NOT NULL,
  `judul` varchar(100) NOT NULL,
  `penyusun` varchar(100) NOT NULL,
  `tahun` int(11) NOT NULL,
  `kata_kunci` varchar(100) NOT NULL,
  `abstrak` text NOT NULL,
  `id_jenisjurnal` int(11) NOT NULL,
  `download` text NOT NULL,
  PRIMARY KEY (`id_jurnal`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `table_jurnal`
--

INSERT INTO `table_jurnal` (`id_jurnal`, `edisi`, `judul`, `penyusun`, `tahun`, `kata_kunci`, `abstrak`, `id_jenisjurnal`, `download`) VALUES
(3, 'I Tahun 2016', 'Pedoman Kelitbangan Tahun 2016', 'Tim Penyusun Balitbang Prov.Sultra', 2016, 'Pedoman,Kelitbangan,Badan Penelitian dan Pengembangan,Provinsi,Sulawesi Tenggara', 'Sebagai bentuk keseragaman dalam penulisan seluruh Laporan Kegiatan Penelitian dan Pengembangan yang dalam lingkup Balitbang Prov.Sultra maka dikeluarkan kebijakan Pedoman Kelitbangan Tahun 2016. Buku pedoman ini disusun sebagai turunan dari Permendagri No.17 tahun 2016 dengan merujuk pada Peraturan Gubernur Sulawesi Tenggara. Diharapkan buku pedoman ini dijadikan sebagai acuan dasar bagi peneliti maupun calon peneliti yang akan melakukan kegiatan penelitian dan pengembangan pada Badan Penelitian dan Pengembangan Provinsi.Sulawesi Tenggara', 7, 'Pedoman_Kelitbangan_Tahun_2016.pdf'),
(4, 'I Tahun 2017', 'Arah Kebijakan Badan Penelitian dan Pengembangan Provinsi Sulawesi Tenggara Tahun 2017', 'Tim Penyusun Balitbang Prov.Sultra', 2017, 'Arah, Kebijakan, Balitbang, Provinsi, Sulawesi Tenggara', 'Penetapan Arah Kebijakan Litbang Tahun 2017 dibentuk sebagai pedoman dalam menentukan sasaran kinerja Badan Penelitian dan Pengembangan Provinsi Sulawesi Tenggara Tahun 2017. Dokumen tersebut disusun sebagai dasar dari seluruh cakupan kegiatan utama dan kegiatan penunjang kapasitas kelitbangan yang akan dilaksanakan oleh Balitbang Prov.Sultra pada tahun 2017 ini. Visi dan misi yang hendak dicapai terurai jelas serta memuat strategi pencapaian sasaran kerja dan pengembangan Balitbang Prov.Sultra kedepannya. Dengan menetapkan 6 topik kelitbangan diharapkan mampu memberikan rujukan yang lebih spesifik dan mendalam bagi penentuan solusi permasalahan di berbagai bidang baik sosial kemasyarakatn, ekonomi, ketahanan pangan, budaya serta Ilmu Pengetahuan dan Tekhnologi', 7, 'Arah_Kebijakan_Badan_Penelitian_dan_Pengembangan_Provinsi_Sulawesi_Tenggara_Tahun_2017.pdf'),
(5, 'MATERI LOKAKARYA KELITBANGAN TAHUN 2017', 'MATRIKS DAN PELAKU KEGIATAN KELITBANGAN', 'Tim Penyusun Balitbang Prov.Sultra', 2017, '', '<p>Disampaikan sebagai paparan Narasumber La Fariki, S.Hut,M.Si dalam kegiatan Lokakarya Kelitbangan yang diadakan di Hotel Horison, 31 Januari 2017 - 01 Februari 2017</p>\r\n', 7, ''),
(6, 'MATERI LOKAKARYA KELITBANGAN TAHUN 2017', 'TELAAHAN PERMENDAGRI NO.17 TAHUN 2016 ', 'Tim Penyusun Balitbang Prov.Sultra', 2017, '', '<p>Disampaikan sebagai paparan Narasumber pada kegiatan Lokakarya Kelitbangan, Hotel Horison, 31 Januari 2017 - 01 Februari 2017</p>\r\n', 7, ''),
(7, 'Pertama', 'PEMETAAN SEKOLAH BERBASIS GEOGRAPHIC  INFORMATION SYSTEM (GIS) DI SULAWESI TENGGARA ', 'Fredy, Idin Ruspian Laydi ', 2016, 'Pemetaan Sekolah, SMA, SMK, Geographic Information System', '<p style="text-align:center">ABSTRAK</p>\r\n\r\n<p>Penelitian ini bertujuan menghasilkan sistem pemetaan sekolah berbasis geographic information system (GIS) yang menampilkan profil dan letak sekolah menengah atas (SMA) dan sekolah menengah kejuruan (SMK) di Sulawesi Tenggara. Pendekatan yang digunakan adalah survey. Metode pengumpulan data adalah observasi lapangan untuk pengambilan titik koordinat lokasi sekolah dengan menggunakan global positioning system (GPS), dokumentasi untuk mengetahui data alamat, jumlah sekolah, jumlah rombongan belajar, dan jumlah ruang kelas SMA dan SMK. Hasil penelitian menunjukkan bahwa sistem pemetaan sekolah berbasis Web GIS dapat memberikan gambaran profil dan letak sekolah (SMA dan SMK) di Kabupaten Muna, Muna Barat, Konawe, Kolaka, Kolaka Timur dan Kota Bau-Bau. Peta tematik digunakan untuk menyimpulkan dan menganalisis tingkat kepadatan siswa, tingkat kepadatan guru dan tingkat persebaran sekolah</p>\r\n', 2, ''),
(8, 'Pertama', 'PENGEMBANGAN AGROMINERALOGI UNTUK PERTUMBUHAN  DAN PRODUKSI JAGUNG DI LAHAN SUB-OPTIMAL  PROV.SULTRA', 'M. Tufaila, Sitti Leomo, Jufri Karim dan Syamsu Alam ', 2016, 'Agrogeologi, Dosis Kapur, Produktivitas lahan', '<p style="text-align:center">Abstrak &nbsp;<br />\r\nPenelitian ini bertujuan untuk mengkarakterisasi dan memetakan potensi agromineralogi sebagai sumber unsur hara, mengembangkan formulasi pupuk alam berbasis batuan yang mudah dan murah dalam merehabilitasi lahan-lahan sub optimal/marginal, dan menguji efektifitas pupuk alam dari batuan dalam menyediakan unsur hara bagi kebutuhan tanaman serta efisiensinya dalam meningkatkan produktifitas lahan. Penelitian dilakukan dengan metode survei berdasarkan Satuan Peta Geologi, dilanjutkan dengan karakterisasi di laboratorium dan uji lapangan. Hasil penelitian menunjukkan bahwa sampel batuan asal Pulau Muna berpotensi dikembangkan sebagai pupuk alam karena mengandung unsur hara makro dalam jumlah besar. Hasil pengujian berbagai jenis bahan agromineral asal Kabupaten Muna menunjukkan bahwa pemberian berbagai bahan agromineral pada lahan sub-optimal di Kabupaten Konawe Selatan berpengaruh signifikan pada pertumbuhan tanaman. Rata-rata pertumbuhan dan hasil tanaman jagung akibat perlakuan berbagai bahan agromineral lebih baik dari pada kontrol tanpa pemberian agromineral</p>\r\n', 2, ''),
(9, 'Pertama', 'PROGRAM LEM SEJAHTERA DAN PERUBAHAN STRATEGI  USAHA TANI KAKAO DI SULAWESI TENGGARA ', 'Heksa Biopsi Puji Hastuti ,  Syahrun, Arwin ', 2016, ' Program Lembaga Ekonomi Masyarakat  Sejahtera, Strategi, Usaha Tani Kakao ', '<p style="text-align: center;">Abstrak &nbsp;<br />\r\nPenelitian ini bertujuan menganalisis perubahan pengetahuan dan praktek usaha tani petani kakao sebelum dan sesudah program Lembaga Ekonomi Masyarakat (LEM) Sejahtera, &nbsp;dan dampak program LEM Sejahtera mempengaruhi perubahan strategi usaha tani petani kakao di Sulawesi Tenggara. Penelitian ini menggunakan metode deskriptif kualitatif. Hasil penelitian menunjukkan bahwa sebelum ada program LEM Sejahtera, petani kakao memperoleh pengetahuan usaha tani dari keluarga, tetangga, atau teman. Penyuluhan dari pemerintah lebih normatif, sehingga petani tetap menerapkan pengetahuan lamanya. Setelah ada program LEM Sejahtera, materi penyuluhan lebih komprehensif, ada pendampingan dan akses komunikasi petani dengan penyuluh berjalan baik, mendorong petani menerapkan pengetahuan barunya. Program LEM Sejahtera mempengaruhi cara berpikir petani, semula berjalan individual menjadi lebih kompak. Program LEM Sejahtera yang meliputi usaha tani kakao dari permasalahan budi daya, hingga manajemen pemasaran berpengaruh positif bagi upaya mewujudkan kemandirian petani kakao di Sulawesi Tenggara</p>\r\n', 2, ''),
(10, 'Pertama', 'TEKNOLOGI RUMPON DASAR BERBASIS APARTEMEN  IKAN DALAM PENINGKATAN STOK IKAN BERKELANJUTAN DI SULTRA', 'La Ode Abdul Rajab Nadia, Kadir Sabilu, Salwiyah', 2016, ' Rumpon, Apartemen Ikan, Sumber Daya Ikan, Berkelanjutan ', '<p style="text-align:center"><strong>Abstrak </strong>&nbsp;<br />\r\nPenelitian ini bertujuan untuk mengembangkan teknologi rumpon dasar berbasis apartemen ikan dalam meningkatkan stok ikan dan produksi perikanan tangkap, sehingga tercipta bank ikan dan menghasilkan inkubasi bisnis dalam mendukung ketahanan pangan ikan di Sulawesi Tenggara. Penelitian ini menggunakan metode survei, action research pembuatan rumpon dasar dan apartemen ikan, pengumpulan data ekologi dan status keberlanjutan pengelolaan rumpon dasar berbasis apartemen ikan. &nbsp;Hasil penelitian menunjukkan bahwa keberadaan ikan pasca pemasangan rumpon berbasis apartemen ikan, maka jumlah spesies ikan terus meningkat secara periodik dan dari 99 menjadi 141 spesies, dari 351 individu meningkat menjadi 818 individu, dari total 22 family menjadi 30 family pada pengambilan data terakhir. Aspek status keberlanjutan pengelolaan rumpon berbasis apartemen ikan yaitu diperoleh nilai indeks dimensi ekologi 72,16, nilai indeks dimensi ekonomi 82,67 dan nilai indeks dimensi teknologi 47,20 yang tergolong baik</p>\r\n', 2, ''),
(11, 'Pertama', 'KAJIAN GEOSPASIAL POTENSI RESIKO  PERUBAHAN EKOLOGIS KORIDOR MOTUI-SAMPARA ', 'Mukhtar, Sawaluddin, Yusran ', 2016, ' Konversi Lahan, Risiko, Perubahan Ekologis   ', '<p style="text-align: center;"><strong>Abstrak </strong>&nbsp;<br />\r\nPenelitian ini bertujuan untuk mengetahui pola konversi lahan; proyeksi pertumbuhan penduduk; menganalisis perubahan mata pencaharian penduduk; menganalisis perubahan struktur penerimaan dan risiko sektor pertanian dan UKM; menganalisis perubahan kondisi geologi dan geomorfologi lahan; menganalisis kondisi ekosistem; dan menganalisis strategis pengembangan ekologis di koridor Motui-sampara. &nbsp;Metode yang digunakan penelitian ini adalah survey. &nbsp;Hasil penelitian menunjukan bahwa konversi lahan cukup luas untuk pembangunan smelter nikel (107 hektar), kawasan pertambangan Motui seluas 438 hektar; &nbsp;proyeksi pertumbuhan penduduk di Kecamatan Morosi pada tahun 2022 sebesar 51.994 jiwa dan &nbsp;di Kecamatan Motui sebesar 5.438 jiwa; &nbsp;terjadi perubahan mata pencaharian dari petani menjadi pekerja tambang (63,06 %); &nbsp;terjadi peningkatan pendapatan usahatani dan UKM sesudah adanya aktivitas pertambangan; tingkat risiko biaya usaha tani palawija dan UKM mengalami peningkatan; terjadi perubahan bentang alam, perubahan tekstur tanah, ketersediaan air tanah dan berkurangnya bahan organik di kawasan pertambangan; dan alternatif strategi pengelolaan lingkungan (Zona I): optimalisasi pengolahan tambang yang berorientasi pada keberlanjutan lingkungan; kemitraan pengelolaan lingkungan. Alternatif strategi pengelolaan lingkungan (Zona II): kerjasama pengelolaan lingkungan dan perumusan peraturan daerah pengelolaan lingkungan kawasan pertambangan. &nbsp; &nbsp;</p>\r\n', 2, ''),
(12, 'Pertama', 'EFEKTIFITAS IMPLEMENTASI KEBIJAKAN SEKTOR UNGGULAN DALAM MENDORONG PERCEPATAN  PEMBANGUNAN DI SULAWE', 'Jamal Bake, M. Syahadat, Rola Pola Anto ', 2016, ' Efektifitas, Implementasi Kebijakan, Komoditas Unggulan', '<p style="text-align: center;"><strong>Abstrak</strong> &nbsp;</p>\r\n\r\n<p>Penelitian ini bertujuan untuk menganalisis penetapan kebijakan sektor unggulan daerah; implementasi kebijakan dan program sektor unggulan untuk mencapai tujuan dan sasaran; pencapaian target kinerja dalam implementasi kebijakan sektor unggulan di setiap kabupaten/kota; dan konsistensi pencapaian target kinerja implementasi sektor unggulan di Sulawesi Tenggara. Penelitian ini menggunakan teknik analisis uji Location Quotient (LQ). Hasil penelitian menunjukkan bahwa penetapan kebijakan sektor unggulan daerah belum menerapkan pendekatan ekonomi basis; implementasi kebijakan dan program sektor unggulan belum dilaksanakan secara konsisten, dilihat dari proporsi target yang hendak dicapai dengan proporsi anggaran yang dialokasikan secara berkelanjutan; pencapaian kinerja sektor unggulan sudah efektif; dan capaian target kinerja sektor unggulan dilihat dari perkembangan produk domestik regional bruto (PDRB) masing-masing sektor pada setiap kabupaten/kota di Sulawesi Tenggara belum konsisten.&nbsp;</p>\r\n', 2, ''),
(13, 'Pertama', 'KARAKTERISTIK KAWASAN SENTRA PETERNAKAN RAKYAT BERDASARKAN KETERSEDIAAN FASILITAS FISIK PROV.SULTRA', 'Takdir Saili,  La Ode Arsad Sani, Hamdan Has ', 2016, ' Sentra Peternakan Rakyat, Puskeswan, Rumah Potong Hewan, Inseminasi Buatan ', '<p style="text-align: center;"><strong>Abstrak &nbsp;</strong></p>\r\n\r\n<p style="text-align: center;">Penelitian ini bertujuan untuk mengkaji ketersediaan fasilitas fisik peternakan pada lokasi usulan Sentra Peternakan Rakyar (SPR) yang ada di Provinsi Sulawesi Tenggara. Penelitian ini menggunakan analisis deskriptip kualitatif dan kuantitatif. &nbsp;Hasil penelitian menunjukkan bahwa aksesibilitas fasilitas rumah potong hewan (RPH) hanya terdapat di Kabupaten Muna, Konawe dan Konawe Selatan dengan tingkat aksesibilitas 25%-97%, Puskeswan, Pos inseminasi buatan dan pabrik/gudang pakan tersedia hampir di seluruh kabupaten dengan tingkat aksesibilitas sebanyak 49% untuk puskeswan, 48% untuk pos inseminasi buatan (IB) dan 24% untuk pabrik/gudang pakan. Padang Penggembalaan dan Kebun hijauan pakan ternak telah tersedia di seluruh kabupaten dengan tingkat ketersediaan 100%. Kabupaten-kabupaten pengusul kawasan SPR di Provinsi Sulawesi Tenggara telah memiliki dukungan fasilitas fisik peternakan dengan tingkat ketersediaan yang cukup tinggi terutama fasilitas padang penggembalaan dan kebun hijauan pakan ternak</p>\r\n', 2, ''),
(14, 'Pertama', 'ANALISIS POTENSI ENERGI ANGIN DAN SITE MATCHING PEMBANGKIT LISTRIK TENAGA ANGIN DI SULAWESI TENGGARA', 'Hasmina Tari Mokui, Sahabuddin Hay, Cindy Puspitafuri', 2016, ' Turbin Angin, Potensi Energi Angin, Wind Rose, Site Matching, Capacity Factor', '<p style="text-align: center;"><strong>Abstrak &nbsp;</strong><br />\r\nPenelitian ini bertujuan untuk mengkaji potensi energi angin; potensi modifikasi model turbin angin untuk meningkatkan performansinya; serta melakukan site matching mesin pembangkit listrik tenaga angin/bayu (PLTB) yang sesuai dengan kondisi angin di Sulawesi Tenggara. Metode yang digunakan untuk menganalisis potensi angin adalah Distribusi Weibull. Sedangkan untuk membuat grafik wind rose yang menunjukkan besar dan arah angin, digunakan software WRPLOT View. Terkait potensi energi angin, nilai parameter k, c, standar deviasi, kecepatan angina rata-rata, kecepatan angina optimal serta wind energi density juga dihitung. Berdasarkan perhitungan wind power density, karakteristik angin Sulawesi Tenggara tergolong baik di atas ketinggian 50 m. Hasil konfirmasi site matching terhadap 5 mesin PLTB standar, terjadi peningkatan nilai capacity factor pada ketiga stasiun ketika ketinggian tower PLTB meningkat. Nilai capacity factor juga tergantung pada spesifikasi teknis dari mesin PLTB, termasuk diameter rotor, kapasitas serta teknologi mesin</p>\r\n', 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `table_kecamatan`
--

CREATE TABLE IF NOT EXISTS `table_kecamatan` (
  `kec_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kecamatan` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`kec_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `table_kecamatan`
--

INSERT INTO `table_kecamatan` (`kec_id`, `nama_kecamatan`, `keterangan`) VALUES
(1, 'Ladongi', 'Kecamatan Ladongi'),
(2, 'Lalolae', 'Kecamatan Lalolae');

-- --------------------------------------------------------

--
-- Table structure for table `table_kehutanan`
--

CREATE TABLE IF NOT EXISTS `table_kehutanan` (
  `desa_id` int(11) NOT NULL,
  `kehutanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_kawasan` varchar(100) NOT NULL,
  `luas` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `tahun` year(4) NOT NULL,
  PRIMARY KEY (`kehutanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_kelamin`
--

CREATE TABLE IF NOT EXISTS `table_kelamin` (
  `id_kelamin` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_kelamin` varchar(100) NOT NULL,
  PRIMARY KEY (`id_kelamin`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_kelembagaan`
--

CREATE TABLE IF NOT EXISTS `table_kelembagaan` (
  `desa_id` int(11) NOT NULL,
  `kelembagaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kelembagaan` varchar(300) NOT NULL,
  `jenis_kelembagaan` varchar(300) NOT NULL,
  `alamat` text NOT NULL,
  `tahun` char(4) NOT NULL,
  PRIMARY KEY (`kelembagaan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `table_kelembagaan`
--

INSERT INTO `table_kelembagaan` (`desa_id`, `kelembagaan_id`, `nama_kelembagaan`, `jenis_kelembagaan`, `alamat`, `tahun`) VALUES
(2, 1, 'adt', 'adat', 'jl adat', '2016'),
(2, 3, 'pp', 'politik', 'pp', '6868'),
(2, 4, '52352', 'pendidikan', '523535', '4324'),
(2, 5, 'qwe', 'hukum', 'qwerty', 'qwer'),
(2, 6, 'adat', 'politik', 'asaas', 'sasa');

-- --------------------------------------------------------

--
-- Table structure for table `table_layanan`
--

CREATE TABLE IF NOT EXISTS `table_layanan` (
  `id_layanan` int(11) NOT NULL AUTO_INCREMENT,
  `jenis_layanan` varchar(50) NOT NULL,
  `judul` varchar(50) NOT NULL,
  `deskripsi` varchar(50) NOT NULL,
  PRIMARY KEY (`id_layanan`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `table_layanan`
--

INSERT INTO `table_layanan` (`id_layanan`, `jenis_layanan`, `judul`, `deskripsi`) VALUES
(3, 'Layanan 2', 'Layanan 2', 'Layanan 2'),
(4, 'Layanan 1', 'Layanan 1', 'Layanan 1');

-- --------------------------------------------------------

--
-- Table structure for table `table_log`
--

CREATE TABLE IF NOT EXISTS `table_log` (
  `log_id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(100) NOT NULL,
  `logdate` date NOT NULL,
  `logtime` time NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`log_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=55 ;

--
-- Dumping data for table `table_log`
--

INSERT INTO `table_log` (`log_id`, `userid`, `logdate`, `logtime`, `description`) VALUES
(1, 'usr668', '2017-06-13', '00:31:28', 'Insert Data Menu dengan menu_id = menu581'),
(2, 'usr668', '2017-06-13', '04:16:49', 'Insert Data kecamatan'),
(3, 'usr668', '2017-06-13', '04:29:17', 'Update Data dokter dengan kec_id = 1'),
(4, 'usr668', '2017-06-13', '06:03:09', 'Insert Data kecamatan'),
(5, 'usr668', '2017-06-13', '06:06:14', 'Insert Data Desa dengan desa_id = '),
(6, 'usr668', '2017-06-14', '05:38:13', 'Login'),
(7, 'usr668', '2017-06-14', '05:45:37', 'Login'),
(8, 'usr668', '2017-06-14', '05:52:18', 'Insert Data Desa dengan desa_id = '),
(9, 'usr668', '2017-06-14', '06:28:38', 'Insert Data Desa Geografi dengan desageografi_id = '),
(10, 'usr668', '2017-06-14', '06:30:16', 'Insert Data Desa Geografi dengan desageografi_id = '),
(11, 'usr668', '2017-06-14', '06:36:42', 'Update Data Desa Geografi dengan desageografi_id = 1'),
(12, 'usr668', '2017-06-14', '06:38:26', 'Update Data Desa Geografi dengan desageografi_id = 1'),
(13, 'usr668', '2017-06-14', '06:42:18', 'Hapus Data Desa Geografi dengan desageografi_id = '),
(14, 'usr668', '2017-06-14', '06:43:02', 'Insert Data Desa Geografi dengan desageografi_id = '),
(15, 'usr668', '2017-06-14', '06:43:07', 'Hapus Data Desa Geografi dengan desageografi_id = '),
(16, 'usr668', '2017-06-15', '02:44:23', 'Login'),
(17, 'usr668', '2017-06-15', '02:55:13', 'Insert Data Desa Geografi dengan desageografi_id = '),
(18, 'usr668', '2017-06-15', '02:55:23', 'Update Data Desa Geografi dengan desageografi_id = 4'),
(19, 'usr668', '2017-06-15', '02:55:35', 'Update Data Desa Geografi dengan desageografi_id = 4'),
(20, 'usr668', '2017-06-15', '02:55:43', 'Hapus Data Desa Geografi dengan desageografi_id = '),
(21, 'usr668', '2017-06-15', '10:35:24', 'Login'),
(22, 'usr668', '2017-06-16', '02:25:13', 'Login'),
(23, 'usr668', '2017-06-16', '10:54:31', 'Login'),
(24, 'usr668', '2017-06-16', '12:11:00', 'Login'),
(25, 'usr668', '2017-06-17', '06:40:16', 'Login'),
(26, 'usr668', '2017-06-17', '11:39:04', 'Login'),
(27, 'usr668', '2017-06-17', '20:49:34', 'Login'),
(28, 'usr668', '2017-06-18', '06:54:03', 'Login'),
(29, 'usr668', '2017-06-18', '21:05:56', 'Login'),
(30, 'usr668', '2017-06-18', '21:36:27', 'Login'),
(31, 'usr668', '2017-06-19', '03:20:04', 'Login'),
(32, 'usr668', '2017-06-19', '06:09:50', 'Login'),
(33, 'usr668', '2017-06-19', '07:08:17', 'Login'),
(34, 'usr668', '2017-06-19', '07:22:34', 'Insert Data Desa Geografi dengan desageografi_id = '),
(35, 'usr668', '2017-06-19', '07:22:55', 'Update Data Desa Geografi dengan desageografi_id = 2'),
(36, 'usr668', '2017-06-19', '07:23:06', 'Update Data Desa Geografi dengan desageografi_id = 2'),
(37, 'usr668', '2017-06-19', '07:23:25', 'Hapus Data Desa Geografi dengan desageografi_id = '),
(38, 'usr668', '2017-06-19', '08:43:54', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = '),
(39, 'usr668', '2017-06-19', '08:45:23', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = '),
(40, 'usr668', '2017-06-19', '09:49:04', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = '),
(41, 'usr668', '2017-06-19', '15:30:19', 'Login'),
(42, 'usr668', '2017-06-19', '16:12:38', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = '),
(43, 'usr668', '2017-06-19', '16:15:10', 'Hapus Data jenis pekerjaan dengan jenis_pekerjaan_id = '),
(44, 'usr668', '2017-06-19', '16:16:47', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = '),
(45, 'usr668', '2017-06-19', '16:16:51', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = '),
(46, 'usr668', '2017-06-19', '16:16:55', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = '),
(47, 'usr668', '2017-06-19', '16:16:58', 'Hapus Data jenis pekerjaan dengan jenis_pekerjaan_id = '),
(48, 'usr668', '2017-06-19', '16:20:07', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = '),
(49, 'usr668', '2017-06-19', '16:20:11', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = '),
(50, 'usr668', '2017-06-19', '16:20:15', 'Hapus Data jenis pekerjaan dengan jenis_pekerjaan_id = 9'),
(51, 'usr668', '2017-06-19', '22:54:16', 'Hapus Data jenis pekerjaan dengan jenis_pekerjaan_id = 11'),
(52, 'usr668', '2017-06-19', '23:18:06', 'Login'),
(53, 'usr668', '2017-06-20', '03:43:11', 'Login'),
(54, 'usr668', '2017-06-20', '03:59:44', 'Insert Data Desa Geografi dengan jenis_pekerjaan_id = ');

-- --------------------------------------------------------

--
-- Table structure for table `table_menu`
--

CREATE TABLE IF NOT EXISTS `table_menu` (
  `menu_id` varchar(50) NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `attribute` text NOT NULL,
  `link` varchar(100) NOT NULL,
  `active` char(1) NOT NULL,
  `adminmenu` char(1) NOT NULL,
  `position` char(1) NOT NULL,
  `description` text NOT NULL,
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_menu`
--

INSERT INTO `table_menu` (`menu_id`, `menu_name`, `attribute`, `link`, `active`, `adminmenu`, `position`, `description`) VALUES
('menu001', 'Setting', '<i class="fa fa-wrench"></i>Setting', '/home', 'Y', 'Y', '1', 'CMS Modul'),
('menu165', 'Post', '<i class="fa fa-paper-plane"></i>Posts', '/home', 'Y', 'Y', '4', 'berita dan info sehat'),
('menu186', 'Frontpage', '<i class="fa fa-align-justify"></i>Frontpage', '/home', 'T', 'T', '3', 'Menu Frontoffice'),
('menu192', 'Others', '<i class="fa fa-plug"></i>Others', '/home', 'Y', 'Y', '7', 'Tentang Kami dan Banner'),
('menu240', 'Service', '<i class="fa fa-ambulance"></i>Service', '/home', 'T', 'Y', '5', 'layanan, jam kerja, Fasilitas'),
('menu326', 'Pemerintahan', '<i class="fa fa-building"></i>Pemerintahan', '/home', 'Y', 'Y', '9', 'Menu Pemerintahan'),
('menu408', 'Jurnal', '<i class="fa fa-book"></i>Jurnal', '/home', 'T', 'Y', '8', 'Menu Upload Jurnal'),
('menu414', 'Galeri', '<i class="fa fa-film"></i>Galeri', '/home', 'Y', 'Y', '6', 'foto dan video'),
('menu422', 'Agenda', '<i class="fa fa-users"></i>Agenda', '/home', 'T', 'Y', '1', ''),
('menu439', 'Backoffice', '<i class="fa fa-check-square-o"></i>Backoffice', '/home', 'Y', 'Y', '1', 'Backoffice Menu'),
('menu494', 'Jasakip', '<i class="fa fa-book"></i>JASAKIP', '/home', 'T', 'Y', '9', 'Menu JASAKIP'),
('menu778', 'Setting', '<i class="fa fa-wrench"></i>Setting', '/home', 'Y', 'Y', '1', ''),
('menu961', 'Medic', '<i class="fa fa-user-md"></i>Medic', '/home', 'T', 'Y', '3', 'dokter, jadwal dokter, struktur');

-- --------------------------------------------------------

--
-- Table structure for table `table_pekerjaan`
--

CREATE TABLE IF NOT EXISTS `table_pekerjaan` (
  `desa_id` int(11) NOT NULL,
  `pekerjaan_id` int(11) NOT NULL AUTO_INCREMENT,
  `jumlah` int(100) NOT NULL,
  `tahun` char(4) NOT NULL,
  PRIMARY KEY (`pekerjaan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_pendidikan`
--

CREATE TABLE IF NOT EXISTS `table_pendidikan` (
  `desa_id` int(11) NOT NULL,
  `pendidikan_id` int(11) NOT NULL AUTO_INCREMENT,
  `sd` int(11) NOT NULL,
  `smp` int(11) NOT NULL,
  `sma` int(11) NOT NULL,
  `s1` int(11) NOT NULL,
  `s2` int(11) NOT NULL,
  `s3` int(11) NOT NULL,
  `non` int(11) NOT NULL,
  PRIMARY KEY (`pendidikan_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `table_pendidikan`
--

INSERT INTO `table_pendidikan` (`desa_id`, `pendidikan_id`, `sd`, `smp`, `sma`, `s1`, `s2`, `s3`, `non`) VALUES
(2, 1, 121, 342, 563, 7893, 433, 233, 109877),
(2, 3, 2321, 3123, 1424, 1241, 12421, 1412, 2142),
(2, 4, 9999, 9999, 9999, 9999, 9999, 9999, 9999),
(2, 5, 888, 888, 888, 888, 888, 888, 888),
(2, 6, 8989, 8989, 8989, 8989, 8989, 8989, 8989);

-- --------------------------------------------------------

--
-- Table structure for table `table_penduduk`
--

CREATE TABLE IF NOT EXISTS `table_penduduk` (
  `desa_id` int(11) NOT NULL,
  `penduduk_id` int(11) NOT NULL AUTO_INCREMENT,
  `jumlah` char(16) NOT NULL,
  `kelamin` varchar(11) NOT NULL,
  `tahun` int(4) NOT NULL,
  PRIMARY KEY (`penduduk_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=28 ;

--
-- Dumping data for table `table_penduduk`
--

INSERT INTO `table_penduduk` (`desa_id`, `penduduk_id`, `jumlah`, `kelamin`, `tahun`) VALUES
(1, 10, '222', 'wanita', 1111),
(2, 15, '1230231', 'wanita', 1323),
(2, 16, '343243', 'pria', 3243),
(2, 17, '23123', 'pria', 1321),
(2, 18, '13134343', 'pria', 1322),
(1, 19, '123456789', 'pria', 2313),
(0, 20, '312312', 'wanita', 2141),
(0, 21, '999999999', 'pria', 9999),
(0, 22, '23333', 'wanita', 3213),
(1, 23, '121111', 'pria', 1211),
(2, 24, '212', 'wanita', 2121),
(1, 26, '321323', 'pria', 2321),
(1, 27, '3333333', 'wanita', 3333);

-- --------------------------------------------------------

--
-- Table structure for table `table_perikanan`
--

CREATE TABLE IF NOT EXISTS `table_perikanan` (
  `desa_id` int(11) NOT NULL,
  `perikanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ikan` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `luas` int(11) NOT NULL,
  PRIMARY KEY (`perikanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_perkebunan`
--

CREATE TABLE IF NOT EXISTS `table_perkebunan` (
  `desa_id` int(11) NOT NULL,
  `perkebunan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tanaman` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `luas` int(11) NOT NULL,
  PRIMARY KEY (`perkebunan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_pertambangan`
--

CREATE TABLE IF NOT EXISTS `table_pertambangan` (
  `desa_id` int(11) NOT NULL,
  `tambang_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tambang` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `luas` int(11) NOT NULL,
  PRIMARY KEY (`tambang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_pertanian`
--

CREATE TABLE IF NOT EXISTS `table_pertanian` (
  `desa_id` int(11) NOT NULL,
  `pertanian_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_tanaman` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `luas` int(11) NOT NULL,
  PRIMARY KEY (`pertanian_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_peternakan`
--

CREATE TABLE IF NOT EXISTS `table_peternakan` (
  `desa_id` int(11) NOT NULL,
  `peternakan_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_ternak` varchar(100) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `tahun` year(4) NOT NULL,
  `luas` int(11) NOT NULL,
  PRIMARY KEY (`peternakan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_profil`
--

CREATE TABLE IF NOT EXISTS `table_profil` (
  `id_profil` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `gambar` varchar(100) NOT NULL,
  PRIMARY KEY (`id_profil`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `table_profil`
--

INSERT INTO `table_profil` (`id_profil`, `judul`, `content`, `gambar`) VALUES
(1, 'SEJARAH', 'Dinas Perindustrian dan Perdagangan Sulawesi Tenggara&nbsp;', 'SEJARAH.jpg'),
(2, 'VISI DAN MISI', '<p style="line-height: 20.8px;"><strong>VISI :</strong></p>\r\n\r\n<p style="line-height: 20.8px;">Terwujudnya industri dan perdagangan yang maju, mandiri dan berdaya saing menuju masyarakat Sulawesi Tenggara yang sejahtera tahun 2013-2018</p>\r\n\r\n<p style="line-height: 20.8px;">&nbsp;</p>\r\n\r\n<p style="line-height: 20.8px;"><strong>MISI :</strong></p>\r\n\r\n<ol style="line-height: 20.8px;">\r\n	<li>\r\n	<p>Meningkatkan kualitas sumber daya manusia industri dan perdagangan</p>\r\n	</li>\r\n	<li>\r\n	<p>Mendorong pertumbuhan dan kemajuan industri dan perdagangan</p>\r\n	</li>\r\n	<li>\r\n	<p>Mewujudkan perencanaan yang baik di sektor industri dan perdagangan</p>\r\n	</li>\r\n	<li>\r\n	<p>Mewujudkan penataan usaha industri dan perdagangan</p>\r\n	</li>\r\n	<li>\r\n	<p>Mewujudkan tertib usaha industri dan perdagangan</p>\r\n	</li>\r\n	<li>\r\n	<p>Meningkatkan daya saing produk dan komoditi industri dan perdagangan</p>\r\n	</li>\r\n</ol>\r\n', 'VISI_DAN_MISI.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `table_sarana`
--

CREATE TABLE IF NOT EXISTS `table_sarana` (
  `desa_id` int(11) NOT NULL,
  `sarana_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_sarana` varchar(100) NOT NULL,
  `jenis_sarana` varchar(100) NOT NULL,
  `luas` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `koordinat` text NOT NULL,
  `alamat` varchar(100) NOT NULL,
  PRIMARY KEY (`sarana_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `table_struktur`
--

CREATE TABLE IF NOT EXISTS `table_struktur` (
  `id_struktur` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `deskripsi` text NOT NULL,
  `gambar` varchar(50) NOT NULL,
  PRIMARY KEY (`id_struktur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `table_struktur`
--

INSERT INTO `table_struktur` (`id_struktur`, `nama`, `jabatan`, `deskripsi`, `gambar`) VALUES
(22, 'dr. Sukirman, MARS, Sp.PA', 'KOMITE MEDIK', '', 'njbg.jpg'),
(23, 'dr. Hj. Asridah Mukaddim, M.Kes', 'DIREKTUR', '', 'knf.jpg'),
(25, 'Tess', 'dsf', '', 'Tess.jpg'),
(26, 'DSF', 'dwF', 'dD', 'DSF.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `table_submenu`
--

CREATE TABLE IF NOT EXISTS `table_submenu` (
  `submenu_id` varchar(50) NOT NULL,
  `menu_id` varchar(50) NOT NULL,
  `submenu_name` varchar(100) NOT NULL,
  `attribute` text NOT NULL,
  `link` varchar(100) NOT NULL,
  `active` char(1) NOT NULL,
  PRIMARY KEY (`submenu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_submenu`
--

INSERT INTO `table_submenu` (`submenu_id`, `menu_id`, `submenu_name`, `attribute`, `link`, `active`) VALUES
('submenu001', 'menu001', 'User', '', '/user/data', 'Y'),
('submenu002', 'menu001', 'Group', '', '/group/data', 'Y'),
('submenu003', 'menu001', 'Menu', '', '/menu/data', 'Y'),
('submenu132', 'menu897', 'Tentang Kami', '', '/home', 'Y'),
('submenu137', 'menu418', 'Layanan', '', '/home', 'Y'),
('submenu144', 'menu866', 'Antar Pulau', '', '/antar_pulau/data', 'Y'),
('submenu145', 'menu382', 'Info Sehat', '', '/home', 'Y'),
('submenu150', 'menu326', 'Kecamatan', '', '/kecamatan/data', 'Y'),
('submenu153', 'menu439', 'Tanda Tangan', '', '/tanda_tangan/data', 'Y'),
('submenu185', 'menu192', 'F.A.Q', '', '/faq/data', 'T'),
('submenu215', 'menu265', 'Komoditi', '', '/komoditi/data', 'Y'),
('submenu222', 'menu240', 'Layanan', '', '/layanan/data', 'Y'),
('submenu239', 'menu192', 'Tentang Kami', '', '/about/data', 'Y'),
('submenu279', 'menu240', 'Jam Kerja', '', '/jamker/data', 'Y'),
('submenu283', 'menu457', 'Mingguan', '', '/grafik/mingguan', 'Y'),
('submenu304', 'menu414', 'Foto', '', '/foto/data', 'Y'),
('submenu309', 'menu866', 'Stok', '', '/stok/data', 'Y'),
('submenu323', 'menu874', 'Bahan Strategis', '', '/laporan/data6', 'Y'),
('submenu337', 'menu474', 'Bahan Strategis Pasar', '', '/laporan/data4', 'T'),
('submenu338', 'menu897', 'FAQ', '', '/home', 'Y'),
('submenu365', 'menu265', 'Komoditi Tetap', '', '/komoditi_tetap/data', 'Y'),
('submenu368', 'menu457', 'Harian', '', '/grafik/harian', 'Y'),
('submenu369', 'menu494', 'Jasakip', '', '/jasakip/data', 'Y'),
('submenu383', 'menu265', 'Jenis Komoditi', '', '/jenis_komoditi/data', 'Y'),
('submenu384', 'menu422', 'Jenis Agenda', '', '/jenisagenda/data', 'Y'),
('submenu385', 'menu444', 'Pelabuhan ', '', '/pelabuhan/data', 'Y'),
('submenu386', 'menu422', 'Agenda', '', '/agenda/data', 'Y'),
('submenu394', 'menu897', 'Testimoni', '', '/home', 'T'),
('submenu414', 'menu408', 'Jenis Jurnal', '', '/jenisjurnal/data', 'Y'),
('submenu416', 'menu874', 'Bahan Pokok', '', '/laporan/data5', 'Y'),
('submenu426', 'menu961', 'Struktur', '', '/struktur/data', 'Y'),
('submenu435', 'menu240', 'Fasilitas', '', '/fasilitas/data', 'Y'),
('submenu437', 'menu165', 'Berita', '', '/berita/data', 'Y'),
('submenu448', 'menu457', 'Triwulan', '', '/grafik/triwulan', 'Y'),
('submenu455', 'menu326', 'Desa', '', '/desa/data', 'Y'),
('submenu457', 'menu874', 'Antar Pulau Prov.', '', '/laporan/data2', 'Y'),
('submenu472', 'menu742', 'Galeri', '', '/home', 'Y'),
('submenu473', 'menu444', 'Pasar ', '', '/pasar/data ', 'Y'),
('submenu512', 'menu418', 'Hubungi Kami', '', '/home', 'Y'),
('submenu513', 'menu265', 'Pelabuhan', '', '/pelabuhan/data', 'Y'),
('submenu571', 'menu874', 'Bahan Strategis Pasar', '', '/laporan/data4', 'T'),
('submenu589', 'menu474', 'Antar Pulau Kab.', '', '/laporan/data', 'Y'),
('submenu633', 'menu192', 'Banner', '', '/banner/data', 'Y'),
('submenu639', 'menu874', 'Antar Pulau Kab.', 'Laporan kabupaten', '/laporan/data', 'Y'),
('submenu658', 'menu474', 'Bahan Pokok', '', '/laporan/data5', 'Y'),
('submenu676', 'menu418', 'Jam Kerja', '', '/home', 'Y'),
('submenu681', 'menu474', 'Bahan Pokok Pasar', '', '/laporan/data3', 'T'),
('submenu684', 'menu414', 'Video', '', '/video/data', 'Y'),
('submenu693', 'menu494', 'Jenis Jasakip', '', '/jenisjasakip/data', 'Y'),
('submenu695', 'menu192', 'Testimoni', '', '/testi/data', 'T'),
('submenu702', 'menu186', 'Berita', 'Berita', 'berita/data', 'Y'),
('submenu704', 'menu350', 'Dokter', '', '/home', 'Y'),
('submenu759', 'menu474', 'Stok', '', '/laporan/data7', 'Y'),
('submenu762', 'menu408', 'Jurnal', '', '/jurnal/data', 'Y'),
('submenu787', 'menu778', 'User', '', 'user/data', 'Y'),
('submenu793', 'menu866', 'Bahan Pokok', '', '/pokok/data', 'Y'),
('submenu794', 'menu265', 'Kabupaten', '', '/kabupaten/data', 'Y'),
('submenu817', 'menu186', 'Profil', 'Profil', 'profil/data', 'Y'),
('submenu821', 'menu474', 'Bahan Strategis', '', '/laporan/data6', 'Y'),
('submenu828', 'menu165', 'Kategori Berita', '', '/kategoriberita', 'Y'),
('submenu830', 'menu874', 'Bahan Pokok Pasar', '', '/laporan/data3', 'T'),
('submenu837', 'menu457', 'Bulanan', '', '/grafik/bulanan', 'Y'),
('submenu841', 'menu350', 'Jadwal Dokter', '<i class="fa fa-check-square-o"></i>Backoffice', '/home', 'Y'),
('submenu887', 'menu382', 'Berita', 'berita', 'berita/data', 'Y'),
('submenu889', 'menu874', 'Stok', '', '/laporan/data7', 'Y'),
('submenu904', 'menu265', 'Pasar', '', '/pasar/data', 'Y'),
('submenu905', 'menu866', 'Bahan Strategis', '', '/strategis/data', 'Y'),
('submenu930', 'menu457', 'Tahunan', '', '/grafik/tahunan', 'Y'),
('submenu946', 'menu961', 'Dokter', '', '/dokter/data', 'Y'),
('submenu965', 'menu961', 'Jadwal Dokter', '', '/jadwal/data', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `table_testi`
--

CREATE TABLE IF NOT EXISTS `table_testi` (
  `id_testi` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `jabatan` varchar(50) NOT NULL,
  `testimoni` text NOT NULL,
  PRIMARY KEY (`id_testi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `table_testi`
--

INSERT INTO `table_testi` (`id_testi`, `nama`, `jabatan`, `testimoni`) VALUES
(1, 'Paulo Dybala', 'Striker Juve', 'Dalam kondisi persaingan yang semakin ketat seperti saat ini, RSPP dituntut untuk peka menangkap dan menyaring opini yang berkembang dimasyarakat. Dari hari ke hari RSPP terus mengadakan penyesuaian-penyesuaian dalam bisnis pelayanannya'),
(4, 'Paul Pogba', 'Mid Munyuk', 'Dalam kondisi persaingan yang semakin ketat seperti saat ini, RSPP dituntut untuk peka menangkap dan'),
(5, 'Cristiano Ronaldo', 'Striker Madrid', 'Dalam kondisi persaingan yang semakin ketat seperti saat ini, RSPP dituntut untuk peka menangkap dan'),
(6, 'Gigi Buffon', 'GK Juventus', 'Duis autem vel eum iriure dolor in hend rerit in vulputate velit esse molestie vel illum dolore null');

-- --------------------------------------------------------

--
-- Table structure for table `table_user`
--

CREATE TABLE IF NOT EXISTS `table_user` (
  `userid` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `fullname` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `email` varchar(50) NOT NULL,
  `register_date` date NOT NULL,
  `register_time` time NOT NULL,
  `group_id` varchar(50) NOT NULL,
  `active` char(1) NOT NULL,
  `nama_instansi` varchar(100) NOT NULL,
  `id_kabupaten` int(11) NOT NULL,
  `photo` varchar(100) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `table_user`
--

INSERT INTO `table_user` (`userid`, `username`, `fullname`, `password`, `email`, `register_date`, `register_time`, `group_id`, `active`, `nama_instansi`, `id_kabupaten`, `photo`) VALUES
('usr236', 'administrator', 'Administrator', '01488dd067097083f80f32761c0ad802', 'admin@gmail.com', '2017-02-07', '19:53:40', 'group1000', 'Y', 'Balitbang', 2, ''),
('usr668', 'admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'technos-studio@gmail.com', '2016-04-25', '13:49:06', 'group1000', 'Y', '1', 1, 'usr668.png');

-- --------------------------------------------------------

--
-- Table structure for table `table_video`
--

CREATE TABLE IF NOT EXISTS `table_video` (
  `id_video` int(11) NOT NULL AUTO_INCREMENT,
  `judul` varchar(50) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `link` varchar(50) NOT NULL,
  PRIMARY KEY (`id_video`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `table_video`
--

INSERT INTO `table_video` (`id_video`, `judul`, `deskripsi`, `gambar`, `link`) VALUES
(3, 'Video Geografis  Balitbang', 'Video Geografis  Balitbang', 'liverpool.jpg', 'https://www.youtube.com/watch?v=_2jIm2vSTMk&t=65s'),
(5, 'Profile Jurnal Formasi', 'Profil Jurnal Formasi', 'Profile_Jurnal_Formasi.jpg', 'https://www.youtube.com/watch?v=4XVjssKxy6M');

-- --------------------------------------------------------

--
-- Table structure for table `table_wisata`
--

CREATE TABLE IF NOT EXISTS `table_wisata` (
  `desa_id` int(11) NOT NULL,
  `wisata_id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_wisata` varchar(100) NOT NULL,
  `jenis_wisata` varchar(100) NOT NULL,
  `luas` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `koordinat` text NOT NULL,
  `alamat` varchar(100) NOT NULL,
  PRIMARY KEY (`wisata_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
