<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem perindustrian

class Stok extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_komoditi_stok');
		$this->load->model('m_stok');
		$this->load->model('m_login');
		$this->load->model('m_kabupaten');
		$as=$this->session->userdata('userid');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Informasi")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
    }

    public function data(){
	
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('informasi/module_stok/stok', $data);
        $this->load->view('footer');
    }
	
	 public function data_mingguan(){
	
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('informasi/module_stok_mingguan/stok', $data);
        $this->load->view('footer');
    }
	
	public function result(){
       
		$data['id_kabupaten'] = $this->input->post('id_kabupaten');
		//$data['bulan'] = $this->input->post('bulan');
		$data['tahun'] = $this->input->post('tahun');
		$data["pokok"] = $this->m_stok->search_stok($data['id_kabupaten'], $data['tahun']);
		$data['tempat'] = $this->m_kabupaten->get($data['id_kabupaten']);
		$data['tempat2'] = $data['tempat'][0];
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('informasi/module_stok/result', $data);
		$this->load->view('footer');		
		
    }
	
	public function result_mingguan(){
       
		$data['id_kabupaten'] = $this->input->post('id_kabupaten');
		$data['bulan'] = $this->input->post('bulan');
		$data['minggu'] = $this->input->post('minggu');
		$data['tahun'] = $this->input->post('tahun');
		$data["stok"] = $this->m_stok->search_stok_mingguan($data['id_kabupaten'], $data['bulan'], $data['minggu'], $data['tahun']);
		$data['tempat'] = $this->m_kabupaten->get($data['id_kabupaten']);
		$data['tempat2'] = $data['tempat'][0];
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('informasi/module_stok_mingguan/result', $data);
		$this->load->view('footer');		
		
    }
	
	public function result2(){
       
		$data['id_kabupaten'] = $this->input->get('id_kabupaten');
		//$data['bulan'] = $this->input->get('bulan');
		$data['tahun'] = $this->input->get('tahun');
		$data["pokok"] = $this->m_stok->search_stok($data['id_kabupaten'], $data['tahun']);
		$data['tempat'] = $this->m_kabupaten->get($data['id_kabupaten']);
		$data['tempat2'] = $data['tempat'][0];
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('informasi/module_stok/result', $data);
		$this->load->view('footer');		
		
    }
	
	public function result3(){
       
		$data['id_kabupaten'] = $this->input->get('id_kabupaten');
		$data['bulan'] = $this->input->get('bulan');
		$data['minggu'] = $this->input->get('minggu');
		$data['tahun'] = $this->input->get('tahun');
		$data["stok"] = $this->m_stok->search_stok_mingguan($data['id_kabupaten'], $data['bulan'], $data['minggu'], $data['tahun']);
		$data['tempat'] = $this->m_kabupaten->get($data['id_kabupaten']);
		$data['tempat2'] = $data['tempat'][0];
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('informasi/module_stok_mingguan/result', $data);
		$this->load->view('footer');		
		
    }
	
    public function insert(){
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$data['group_id'] = $this->session->userdata('group_id');
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
				
		$data['komoditi'] = $this->m_komoditi_stok->getkomoditi();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('informasi/module_stok/insert');
        $this->load->view('footer');
    }
	
	public function insert_mingguan(){
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$data['group_id'] = $this->session->userdata('group_id');
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
				
		$data['komoditi'] = $this->m_komoditi_stok->getkomoditi();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('informasi/module_stok_mingguan/insert');
        $this->load->view('footer');
    }
    
    public function update(){
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_stok->get($this->input->get('id'));
		$data['komoditi'] = $this->m_komoditi_stok->data();
		if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('stok/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('informasi/module_stok/update', $data);
            $this->load->view('footer');
        }
    }
    
    public function update_mingguan(){
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_stok->get_mingguan($this->input->get('id'));
		$data['komoditi'] = $this->m_komoditi_stok->data();
		if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('stok/data_mingguan');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('informasi/module_stok_mingguan/update', $data);
            $this->load->view('footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_stok->get($this->input->get('id'));
		$data['komoditi'] = $this->m_komoditi_stok->data();
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('stok/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('informasi/module_stok/detail', $data);
            $this->load->view('footer');
        }
    }
	
    public function detail_mingguan(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_stok->get_mingguan($this->input->get('id'));
		$data['komoditi'] = $this->m_komoditi_stok->data();
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('stok/data_mingguan');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('informasi/module_stok_mingguan/detail', $data);
            $this->load->view('footer');
        }
    }
    
    public function create_data() {
        //get data
		$jumlah = count($this->input->post('id_komoditi_stok'));
		$id_komoditi_stok = $this->input->post('id_komoditi_stok');
		//$bulan = $this->input->post('bulan');
		$tahun = $this->input->post('tahun');
		$id_kabupaten = $this->input->post('id_kabupaten');
		$stok = $this->input->post('stok');
		$ketahanan = $this->input->post('ketahanan');
		echo $jumlah;
        
		$cek = $this->m_stok->cekStok($id_kabupaten,$tahun);
		
		if($cek==0){
				//call function
				for($i=0;$i<$jumlah;$i++) {
					$data['id_komoditi_stok'] = $id_komoditi_stok[$i];
					//$data['bulan'] = $bulan;
					$data['tahun'] = $tahun;
					$data['id_kabupaten'] = $id_kabupaten;
					$data['stok'] = $stok[$i];
					$data['ketahanan'] = $ketahanan[$i];
					
					$this->m_stok->create($data);
				}
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data Stok Bahan pokok");
				
				//redirect to page
				$this->session->set_flashdata('notif','DATA STOK TERSIMPAN');
				redirect('stok/data');
		}else{
				//redirect to page
				$this->session->set_flashdata('notif2','DATA STOK SUDAH ADA');
				redirect('stok/data');
		}
        
    }
	
	 public function create_data_mingguan() {
        //get data
		$jumlah = count($this->input->post('id_komoditi_stok'));
		$id_komoditi_stok = $this->input->post('id_komoditi_stok');
		$bulan = $this->input->post('bulan');
		$minggu = $this->input->post('minggu');
		$tahun = $this->input->post('tahun');
		$id_kabupaten = $this->input->post('id_kabupaten');
		$stok = $this->input->post('stok');
		$ketahanan = $this->input->post('ketahanan');
		echo $jumlah;
        
		$cek = $this->m_stok->cekStokMingguan($id_kabupaten,$bulan,$minggu,$tahun);
		
		if($cek==0){
				//call function
				for($i=0;$i<$jumlah;$i++) {
					$data['id_komoditi_stok'] = $id_komoditi_stok[$i];
					$data['bulan'] = $bulan;
					$data['minggu'] = $minggu;
					$data['tahun'] = $tahun;
					$data['id_kabupaten'] = $id_kabupaten;
					$data['stok'] = $stok[$i];
					$data['ketahanan'] = $ketahanan[$i];
					
					$this->m_stok->create_mingguan($data);
				}
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data Stok Mingguan Bahan pokok");
				
				//redirect to page
				$this->session->set_flashdata('notif','DATA STOK TERSIMPAN');
				redirect('stok/data_mingguan');
		}else{
				//redirect to page
				$this->session->set_flashdata('notif2','DATA STOK SUDAH ADA');
				redirect('stok/data_mingguan');
		}
        
    }
    
    public function update_data() {
        //get data
		$data['id_stok'] = $this->input->post('id_stok');
		$data['stok'] = $this->input->post('stok');
		$data['ketahanan'] = $this->input->post('ketahanan');
        
		if(empty($this->input->post('id_stok'))){
				
		}else{
			//call function
			$this->m_stok->update($data);
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Update Data Stok Bahan pokok dengan id_stok = ".$data['id_stok']);
		}
	    
       //result kembali
        redirect("/stok/result2?id_kabupaten=".$this->input->post('id_kabupaten')."&&tahun=".$this->input->post('tahun'));	
        
    }
    
    public function update_data_mingguan() {
        //get data
		$data['id_stok'] = $this->input->post('id_stok');
		$data['stok'] = $this->input->post('stok');
		$data['ketahanan'] = $this->input->post('ketahanan');
        
		if(empty($this->input->post('id_stok'))){
				
		}else{
			//call function
			$this->m_stok->update_mingguan($data);
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Update Data Stok Mingguan Bahan pokok dengan id_stok = ".$data['id_stok']);
		}
	    
       //result kembali
        redirect("/stok/result3?id_kabupaten=".$this->input->post('id_kabupaten')."&&bulan=".$this->input->post('bulan')."&&minggu=".$this->input->post('minggu')."&&tahun=".$this->input->post('tahun'));	
        		
        
    }
    
    public function delete() {
                
        if($this->input->get('id')!="") {
            $this->m_stok->delete($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data Stok Bahan Pokok dengan id_stok = ".$this->input->get('id'));
        }
        
        //result kembali
        redirect("/stok/result2?id_kabupaten=".$this->input->post('id_kabupaten')."&&tahun=".$this->input->post('tahun'));	
        	
        
    }
      
    public function delete_mingguan() {
                
        if($this->input->get('id')!="") {
            $this->m_stok->delete_mingguan($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data Stok Mingguan Bahan Pokok dengan id_stok = ".$this->input->get('id'));
        }
        
         //result kembali
        redirect("/stok/result3?id_kabupaten=".$this->input->get('id_kabupaten')."&&bulan=".$this->input->get('bulan')."&&minggu=".$this->input->get('minggu')."&&tahun=".$this->input->get('tahun'));	
        	
        
    }
      
}
