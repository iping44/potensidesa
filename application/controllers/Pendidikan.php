<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikan extends CI_Controller {

    public function __construct(){
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_menu');
		$this->load->model('m_submenu');
		$this->load->model('m_pendidikan');
		$this->load->model('m_group');
		$this->load->model('m_login');

		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }

		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Setting")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('admin/header');
			$this->load->view('admin/notification');
			$this->load->view('admin/menu', $data);
			$this->load->view('admin/forbidden');
			$this->load->view('admin/footer');
		}
    }

    public function data(){

		//get id
		$data["id"] = $this->input->get('id');

		//config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/pendidikan/data";
		$config["total_rows"] = $this->m_pendidikan->record_count($data["id"]);
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
    $config['full_tag_close'] = '</ul>';
    $config['first_link'] = false;
    $config['last_link'] = false;
    $config['first_tag_open'] = '<li>';
    $config['first_tag_close'] = '</li>';
    $config['prev_link'] = '&laquo';
    $config['prev_tag_open'] = '<li class="prev">';
    $config['prev_tag_close'] = '</li>';
    $config['next_link'] = '&raquo';
    $config['next_tag_open'] = '<li>';
    $config['next_tag_close'] = '</li>';
    $config['last_tag_open'] = '<li>';
    $config['last_tag_close'] = '</li>';
    $config['cur_tag_open'] = '<li class="active"><a href="#">';
    $config['cur_tag_close'] = '</a></li>';
    $config['num_tag_open'] = '<li>';
    $config['num_tag_close'] = '</li>';

		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["pendidikan"] = $this->m_pendidikan->fetch_pendidikan($config["per_page"], $page, $data["id"]);
		$data["links"] = $this->pagination->create_links();

		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
    $data["pendidikan"]=$this->m_pendidikan->data($this->input->get('id'));
		$data["id"] = $this->input->get('id');
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('module_pendidikan/pendidikan', $data);
        $this->load->view('admin/footer');
    }

	public function result(){

        //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/m_pendidikan/result";
		$config["total_rows"] = $this->m_pendidikan->record_count_search($this->input->post('key'), $this->input->post('id'));
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
    $config["num_links"] = floor($choice);

		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["pendidikan"] = $this->m_pendidikan->search_pendidikan($config["per_page"], $page, $this->input->post('key'), $this->input->post('id'));
		if($data["pendidikan"] == null) {
			$data["links"] = "";
		} else {
			$data["links"] = $this->pagination->create_links();
		}
    $data['id']=$this->input->post('id');
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
    $this->load->view('admin/header');
    $this->load->view('admin/notification');
    $this->load->view('admin/menu', $data);
    $this->load->view('module_pendidikan/pendidikan', $data);
    $this->load->view('admin/footer');
    }

    public function insert(){

  		//get id
  		$data["id"] = $this->input->get('id');

  		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
  		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));

  		$data['menus'] = $this->m_menu->data();
  		$this->load->view('admin/header');
          $this->load->view('admin/notification');
          $this->load->view('admin/menu', $data);
          $this->load->view('module_pendidikan/insert', $data);
          $this->load->view('admin/footer');
    }

    public function update(){

		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
    $data['entry'] =  $this->m_pendidikan->get($this->input->get('id1'), $this->input->get('id2'));
		$data['menus'] = $this->m_menu->data();
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('m_pendidikan/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('module_pendidikan/update', $data);
            $this->load->view('admin/footer');
        }

    }

    public function detail(){

		$data['menu'] = $this->m_menu->menu($this->session->userdata('desa_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('desa_id'));
		$data['menus'] = $this->m_menu->data();
    $data['entry'] =  $this->m_pendidikan->get($this->input->get('id1'), $this->input->get('id2'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('m_pendidikan/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('module_pendidikan/detail', $data);
            $this->load->view('admin/footer');
        }
    }

    public function create_data() {

        //get data
		    $data['desa_id'] = $this->input->post('desa_id');
        $data['pendidikan_id'] = $this->input->post('pendidikan_id');
        $data['sd'] = $this->input->post('sd');
        $data['smp'] = $this->input->post('smp');
		    $data['sma'] = $this->input->post('sma');
        $data['s1'] = $this->input->post('s1');
        $data['s2'] = $this->input->post('s2');
        $data['s3'] = $this->input->post('s3');
        $data['non'] = $this->input->post('non');



        //call function
        $this->m_pendidikan->create($data);

		// //log system
		// $this->m_log->create($this->session->userdata('userid'),
		// 	"Insert Data Hak akses dengan group_id = ".$data['group_id']);

        //redirect to page
        redirect('pendidikan/data?id='.$data['desa_id']);

    }

    public function update_data() {

        //get data
        $data['desa_id'] = $this->input->post('desa_id');
        $data['pendidikan_id'] = $this->input->post('pendidikan_id');
        $data['sd'] = $this->input->post('sd');
        $data['smp'] = $this->input->post('smp');
		    $data['sma'] = $this->input->post('sma');
        $data['s1'] = $this->input->post('s1');
        $data['s2'] = $this->input->post('s2');
        $data['s3'] = $this->input->post('s3');
        $data['non'] = $this->input->post('non');

        //call function
        $this->m_pendidikan->update($data);

		// //log system
		// $this->m_log->create($this->session->userdata('userid'),
		// 	"Update Data Hak akses dengan group_id = ".$data['submenu_id']);

        //redirect to page
        redirect('pendidikan/data?id='.$data['desa_id']);

    }

    public function delete() {

        if($this->input->get('id1')!="") {
            $this->m_pendidikan->delete($this->input->get('id1'), $this->input->get('id2'));

			// //log system
			// $this->m_log->create($this->session->userdata('userid'),
			// 	"Hapus Data m_pendidikan dengan group_id = ".$this->input->get('id1'));
        }

        //redirect to page
        redirect('pendidikan/data?id='.$this->input->get('id1'));

    }

}
