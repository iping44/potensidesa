<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    public function __construct(){
        parent::__construct();
    		error_reporting(0);
    		$this->load->model('m_log');
    		$this->load->model('m_menu');
    		$this->load->model('m_login');

    		//cek login
    		if(!$this->session->userdata('userid')) {
          $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
          redirect('login');


          }

    		//cek hak akses
    		if(!$this->m_login->access($this->session->userdata('group_id'), "Setting")) {
    			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
    			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));

          //view
    			$this->load->view('admin/header');
    			$this->load->view('admin/notification');
    			$this->load->view('admin/menu', $data);
    			$this->load->view('admin/forbidden');
    			$this->load->view('admin/footer');
    		}

    }

    public function data(){

    		//config for pagination
    		$config = array();
    		$config["base_url"] = base_url()."index.php/menu/data";
    		$config["total_rows"] = $this->m_menu->record_count();
    		$config["per_page"] = 30;
    		$config["uri_segment"] = 3;
    		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;

    		//config css for pagination
    		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

    		if($this->uri->segment(3)=="") {
    			$data['number']=0;
    		} else {
    			$data['number'] = $this->uri->segment(3);
    		}

    		$this->pagination->initialize($config);
    		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;

        //select data from table_menu
    		$data["mmenu"] = $this->m_menu->fetch_menu($config["per_page"], $page);
    		$data["links"] = $this->pagination->create_links();

        //select menu for navigation
    		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
    		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        //view module
        $this->load->view('module_menu/menu', $data);
        $this->load->view('admin/footer');
    }

	public function result(){

		//config for pagination
		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;

		$config = array();
		$config["base_url"] = base_url()."menu/result/".$search;
		$config["total_rows"] = $this->m_menu->record_count_search($search);
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;

		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;
    //select data from table_menu
		$data["mmenu"] = $this->m_menu->search_menu($config["per_page"], $page, $search);
		$data["links"] = $this->pagination->create_links();

    //select menu for navigation
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));

		$this->load->view('admin/header');
		$this->load->view('admin/notification');
		$this->load->view('admin/menu', $data);
    //view module
		$this->load->view('module_menu/menu', $data);
		$this->load->view('admin/footer');

	}

    public function insert(){
        //select menu for navigation
    		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
    		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));

        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        //view module
        $this->load->view('module_menu/insert');
        $this->load->view('admin/footer');
    }

    public function update(){
        //select menu for navigation
    		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
    		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        //get data with parameter
        $data['entry'] =  $this->m_menu->get($this->input->get('id'));
        //if data is null
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            //back to view data
            redirect('menu/data');
        } else {
            //set data
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            //view module
            $this->load->view('module_menu/update', $data);
            $this->load->view('admin/footer');
        }

    }

    public function detail(){
        //select menu for navigation
    		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
    		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        //get data with parameter
        $data['entry'] =  $this->m_menu->get($this->input->get('id'));

        //if data is null
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            //back to view data
            redirect('menu/data');
        } else {
            //set data
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            //view module
            $this->load->view('module_menu/detail', $data);
            $this->load->view('admin/footer');
        }
    }

    public function create_data() {

        //init data
        $data['menu_id'] = "menu".rand(999, 100);
        $data['menu_name'] = $this->input->post('menu_name');
        $data['attribute'] = $this->input->post('attribute');
        $data['link'] = $this->input->post('link');
        $data['active'] = $this->input->post('active');
    		$data['adminmenu'] = $this->input->post('adminmenu');
    		$data['position'] = $this->input->post('position');
        $data['description'] = $this->input->post('description');
        //call insert model
        $this->m_menu->create($data);

    		//log system
    		$this->m_log->create($this->session->userdata('userid'),
    		 	"Insert Data Menu dengan menu_id = ".$data['menu_id']);

        //redirect to page
        redirect('menu/data');

    }

    public function update_data() {

        //get data
        $data['menu_id'] = $this->input->post('menu_id');
        $data['menu_name'] = $this->input->post('menu_name');
        $data['attribute'] = $this->input->post('attribute');
		    $data['link'] = $this->input->post('link');
        $data['active'] = $this->input->post('active');
    		$data['adminmenu'] = $this->input->post('adminmenu');
    		$data['position'] = $this->input->post('position');
        $data['description'] = $this->input->post('description');
        //call function
        $this->m_menu->update($data);

    		// log system
    		$this->m_log->create($this->session->userdata('userid'),
    		"Update Data Menu dengan menu_id = ".$data['menu_id']);

        //redirect to page
        redirect('menu/data');

    }

    public function delete() {

        if($this->input->get('id')!="") {
            $this->m_menu->delete($this->input->get('id'));

    			//log system
    			$this->m_log->create($this->session->userdata('userid'),
    			"Hapus Data Menu dengan menu_id = ".$this->input->get('id'));
        }

        //redirect to page
        redirect('menu/data');

    }

}
