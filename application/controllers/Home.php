<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct() {
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_menu');
		$this->load->model('m_home');
		$this->load->model('m_kabupaten');
		$this->load->model('m_banner');
		$this->load->model('m_jurnal');
		$this->load->model('m_jenisjurnal');
		$this->load->model('m_agenda');
		$this->load->model('m_jenisagenda');
		$this->load->model('m_jasakip');
		$this->load->model('m_jenisjasakip');
		$this->load->model('m_jadwal');
		$this->load->model('m_jamker');
		$this->load->model('m_testi');
		$this->load->model('m_about');
		$this->load->model('m_struktur');
		$this->load->model('m_faq');
		$this->load->model('m_foto');
		$this->load->model('m_video');
		$this->load->model('m_fasilitas');
		$this->load->model('m_layanan');
		$this->load->model('m_berita');
		$this->load->model('m_info');
    }

	 public function index() {

		$data['nama_user'] = $this->session->userdata('fullname');
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		//$data['dokter'] = $this->m_dokter->dokter();
		//=========================================
		$data['struktur'] = $this->m_struktur->struktur();
		//=========================================
		$data['berita'] = $this->m_berita->berita();
		//=========================================
		$data['info'] = $this->m_info->info();
		//=========================================


        $this->load->view('admin/header');
        $this->load->view('admin/notification',$data);
        $this->load->view('admin/menu', $data);
				$this->load->view('admin/home');

        $this->load->view('admin/footer');
    }


	public function beranda()
	{
		$data['jad'] = $this->m_jadwal->jadwal();
		$data['jam'] = $this->m_jamker->jamker();
		$data['tes'] = $this->m_testi->testi();
		$data['fas'] = $this->m_fasilitas->fasilitas();
		$data['news'] = $this->m_berita->berita();
		$data['jadwal'] = $this->m_jadwal->jadwal($this->input->get('jadwal'));
		$data['jamker'] = $this->m_jamker->jamker($this->input->get('jamker'));
		$data['testi'] = $this->m_testi->testi($this->input->get('testi'));
		$data['fasilitas'] = $this->m_fasilitas->fasilitas($this->input->get('fasilitas'));
		$data['berita'] = $this->m_berita->berita($this->input->get('berita'));
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('slider');
		$this->load->view('content1', $data);
		//$this->load->view('content2', $data);
		$this->load->view('content3', $data);
		$this->load->view('footer');
	}

	public function berita()
	{
		$data['berita'] = $this->m_berita->get($this->uri->segment(3));
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('berita', $data);
		$this->load->view('footer');
	}

	public function tentang()
	{
		$data['ttg'] = $this->m_about->about();
		$data['str'] = $this->m_struktur->struktur();
		$data['about'] = $this->m_about->about($this->input->get('about'));
		$data['struktur'] = $this->m_struktur->struktur($this->input->get('struktur'));
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('about', $data);
		$this->load->view('footer');
	}

	public function layanan()

	{
		$data['lay'] = $this->m_layanan->layanan();
		$data['layanan'] = $this->m_layanan->layanan($this->input->get('layanan'));

		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('layanan', $data);
		$this->load->view('footer');
	}

	public function dokter()
	{
		$data['dok'] = $this->m_dokter->dokter();
		$data['nama_dokter'] = $this->m_dokter->dokter($this->input->get('nama_dokter'));
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('dokter', $data);
		$this->load->view('footer');
	}

	public function faq()
	{
		$data['ask'] = $this->m_faq->faq();
		$data['faq'] = $this->m_faq->faq($this->input->get('faq'));
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('faq', $data);
		$this->load->view('footer');
	}

	public function galeri()
	{
		$data['img'] = $this->m_foto->foto();
		$data['vid'] = $this->m_video->video();
		$data['foto'] = $this->m_foto->foto($this->input->get('img'));
		$data['video'] = $this->m_video->video($this->input->get('vid'));
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('galeri', $data);
		$this->load->view('footer');
	}

	public function info()
	{
		$data['inf'] = $this->m_info->info();
		$data['info'] = $this->m_info->info($this->input->get('inf'));
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('info_sehat', $data);
		$this->load->view('footer');
	}

	public function litbang()
	{
		$data['lit'] = $this->m_jurnal->jurnal();
		$data['jenisjurnal'] = $this->m_jenisjurnal->jenisjurnal();
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('litbang', $data);
		$this->load->view('footer');
	}

	public function agenda()
	{
		$data['lit'] = $this->m_agenda->agenda();
		$data['jenisagenda'] = $this->m_jenisagenda->jenisagenda();
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('agenda', $data);
		$this->load->view('footer');
	}

	public function jasakip()
	{
		$data['lit'] = $this->m_jasakip->jasakip();
		$data['banner'] = $this->m_banner->banner();
		$data['jenisjasakip'] = $this->m_jenisjasakip->jenisjasakip();
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('jasakip', $data);
		$this->load->view('footer');
	}

	public function info2()
	{
		$data['info'] = $this->m_info->get($this->uri->segment(3));
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('info_sehat_2', $data);
		$this->load->view('footer');
	}

	public function litbang2()
	{
		$data['lit'] = $this->m_jurnal->get_jenis($this->uri->segment(3));
		$data['jenisjurnal'] = $this->m_jenisjurnal->jenisjurnal();
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('litbang2', $data);
		$this->load->view('footer');
	}

	public function agenda2()
	{
		$data['lit'] = $this->m_agenda->get_jenis($this->uri->segment(3));
		$data['jenisagenda'] = $this->m_jenisagenda->jenisagenda();
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('agenda2', $data);
		$this->load->view('footer');
	}

	public function jasakip2()
	{
		$data['lit'] = $this->m_jasakip->get_jenis($this->uri->segment(3));
		$data['banner'] = $this->m_banner->banner();
		$data['jenisjasakip'] = $this->m_jenisjasakip->jenisjasakip();
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('jasakip2', $data);
		$this->load->view('footer');
	}

	public function kategori()
	{
		if($this->uri->segment(3)==1) {
			$idd = "Berita";
		} else if($this->uri->segment(3)==2) {
			$idd = "Artikel";
		} else if($this->uri->segment(3)==3) {
			$idd = "Promosi UMKM";
		} else if($this->uri->segment(3)==4) {
			$idd = "Informasi Penelitian ";
		}

		$data['info'] = $this->m_info->get_byKategori($idd);
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('info_kategori', $data);
		$this->load->view('footer');
	}

	public function hubungi()
	{
		$this->load->view('header');
		$this->load->view('nav');
		$this->load->view('hubungi_kami');
		$this->load->view('footer');
	}
}
