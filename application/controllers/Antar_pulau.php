<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem perindustrian

class Antar_pulau extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_pasar');
		$this->load->model('m_komoditi');
		$this->load->model('m_komoditi_tetap');
		$this->load->model('m_strategis');
		$this->load->model('m_kabupaten');
		$this->load->model('m_pelabuhan');
		$this->load->model('m_antar_pulau');
		$this->load->model('m_login');
		$as=$this->session->userdata('userid');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Informasi")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
    }

    public function data(){
		
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_pelabuhan->data2();
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('informasi/module_antar_pulau/antar_pulau', $data);
			$this->load->view('footer');
		}else{
			$data['kabupaten'] = $this->m_pelabuhan->data3($this->session->userdata('id_kabupaten'));
			$data['kabupaten2'] = $data['kabupaten'][0];
			
			if($data['kabupaten2']->id_kabupaten==$id_kabupaten){
				$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
				$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
				$this->load->view('header');
				$this->load->view('notification');
				$this->load->view('menu', $data);
				$this->load->view('informasi/module_antar_pulau/antar_pulau', $data);
				$this->load->view('footer');
			}else{
				$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
				$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
				$this->load->view('header');
				$this->load->view('notification');
				$this->load->view('menu', $data);
				$this->load->view('informasi/module_antar_pulau/error', $data);
				$this->load->view('footer');
			}
		}
		
		
		
    }
	
	public function result(){
    
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		$data['id_kabupaten']= $this->input->post('id_kabupaten');
		$data['id_pelabuhan']= $this->input->post('id_pelabuhan');
		$data['tanggal']= $this->input->post('tanggal');
		$data["antar_pulau"] = $this->m_antar_pulau->search_antar_pulau($data['id_kabupaten'], $data['id_pelabuhan'], $data['tanggal']);
		$data['tempat'] = $this->m_kabupaten->get($data['id_kabupaten']);
		$data['tempat'] = $data['tempat'][0];
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_pelabuhan->data2();
		}else{
			$data['kabupaten'] = $this->m_pelabuhan->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('informasi/module_antar_pulau/result', $data);
		$this->load->view('footer');		
		
    }
	
	
	public function result2(){
    
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		$data['id_kabupaten']= $this->input->get('id_kabupaten');
		$data['id_pelabuhan']= $this->input->get('id_pelabuhan');
		$data['tanggal']= $this->input->get('tanggal');
		$data["antar_pulau"] = $this->m_antar_pulau->search_antar_pulau($data['id_kabupaten'], $data['id_pelabuhan'], $data['tanggal']);
		$data['tempat'] = $this->m_kabupaten->get($data['id_kabupaten']);
		$data['tempat'] = $data['tempat'][0];
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_pelabuhan->data2();
		}else{
			$data['kabupaten'] = $this->m_pelabuhan->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('informasi/module_antar_pulau/result', $data);
		$this->load->view('footer');		
		
    }
	
    public function insert(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_pelabuhan->data2();
		}else{
			$data['kabupaten'] = $this->m_pelabuhan->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['komoditi'] = $this->m_komoditi->dataStrategis();
		$data['komoditi_tetap'] = $this->m_komoditi_tetap->data();
		
		
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('informasi/module_antar_pulau/insert');
        $this->load->view('footer');
    }
    
    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_antar_pulau->get($this->input->get('id'));
		$data['komoditi_tetap'] = $this->m_komoditi_tetap->data();
		$data['pasar'] = $this->m_pasar->data();
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('strategis/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('informasi/module_antar_pulau/update', $data);
            $this->load->view('footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_antar_pulau->get($this->input->get('id'));
		$data['komoditi_tetap'] = $this->m_komoditi_tetap->data();
		$data['pasar'] = $this->m_pasar->data();
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('antar_pulau/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('informasi/module_antar_pulau/detail', $data);
            $this->load->view('footer');
        }
    }
    
    public function create_data() {
        //get data
		$jumlah = count($this->input->post('id_komoditi_tetap'));
		$id_komoditi_tetap = $this->input->post('id_komoditi_tetap');
		$id_kabupaten = $this->input->post('id_kabupaten');
		$id_pelabuhan = $this->input->post('id_pelabuhan');
		$tanggal = $this->input->post('tanggal');
		$volume = $this->input->post('volume');
		$harga = $this->input->post('harga');
		 
		$cek = $this->m_antar_pulau->cekAntarPulau($id_kabupaten,$id_pelabuhan,$tanggal);
		
		if($cek==0){
				//call function
				for($i=0;$i<$jumlah;$i++) {
					//$data['id_harga_barang'] = $id_harga_barang[$i];
					$data['id_komoditi_tetap'] = $id_komoditi_tetap[$i];
					$data['id_kabupaten'] = $id_kabupaten;
					$data['id_pelabuhan'] = $id_pelabuhan;
					$data['tanggal'] = $tanggal;
					$data['volume'] = $volume[$i];
					$data['a'] = $volume[$i];
					$data['b'] = $harga[$i];
					$data['c'] = $harga[$i] * $volume[$i];
					$data['nilai_persatuan'] = $data['b'];
					$data['nilai'] = $data['c'];
					
					//if($data['volume']==0){
					
					//} else if($data['b']==0){
					
					//}else{
					$this->m_antar_pulau->create($data);
					//}
					
					//$this->m_antar_pulau->create($data);
				}
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data Perdagangan Antar Pulau");
				
				//redirect to page
				$this->session->set_flashdata('notif','DATA PERDAGANGAN ANTAR PULAU TERSIMPAN');
				redirect('antar_pulau/data');
		}else{
				//redirect to page
				$this->session->set_flashdata('notif2','DATA PERDAGANGAN ANTAR PULAU SUDAH ADA');
				redirect('antar_pulau/data');
		
		}
        
    }
    
    public function update_data() {
        //get data
		$data['id_antar_pulau'] = $this->input->post('id_antar_pulau');
		$data['volume'] = $this->input->post('volume');
		$data['nilai_persatuan'] = $this->input->post('nilai_persatuan');
		$data['nilai'] = $data['volume'] * $data['nilai_persatuan'];
        
        //call function
        $this->m_antar_pulau->update($data);
		
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Update Data Perdagangan antar Pulau dengan id_antar_pulau = ".$data['id_antar_pulau']);
        
       //result kembali
        redirect("/antar_pulau/result2?id_kabupaten=".$this->input->post('id_kabupaten')."&&id_pelabuhan=".$this->input->post('id_pelabuhan')."&&tanggal=".$this->input->post('tanggal'));	
        
        
    }
    
    public function delete() {
                
        if($this->input->get('id')!="") {
            $this->m_antar_pulau->delete($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data Antar Pulau dengan id_antar_pulau = ".$this->input->get('id'));
        }
        
       //result kembali
        redirect("/antar_pulau/result2?id_kabupaten=".$this->input->get('id_kabupaten')."&&id_pelabuhan=".$this->input->get('id_pelabuhan')."&&tanggal=".$this->input->get('tanggal'));	
        
    }
	
	function get() {
        $id_kabupaten = $this->input->post('id_kabupaten');
        $id_pelabuhan = $this->input->post('id_pelabuhan');
        $kab = $this->m_pelabuhan->data4($id_kabupaten);
		if (count($kab[0]) != 0) {
			if ($id_pelabuhan != "") {
				foreach ($kab as $l){
					if ($l->id_pelabuhan == $id_pelabuhan) {
						$data .="<option value='$l->id_pelabuhan' selected>$l->nama_pelabuhan</option>";
					} else {
						$data .="<option value='$l->id_pelabuhan'>$l->nama_pelabuhan</option>";
					}
				}
			} else {
				$data .="<option value='' hidden>- Pelabuhan -</option>";
				foreach ($kab as $l){
					$data .="<option value='$l->id_pelabuhan'>$l->nama_pelabuhan</option>";
				}
			}
			echo $data;
		} else {
			echo $data ="<option value='' hidden>- Pelabuhan -</option>";
		}
        
    }
      
}
