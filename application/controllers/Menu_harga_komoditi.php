<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_harga_komoditi extends CI_Controller {
    public function __construct(){  
        parent::__construct();
		//error_reporting(0);
		
		$this->load->model('m_pasar');
		$this->load->model('m_pokok');
		$this->load->model('m_komoditi');
		$this->load->model('m_grafik');
	}
	
	public function harga_komoditi_pasar() {
		$data["pasar"] = $this->m_pasar->data();
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/harga_komoditi/harga_komoditi_pasar', $data);
        $this->load->view('frontpage/footer');
		
    }
	
	public function harga_komoditi_barang() {
		$data["komoditi"] = $this->m_komoditi->data();
		$data["pasar"] = $this->m_pasar->data();
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/harga_komoditi/harga_komoditi_barang', $data);
        $this->load->view('frontpage/footer');
		
    }
	
	
	public function harga_komoditi_pasar_result() {
		$id_pasar = $this->input->post("id_pasar");
		$jenis_komoditi = $this->input->post("jenis_komoditi");
		$tanggal = $this->input->post("tanggal");
		$data["tanggal2"] = $this->input->post("tanggal");
	
		$data["pasar"] = $this->m_pasar->data();
		$data["harga_komoditi"] = $this->m_pokok->data5($id_pasar,$tanggal,$jenis_komoditi);
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/harga_komoditi/harga_komoditi_pasar_result', $data);
        $this->load->view('frontpage/footer');
		
    }
	
       
	public function harga_komoditi_barang_result() {
	
		$id_pasar = $this->input->post("id_pasar");
		$id_komoditi = $this->input->post("id_komoditi");
		$bulan = $this->input->post("bulan");
		$data["komoditi"] = $this->m_komoditi->data();
	
		switch($this->input->post("bulan")){
				case '1' 	: $data['nama_bulan'] = "Januari"; break;
				case '2' 	: $data['nama_bulan'] = "Februari"; break;
				case '3' 	: $data['nama_bulan'] = "Maret"; break;
				case '4' 	: $data['nama_bulan'] = "April"; break;
				case '5' 	: $data['nama_bulan'] = "Mei"; break;
				case '6' 	: $data['nama_bulan'] = "Juni"; break;
				case '7' 	: $data['nama_bulan'] = "Juli"; break;
				case '8' 	: $data['nama_bulan'] = "Agustus"; break;
				case '9' 	: $data['nama_bulan'] = "September"; break;
				case '10' 	: $data['nama_bulan'] = "Oktober"; break;
				case '11' 	: $data['nama_bulan'] = "November"; break;
				case '12' 	: $data['nama_bulan'] = "Desember"; break;
				default 	: $data['nama_bulan'] = "Desember"; 
		
		}
		
		foreach($this->m_grafik->grafik($id_komoditi,$id_pasar, $bulan)->result_array() as $row)
		{
			 $data['grafik'][]=(double)$row['1'];
			 $data['grafik'][]=(double)$row['2'];
			 $data['grafik'][]=(double)$row['3'];
			 $data['grafik'][]=(double)$row['4'];
			 $data['grafik'][]=(double)$row['5'];
			 $data['grafik'][]=(double)$row['6'];
			 $data['grafik'][]=(double)$row['7'];
			 $data['grafik'][]=(double)$row['8'];
			 $data['grafik'][]=(double)$row['9'];
			 $data['grafik'][]=(double)$row['10'];
			 $data['grafik'][]=(double)$row['11'];
			 $data['grafik'][]=(double)$row['12'];
			 $data['grafik'][]=(double)$row['13'];
			 $data['grafik'][]=(double)$row['14'];
			 $data['grafik'][]=(double)$row['15'];
			 $data['grafik'][]=(double)$row['16'];
			 $data['grafik'][]=(double)$row['17'];
			 $data['grafik'][]=(double)$row['18'];
			 $data['grafik'][]=(double)$row['19'];
			 $data['grafik'][]=(double)$row['20'];
			 $data['grafik'][]=(double)$row['21'];
			 $data['grafik'][]=(double)$row['22'];
			 $data['grafik'][]=(double)$row['23'];
			 $data['grafik'][]=(double)$row['24'];
			 $data['grafik'][]=(double)$row['25'];
			 $data['grafik'][]=(double)$row['26'];
			 $data['grafik'][]=(double)$row['27'];
			 $data['grafik'][]=(double)$row['28'];
			 $data['grafik'][]=(double)$row['29'];
			 $data['grafik'][]=(double)$row['30'];
			 $data['grafik'][]=(double)$row['31'];
		}
		
	
		$data["pasar"] = $this->m_pasar->data();
		$data["harga_komoditi"] = $this->m_pokok->data6($id_komoditi,$id_pasar);
		
		$data['nama_komoditi'] = $this->m_grafik->nama_komoditi($id_komoditi);
		$data['nama_komoditi'] = $data['nama_komoditi'][0];
		
		$data['nama_pasar'] = $this->m_grafik->nama_pasar($id_pasar);
		$data['nama_pasar'] = $data['nama_pasar'][0];
		
		$tahun = $this->m_grafik->tanggal();
		$data['tahun'] = $tahun[0];
		
		
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/harga_komoditi/harga_komoditi_barang_result', $data);
        $this->load->view('frontpage/footer');
		
    }
	
       
}