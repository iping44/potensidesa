<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem industri

class Foto extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_foto');
		$this->load->model('m_login');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		
    }

    public function data(){
		 //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/foto/data";
		$config["total_rows"] = $this->m_foto->record_count();
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["foto"] = $this->m_foto->fetch_foto($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('galeri/module_foto/foto', $data);
        $this->load->view('admin/footer');
    }
	
	public function result(){
        
		//config for pagination
		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;
				
		$config = array();
		$config["base_url"] = base_url()."foto/result/".$search;
		$config["total_rows"] = $this->m_foto->record_count_search($search);
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
	
		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;
				
		$data["foto"] = $this->m_foto->search_foto($config["per_page"], $page, $search);
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('admin/header');
		$this->load->view('admin/notification');
		$this->load->view('admin/menu', $data);
		$this->load->view('galeri/module_foto/foto', $data);
		$this->load->view('admin/footer');	
		
    }
	
    public function create_data(){
	
		$filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/image_struktur/";
        $config['allowed_types'] = "png|jpg|pdf";
        $config['overwrite']="true";
        $config['max_size']="20000000";
        $config['file_name'] = substr(str_shuffle('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890'), 0,10);
        $this->load->library('upload', $config);
		

		if(!$this->upload->do_upload())
        {
				//get data
				
				$data['judul'] = $this->input->post('judul');
				$data['deskripsi'] = $this->input->post('deskripsi');
				
				//call function
				$this->m_foto->createnoimage($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data Berita");
		}else{
						 //get data
				$dat = $this->upload->data();
				$data['gambar'] = $dat['file_name'];            
				
				$data['judul'] = $this->input->post('judul');
				$data['deskripsi'] = $this->input->post('deskripsi');
				//call function
				$this->m_foto->create($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data foto");

		
		
		}
        //redirect to page
        redirect('foto/data');
    }
    
    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_foto->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('foto/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('galeri/module_foto/update', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_foto->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('foto/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('galeri/module_foto/detail', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function insert() {
       
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
			$this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('galeri/module_foto/insert', $data);
            $this->load->view('admin/footer');
        
    }
    
    public function update_data() {
        
       $filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/image_struktur/";
        $config['allowed_types'] = "png|jpg|pdf";
        $config['overwrite']="true";
        $config['max_size']="20000000";
        $config['file_name'] = $this->input->post('judul');
        $this->load->library('upload', $config);
		

		if(!$this->upload->do_upload())
        {
				//get data
        		$data['id_foto'] = $this->input->post('id_foto');				
				$data['judul'] = $this->input->post('judul');
				$data['deskripsi'] = $this->input->post('deskripsi');
				
				//call function
				$this->m_foto->updatenoimage($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data Berita");
		}else{
						 //get data
				$dat = $this->upload->data();
				$data['gambar'] = $dat['file_name'];
            	$data['id_foto'] = $this->input->post('id_foto');				
				$data['judul'] = $this->input->post('judul');
				$data['deskripsi'] = $this->input->post('deskripsi');
				//call function
				$this->m_foto->update($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data foto");
		
		}
        //redirect to page
        redirect('foto/data');
        
    }

    public function delete() {
                
        if($this->input->get('id')!="") {
		
			$gambar = $this->m_foto->link_gambar($this->input->get('id'));
			if ($gambar->num_rows() > 0)
			{
				$row = $gambar->row();			
				$file_gambar = $row->gambar;
				$path_file = './files/image_struktur/';
				unlink($path_file.$file_gambar);
			}
			
            $this->m_foto->delete($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data Berita dengan id_berita = ".$this->input->get('id'));
        }
        
        //redirect to page
        redirect('foto/data');
    }
      
}
