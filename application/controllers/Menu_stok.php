<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Menu_stok extends CI_Controller {
    public function __construct(){  
        parent::__construct();
		//error_reporting(0);
		
		$this->load->model('m_pasar');
		$this->load->model('m_pokok');
		$this->load->model('m_stok');
		$this->load->model('m_kabupaten');
		$this->load->model('m_grafik');
	}
	
	public function stok() {
		$data["stok"] = $this->m_stok->data();
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/stok/stok', $data);
        $this->load->view('frontpage/footer');
		
    }
       
	public function stok_result() {
	
		$data['id_komoditi_stok'] = $this->input->post("id_komoditi_stok");
		$data['tahun'] = $this->input->post("tahun");
		$data["stok"] = $this->m_stok->data();
		
		$data['grafik'] = $this->m_grafik->grafik_stok($data['id_komoditi_stok'],$data['tahun']);
		$data['kabupaten'] = $this->m_kabupaten->data2();
	
		$nama_komoditi = $this->m_stok->get2($data['id_komoditi_stok']);
		$data['nama_komoditi'] = $nama_komoditi[0];
		
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav2');
        $this->load->view('frontpage/stok/stok_result', $data);
        $this->load->view('frontpage/footer');
		
    }
	
       
}