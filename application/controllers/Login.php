<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
		//error_reporting(0);
        $this->load->model("m_login");
		$this->load->model("m_log");
        if(!empty($_SESSION['user_id']))
            redirect('home');
    }
      
    public function index(){
        //$this->login_check();
        $this->load->view("login/login");
    }
    
    public function login(){
        if($_POST) {
            $data['username'] = $this->input->post('username');
            $data['password'] = md5($this->input->post('password'));
			
          //  $data['password'] = md5($this->input->post('password'));
            $result = $this->m_login->login($data);
            if(!empty($result)) {
                $data = array(
                    'userid' => $result->userid,
                    'username' => $result->username,
					'fullname' => $result->fullname,
                    'group_id' => $result->group_id,
                    'id_kabupaten' => $result->id_kabupaten,
					'photo' => $result->photo
                );
                $this->session->set_userdata($data);
				//log system
				$this->m_log->create($data['userid'], "Login");
				
                redirect('home');
            } else {
                $this->session->set_flashdata('flash_data', 'Username or password is wrong!');
                redirect('login');
            }
        }
    }
    
    public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }
	
	/*function login() {
        //Field validation succeeded.  Validate against database
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        //query the database
        $result = $this->m_login->login2($username, $password);
        if($result) {
			$sess_array = array();
            foreach($result as $row) {
                //create the session
                $sess_array = array(
                    'userid' => $row->userid,
                    'username' => $row->username,
					'fullname' => $row->fullname,
                    'group_id' => $row->group_id,
                    'kode_instansi' => $row->kode_instansi,
                    'id_kabupaten' => $row->id_kabupaten,
					'photo' => $row->photo,
                    'login_status'=>true,
                );
                //set session with value from database
                $this->session->set_userdata($sess_array);
                redirect('home','refresh');
			}
			return TRUE;
        } else {
            //if form validate false
            redirect('home','refresh');
            return FALSE;
        }
    }
	
	 function logout() {
        $this->session->unset_userdata('ID');
        $this->session->unset_userdata('USERNAME');
        $this->session->unset_userdata('PASS');
        $this->session->unset_userdata('NAME');
        $this->session->unset_userdata('LEVEL');
        $this->session->unset_userdata('login_status');
        $this->session->set_flashdata('notif','THANK YOU FOR LOGIN IN THIS APP');
        redirect('login');
    }*/
	
    
}