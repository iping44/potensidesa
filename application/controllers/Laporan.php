<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Laporan extends CI_Controller {

	public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_pelabuhan');
		$this->load->model('m_pasar');
		$this->load->model('m_kabupaten');
		$this->load->model('m_login');
		$as=$this->session->userdata('userid');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Industri")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
    }
	
	public function data(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		/*if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}*/
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_pelabuhan->data2();
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('laporan/laporan_kabupaten', $data);
			$this->load->view('footer');
		}else{
			$data['kabupaten'] = $this->m_pelabuhan->data3($this->session->userdata('id_kabupaten'));
			$data['kabupaten2'] = $data['kabupaten'][0];
			
			if($data['kabupaten2']->id_kabupaten==$data['id_kabupaten']){
				$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
				$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
				$this->load->view('header');
				$this->load->view('notification');
				$this->load->view('menu', $data);
				$this->load->view('laporan/laporan_kabupaten', $data);
				$this->load->view('footer');
			}else{
				$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
				$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
				$this->load->view('header');
				$this->load->view('notification');
				$this->load->view('menu', $data);
				$this->load->view('laporan/laporan_kabupaten_error', $data);
				$this->load->view('footer');
			}
		}
		
		
    }
	
	public function data2(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('laporan/laporan_provinsi', $data);
        $this->load->view('footer');
		
		
    }
	
	public function data3(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('laporan/laporan_bahan_pokok', $data);
        $this->load->view('footer');
		
		
    }
	
	public function data4(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('laporan/laporan_bahan_strategis', $data);
        $this->load->view('footer');
		
		
    }
	
	public function data5(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('laporan/laporan_bahan_pokok_kab', $data);
        $this->load->view('footer');
		
		
    }
	
	public function data6(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
			$pasar2 = $data['kabupaten'][0];
		}
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('laporan/laporan_bahan_strategis_kab', $data);
        $this->load->view('footer');
		
		
    }
	
	
	public function data7(){
	
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('laporan/laporan_stok', $data);
        $this->load->view('footer');
		
    }
	
	
	
	}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */