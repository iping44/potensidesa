<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem industri

class Jadwal extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_jadwal');
		$this->load->model('m_login');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		
    }

    public function data(){
		 //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/jadwal/data";
		$config["total_rows"] = $this->m_jadwal->record_count();
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["jadwal"] = $this->m_jadwal->fetch_jadwal($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('medic/module_jadwal/jadwal', $data);
        $this->load->view('admin/footer');
    }
	
	public function result(){
        
		//config for pagination
		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;
				
		$config = array();
		$config["base_url"] = base_url()."jadwal/result/".$search;
		$config["total_rows"] = $this->m_jadwal->record_count_search($search);
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
	
		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;
				
		$data["jadwal"] = $this->m_jadwal->search_jadwal($config["per_page"], $page, $search);
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('admin/header');
		$this->load->view('admin/notification');
		$this->load->view('admin/menu', $data);
		$this->load->view('medic/module_jadwal/jadwal', $data);
		$this->load->view('admin/footer');	
		
    }
	
    public function insert(){
	
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
				
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('medic/module_jadwal/insert');
        $this->load->view('admin/footer');
    }
    
    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_jadwal->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('jadwal/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('medic/module_jadwal/update', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_jadwal->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('jadwal/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('medic/module_jadwal/detail', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function create_data() {
        //get data
		$data['id_jadwal'] = $this->input->post('id_jadwal');
		$data['nama_dokter'] = $this->input->post('nama_dokter');
		$data['senin'] = $this->input->post('senin');
        $data['selasa'] = $this->input->post('selasa');
		$data['rabu'] = $this->input->post('rabu');
		$data['kamis'] = $this->input->post('kamis');
		$data['jumat'] = $this->input->post('jumat');
		$data['sabtu'] = $this->input->post('sabtu');
        
		//call function
        $this->m_jadwal->create($data);
        
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Insert Data jadwal");
		
        //redirect to page
        redirect('jadwal/data');
        
    }
    
    public function update_data() {
        
        //get data
		$data['id_jadwal'] = $this->input->post('id_jadwal');
		$data['nama_dokter'] = $this->input->post('nama_dokter');
		$data['senin'] = $this->input->post('senin');
        $data['selasa'] = $this->input->post('selasa');
		$data['rabu'] = $this->input->post('rabu');
		$data['kamis'] = $this->input->post('kamis');
		$data['jumat'] = $this->input->post('jumat');
		$data['sabtu'] = $this->input->post('sabtu');
        
        //call function
        $this->m_jadwal->update($data);
		
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Update Data dokter dengan id_jadwal = ".$data['id_jadwal']);
        
        //redirect to page
        redirect('jadwal/data');
        
    }
    
    public function delete() {
        if($this->input->get('id')!="") {
            $this->m_jadwal->delete($this->input->get('id'));
			
			//log system
			//$this->m_log->create($this->session->userdata('userid'), 
				//"Hapus Data komoditi dengan id_komoditi = ".$this->input->get('id'));
        }
        
        //redirect to page
        redirect('jadwal/data');
        
    }
      
}
