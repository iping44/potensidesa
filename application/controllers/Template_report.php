<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Template_report extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_template_report');
		$this->load->model('m_menu');
		$this->load->model('m_group');
		$this->load->model('m_login');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Perizinan")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
		
    }

    public function data(){
        //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/template_report/data";
		$config["total_rows"] = $this->m_template_report->record_count();
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["template_report"] = $this->m_template_report->fetch_template($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('perizinan/module_template_report/template_report', $data);
        $this->load->view('footer');
    }
	
	public function result(){
        //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/template_report/result";
		$config["total_rows"] = $this->m_template_report->record_count_search($this->input->post('key'));
		$config["per_page"] = 10;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["template_report"] = $this->m_template_report->search_template($config["per_page"], $page, $this->input->post('key'));
		if($data["template_report"] == null) {
			$data["links"] = "";
		} else {
			$data["links"] = $this->pagination->create_links();
		}
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('perizinan/module_template_report/template_report', $data);
        $this->load->view('footer');
    }
	

    public function insert(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));	
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('perizinan/module_template_report/insert', $data);
        $this->load->view('footer');
    }
    
    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_template_report->get($this->input->get('id'));
		$data['group'] = $this->m_group->data_active();
		
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('perizinan/template_report/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('perizinan/module_template_report/update', $data);
            $this->load->view('footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_template_report->get($this->input->get('id'));
		$data['group'] = $this->m_group->data_active();
		
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('template_report/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('perizinan/module_template_report/detail', $data);
            $this->load->view('footer');
        }
    }
    
    public function create_data() {
        
        //get data
        $data['id_template'] = "tmp".rand(999, 100);
        $data['desain_template'] = $this->input->post('desain_template');
        $data['judul'] = $this->input->post('judul');
        $data['keterangan'] = $this->input->post('keterangan');
        
        //call function
        $this->m_template_report->create($data);
        //log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Insert Data Template dengan id_template = ".$data['id_template']);
        //redirect to page
        redirect('template_report/data');
        
    }
    
    public function update_data() {
        
        //get data
        $data['id_template'] = $this->input->post('id_template');
        $data['judul'] = $this->input->post('judul');
        $data['desain_template'] = $this->input->post('desain_template');
		$data['keterangan'] = $this->input->post('keterangan');
        
			//call function
			$this->m_template_report->update($data);
        
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Update Data Template dengan id_template = ".$data['id_template']);
		
			//redirect to page
			redirect('template_report/data');
		
        
    }
    
    public function delete() {
        
        if($this->input->get('id')!="") {
            $this->m_template_report->delete($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data Template dengan id_template = ".$this->input->get('id'));
			
        }
        //redirect to page
        redirect('template_report/data');
        
    }
      
}