<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {

    function __construct() {
        parent::__construct();
		//error_reporting(0);
        $this->load->model('m_chart');
        $this->load->model('m_profil');
        $this->load->model('m_kabupaten');
        $this->load->model('m_pasar');
        $this->load->model('m_pokok');
        $this->load->model('m_berita');
        $this->load->model('m_strategis');
		$this->load->helper('text');
    }
	
    public function index() {
		
		foreach($this->m_chart->laporanTahunan()->result_array() as $row)
		{
			$data['grafik'][]=(float)$row['Januari'];
			$data['grafik'][]=(float)$row['Februari'];
			$data['grafik'][]=(float)$row['Maret'];
			$data['grafik'][]=(float)$row['April'];
			$data['grafik'][]=(float)$row['Mei'];
			$data['grafik'][]=(float)$row['Juni'];
			$data['grafik'][]=(float)$row['Juli'];
			$data['grafik'][]=(float)$row['Agustus'];
			$data['grafik'][]=(float)$row['September'];
			$data['grafik'][]=(float)$row['Oktober'];
			$data['grafik'][]=(float)$row['November'];
			$data['grafik'][]=(float)$row['Desember'];
		}
		
		$data["profil"] = $this->m_profil->profil();
		$data["visi_misi"] = $this->m_profil->visi_misi();
		$data["berita"] = $this->m_berita->data();
		$data["profil"] = $data["profil"][0];
		$data["visi_misi"] = $data["visi_misi"][0];
		
		$data["kabupaten2"] = $this->m_kabupaten->data4();
		//$data["pasar"] = $this->m_pasar->data();
		//$pasar = $this->m_pasar->data();
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav');
		
		//$data["pokok"] = $this->m_pokok->data8();
		//$data["strategis"] = $this->m_strategis->data5();
		
		$data["pokok"] = $this->m_pokok->data10();
		$data["strategis"] = $this->m_strategis->data10();
		
														
		$this->load->view('frontpage/content2', $data);
        $this->load->view('frontpage/footer');
    }
	
	 public function beranda() {
		
		foreach($this->m_chart->laporanTahunan()->result_array() as $row)
		{
			$data['grafik'][]=(float)$row['Januari'];
			$data['grafik'][]=(float)$row['Februari'];
			$data['grafik'][]=(float)$row['Maret'];
			$data['grafik'][]=(float)$row['April'];
			$data['grafik'][]=(float)$row['Mei'];
			$data['grafik'][]=(float)$row['Juni'];
			$data['grafik'][]=(float)$row['Juli'];
			$data['grafik'][]=(float)$row['Agustus'];
			$data['grafik'][]=(float)$row['September'];
			$data['grafik'][]=(float)$row['Oktober'];
			$data['grafik'][]=(float)$row['November'];
			$data['grafik'][]=(float)$row['Desember'];
		}
		
		$data["profil"] = $this->m_profil->profil();
		$data["visi_misi"] = $this->m_profil->visi_misi();
		$data["berita"] = $this->m_berita->data();
		$data["profil"] = $data["profil"][0];
		$data["visi_misi"] = $data["visi_misi"][0];
		
		$data["kabupaten2"] = $this->m_kabupaten->data2();
		//$data["pasar"] = $this->m_pasar->data();
		//$pasar = $this->m_pasar->data();
		$this->load->view('frontpage/header');
        $this->load->view('frontpage/nav');
		
		//$data["pokok"] = $this->m_pokok->data10();
		//$data["strategis"] = $this->m_strategis->data10();
		
		$data["pokok"] = $this->m_pokok->data8();
		$data["strategis"] = $this->m_strategis->data5();
														
		$this->load->view('frontpage/content', $data);
        $this->load->view('frontpage/footer');
    }
        
        
}

