<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem perindustrian

class pokok extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_pasar');
		$this->load->model('m_komoditi');
		$this->load->model('m_kabupaten');
		$this->load->model('m_pokok');
		$this->load->model('m_login');
		$as=$this->session->userdata('userid');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Informasi")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('header');
			$this->load->view('notification');
			$this->load->view('menu', $data);
			$this->load->view('forbidden');
			$this->load->view('footer');
		}
    }

    public function data(){
	
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['pasar'] = $this->m_pasar->data();
				
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('informasi/module_pokok/pokok', $data);
        $this->load->view('footer');
    }
	
	public function result(){
       
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		$data['id_kabupaten']= $this->input->post('id_kabupaten');
		$data['id_pasar']= $this->input->post('id_pasar');
		$data['tanggal']= $this->input->post('tanggal');
		$data["pokok"] = $this->m_pokok->search_pokok($data['id_kabupaten'], $data['id_pasar'], $data['tanggal']);
		$data['tempat'] = $this->m_kabupaten->get($data['id_kabupaten']);
		$data['tempat'] = $data['tempat'][0];
		$data['tempat2'] = $this->m_pasar->get($data['id_pasar']);
		$data['tempat2'] = $data['tempat2'][0];
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['pasar'] = $this->m_pasar->data();
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('informasi/module_pokok/result', $data);
		$this->load->view('footer');		
		
    }
	
	public function result2(){
       
		$id_kabupaten = $this->session->userdata('id_kabupaten');
		$data['id_kabupaten']= $this->input->get('id_kabupaten');
		$data['id_pasar']= $this->input->get('id_pasar');
		$data['tanggal']= $this->input->get('tanggal');
		$data["pokok"] = $this->m_pokok->search_pokok($data['id_kabupaten'], $data['id_pasar'], $data['tanggal']);
		$data['tempat'] = $this->m_kabupaten->get($data['id_kabupaten']);
		$data['tempat'] = $data['tempat'][0];
		$data['tempat2'] = $this->m_pasar->get($data['id_pasar']);
		$data['tempat2'] = $data['tempat2'][0];
		
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data['pasar'] = $this->m_pasar->data();
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('header');
		$this->load->view('notification');
		$this->load->view('menu', $data);
		$this->load->view('informasi/module_pokok/result', $data);
		$this->load->view('footer');		
		
    }
	
    public function insert(){
		
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		if($this->session->userdata('group_id')=="group1000"){
					$data['pasar'] = $this->m_pasar->data();
				}else{
					$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
		}
				
		$data['komoditi'] = $this->m_komoditi->datapokok();
		$data['pokok'] = $this->m_pokok->getpokok();
				
        $this->load->view('header');
        $this->load->view('notification');
        $this->load->view('menu', $data);
        $this->load->view('informasi/module_pokok/insert');
        $this->load->view('footer');
    }
    
    public function update(){
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_pokok->get($this->input->get('id'));
		$data['komoditi'] = $this->m_komoditi->datapokok();
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
		}
		
		if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('pokok/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('informasi/module_pokok/update', $data);
            $this->load->view('footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_pokok->get($this->input->get('id'));
		$data['komoditi'] = $this->m_komoditi->data();
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['pasar'] = $this->m_pasar->data();
		}else{
			$data['pasar'] = $this->m_pasar->data2($this->session->userdata('id_kabupaten'));
		}
		
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('pokok/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('header');
            $this->load->view('notification');
            $this->load->view('menu', $data);
            $this->load->view('informasi/module_pokok/detail', $data);
            $this->load->view('footer');
        }
    }
    
    public function create_data() {
        //get data
		$jumlah = count($this->input->post('id_komoditi'));
		$id_komoditi = $this->input->post('id_komoditi');
		$id_pasar = $this->input->post('id_pasar');
		$tanggal = $this->input->post('tanggal');
		$harga = str_replace(".", "", $this->input->post('harga'));
        
		$cek = $this->m_pokok->cekHargaBarang($id_pasar,$tanggal);
		
		if($cek==0){
			//call function
			for($i=0;$i<$jumlah;$i++) {
				//$data['id_harga_barang'] = $id_harga_barang[$i];
				$data['id_komoditi'] = $id_komoditi[$i];
				$data['id_pasar'] = $id_pasar;
				$data['tanggal'] = $tanggal;
				$data['harga'] = $harga[$i];
				
				$this->m_pokok->create($data);
			}
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Insert Data Bahan pokok");	
			
			//redirect to page
			$this->session->set_flashdata('notif','DATA HARGA BAHAN POKOK TERSIMPAN');
			redirect('pokok/data');
			
		}else{
			//redirect to page
			$this->session->set_flashdata('notif2','DATA HARGA BAHAN POKOK SUDAH ADA');
			redirect('pokok/data');
			
		}
        
        
    }
    
    public function update_data() {
        //get data
		$data['id_harga_barang'] = $this->input->post('id_harga_barang');
		$data['harga'] = str_replace(".", "", $this->input->post('harga'));
        
        //call function
        $this->m_pokok->update($data);
		
		//log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Update Data Bahan pokok dengan id_harga_barang = ".$data['id_harga_barang']);
        
		 //result kembali
        redirect("/pokok/result2?id_kabupaten=".$this->input->post('id_kabupaten')."&&id_pasar=".$this->input->post('id_pasar')."&&tanggal=".$this->input->post('tanggal'));	
        
        
    }
    
    public function delete() {
                
        if($this->input->get('id')!="") {
            $this->m_pokok->delete($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data Harga Bahan Pokok dengan id_harga_barang = ".$this->input->get('id'));
        }
        
         //result kembali
        redirect("/pokok/result2?id_kabupaten=".$this->input->get('id_kabupaten')."&&id_pasar=".$this->input->get('id_pasar')."&&tanggal=".$this->input->get('tanggal'));	
        
    }
	
	function get() {
        $id_kabupaten = $this->input->post('id_kabupaten');
        $id_pasar = $this->input->post('id_pasar');
        $kab = $this->m_pasar->data2($id_kabupaten);
		if (count($kab[0]) != 0) {
			if ($id_pasar != "") {
				foreach ($kab as $l){
					if ($l->id_pasar == $id_pasar) {
						$data .="<option value='$l->id_pasar' selected>$l->nama_pasar</option>";
					} else {
						$data .="<option value='$l->id_pasar'>$l->nama_pasar</option>";
					}
				}
			} else {
				$data .="<option value='' hidden>- Pasar -</option>";
				foreach ($kab as $l){
					$data .="<option value='$l->id_pasar'>$l->nama_pasar</option>";
				}
			}
			echo $data;
		} else {
			echo $data ="<option value='' hidden>- Pasar -</option>";
		}
        
    }
      
}
