<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//controller for modul sistem industri

class Info extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_group');
		$this->load->model('m_menu');
		$this->load->model('m_info');
		$this->load->model('m_login');
		
		$this->load->helper(array('url', 'html', 'text', 'ckeditor', 'date'));
        $this->load->library(array('form_validation','session','encrypt'));
        
        $this->data['ckeditor1'] = array(		
	    //ID of the textarea that will be replaced
	    'id' 	=> 	'content',
	    'path'	=>	'asset/ckeditor',
            'config'    =>      array(
		'toolbar' => 	array(	//Setting a custom toolbar
		    array('Source','Maximize','ShowBlocks'),
                    array('Styles','Format','FontSize','Font'),
                    array('Cut','Copy','Paste'),
                    array('Save','Preview','Print','Templates'),
                    array('Bold','Italic','Underline','Strike','Subscript','Superscript'),
                    array('Undo','Redo'),
                    array('Find','Replace'),
                    array('NumberedList','BulletedList','Outdent','Indent','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'),                    
                    array('Link','Unlink','Anchor','Table','Image','Smiley'),
		    '/'
		)
	    )
        );
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		
    }

    public function data(){
		 //config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/info/data";
		$config["total_rows"] = $this->m_info->record_count();
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["info"] = $this->m_info->fetch_info($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();
		
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('posts/module_info/info', $data);
        $this->load->view('admin/footer');
    }
	
	public function result(){
        
		//config for pagination
		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;
				
		$config = array();
		$config["base_url"] = base_url()."info/result/".$search;
		$config["total_rows"] = $this->m_info->record_count_search($search);
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
	
		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;
				
		$data["info"] = $this->m_info->search_info($config["per_page"], $page, $search);
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('admin/header');
		$this->load->view('admin/notification');
		$this->load->view('admin/menu', $data);
		$this->load->view('posts/module_info/info', $data);
		$this->load->view('admin/footer');	
		
    }
	
    public function create_data(){
	
		$filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/image_struktur/";
        $config['allowed_types'] = "png|jpg|pdf";
        $config['overwrite']="true";
        $config['max_size']="20000000";
        $config['file_name'] = substr(str_shuffle('qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM1234567890'), 0,10);
        $this->load->library('upload', $config);
		

		if(!$this->upload->do_upload())
        {
				//get data
				$data['judul'] = $this->input->post('judul');
				$data['tanggal'] = $this->input->post('tanggal');
				$data['content'] = $this->input->post('content');				
				
				//call function
				$this->m_info->createnoimage($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data info");
		}else{
						 //get data
				$dat = $this->upload->data();
				$data['gambar'] = $dat['file_name'];
            
				$data['judul'] = $this->input->post('judul');
				$data['tanggal'] = $this->input->post('tanggal');
				$data['content'] = $this->input->post('content');				
				//call function
				$this->m_info->create($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data info");

		
		
		}
        //redirect to page
        redirect('info/data');
    }
    
    public function update(){
		
		$data['editor']=$this->data;
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_info->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('info/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('posts/module_info/update', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_info->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('info/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('posts/module_info/detail', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function insert() {
       
		$data['editor']=$this->data;
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
			$this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('posts/module_info/insert', $data);
            $this->load->view('admin/footer');
        
    }
    
    public function update_data() {
        
       $filename = $this->input->post('userfile');
        $config['upload_path'] = "./files/image_struktur/";
        $config['allowed_types'] = "png|jpg|pdf";
        $config['overwrite']="true";
        $config['max_size']="20000000";
        $config['file_name'] = $this->input->post('judul');
        $this->load->library('upload', $config);
		

		if(!$this->upload->do_upload())
        {
				//get data
        		$data['id_info'] = $this->input->post('id_info');
				$data['judul'] = $this->input->post('judul');
				$data['tanggal'] = $this->input->post('tanggal');
				$data['content'] = $this->input->post('content');				
				
				//call function
				$this->m_info->updatenoimage($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data info");
		}else{
						 //get data
				$dat = $this->upload->data();
				$data['gambar'] = $dat['file_name'];
            	$data['id_info'] = $this->input->post('id_info');
				$data['judul'] = $this->input->post('judul');
				$data['tanggal'] = $this->input->post('tanggal');
				$data['content'] = $this->input->post('content');
				
				//call function
				$this->m_info->update($data);
				
				//log system
				$this->m_log->create($this->session->userdata('userid'), 
					"Insert Data info");
		
		}
        //redirect to page
        redirect('info/data');
        
    }

    public function delete() {
                
        if($this->input->get('id')!="") {
		
			$gambar = $this->m_info->link_gambar($this->input->get('id'));
			if ($gambar->num_rows() > 0)
			{
				$row = $gambar->row();			
				$file_gambar = $row->gambar;
				$path_file = './files/image_struktur/';
				unlink($path_file.$file_gambar);
			}
			
            $this->m_info->delete($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data info dengan id_info = ".$this->input->get('id'));
        }
        
        //redirect to page
        redirect('info/data');
    }
      
}
