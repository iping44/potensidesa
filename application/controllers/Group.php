<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Group extends CI_Controller {

    public function __construct(){
        parent::__construct();
    		error_reporting(0);
    		$this->load->model('m_log');
    		$this->load->model('m_login');
    		$this->load->model('m_group');
    		$this->load->model('m_menu');

		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }

		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Setting")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('admin/header');
			$this->load->view('admin/notification');
			$this->load->view('admin/menu', $data);
			$this->load->view('admin/forbidden');
			$this->load->view('admin/footer');
		}
    }

    public function data(){

		//config for pagination
		$config = array();
		$config["base_url"] = base_url()."index.php/group/data";
		$config["total_rows"] = $this->m_group->record_count();
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;

		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		$data["group"] = $this->m_group->fetch_group($config["per_page"], $page);
		$data["links"] = $this->pagination->create_links();

		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('module_group/group', $data);
        $this->load->view('admin/footer');
    }

	public function result(){

		//config for pagination

		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;

		$config = array();
		$config["base_url"] = base_url()."group/result/".$search;
		$config["total_rows"] = $this->m_group->record_count_search($search);
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;

		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}

		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;

		$data["group"] = $this->m_group->search_group($config["per_page"], $page, $search);

		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('admin/header');
		$this->load->view('admin/notification');
		$this->load->view('admin/menu', $data);
		$this->load->view('module_group/group', $data);
		$this->load->view('admin/footer');

    }



    public function insert(){

		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('module_group/insert');
        $this->load->view('admin/footer');
    }

    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_group->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('group/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('module_group/update', $data);
            $this->load->view('admin/footer');
        }
    }

    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_group->get($this->input->get('id'));
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('group/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('module_group/detail', $data);
            $this->load->view('admin/footer');
        }
    }

    public function create_data() {

        //get data
        $data['group_id'] = "group".rand(999, 100);
        $data['group_name'] = $this->input->post('group_name');
        $data['description'] = $this->input->post('description');
        $data['active'] = $this->input->post('active');

        //call function
        $this->m_group->create($data);

		//log system
		$this->m_log->create($this->session->userdata('userid'),
			"Insert Data Group dengan group_id = ".$data['group_id']);

        //redirect to page
        redirect('group/data');

    }

    public function update_data() {

        //get data
        $data['group_id'] = $this->input->post('group_id');
        $data['group_name'] = $this->input->post('group_name');
        $data['description'] = $this->input->post('description');
        $data['active'] = $this->input->post('active');

        //call function
        $this->m_group->update($data);

		//log system
		$this->m_log->create($this->session->userdata('userid'),
			"Update Data Grup dengan group_id = ".$data['group_id']);

        //redirect to page
        redirect('group/data');

    }

    public function delete() {

        if($this->input->get('id')!="") {
            $this->m_group->delete($this->input->get('id'));

			//log system
			$this->m_log->create($this->session->userdata('userid'),
				"Hapus Data Group dengan group_id = ".$this->input->get('id'));
        }


        //redirect to page
        redirect('group/data');

    }

}
