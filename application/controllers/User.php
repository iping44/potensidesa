<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
    
    public function __construct(){  
        parent::__construct();
		error_reporting(0);
		$this->load->model('m_log');
		$this->load->model('m_user');
		$this->load->model('m_menu');
		$this->load->model('m_group');
		$this->load->model('m_instansi');
		$this->load->model('m_login');
		$this->load->model('m_kabupaten');
		//$this->load->library('encryption');
		//$this->load->library('encrypt');
		
		//cek login
		if(!$this->session->userdata('userid')) {
            $this->session->set_flashdata('flash_data', 'Anda Tidak Mempunyai Hak Akses!');
            redirect('login');
        }
		
		//cek hak akses
		if(!$this->m_login->access($this->session->userdata('group_id'), "Setting")) {
			$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
			$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
			$this->load->view('admin/header');
			$this->load->view('admin/notification');
			$this->load->view('admin/menu', $data);
			$this->load->view('admin/forbidden');
			$this->load->view('admin/footer');
		}
		
		
    }

    public function data(){
        //config for pagination
		$config = array();
		$config["base_url"] = base_url()."user/data";
		
		if($this->session->userdata('group_id')=="group1000"){
			$config["total_rows"] = $this->m_user->record_count();
		}else{
			$config["total_rows"] = $this->m_user->record_count2($this->session->userdata('id_kabupaten'));
		}
		
		$config["per_page"] = 30;
		$config["uri_segment"] = 3;
		$choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = 'First';
        $config['last_link'] = 'Last';
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = 'Previous';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = 'Next';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(3)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(3);
		}
		
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(3))?$this->uri->segment(3) : 0;
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		if($this->session->userdata('group_id')=="group1000"){
			$data["user"] = $this->m_user->fetch_user($config["per_page"], $page);
		}else{
			$data["user"] = $this->m_user->fetch_user2($config["per_page"], $page, $this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		$data["links"] = $this->pagination->create_links();
		
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('module_user/user', $data);
        $this->load->view('admin/footer');
    }
	
	public function result(){
	
		//config for pagination
		$search = ($this->input->post("key"))? $this->input->post("key") : "NIL";
		$search = ($this->uri->segment(3)) ? $this->uri->segment(3) : $search;
				
		$config = array();
		$config["base_url"] = base_url()."user/result/".$search;
		
		if($this->session->userdata('group_id')=="group1000"){
			$config["total_rows"] = $this->m_user->record_count3($search);
		}else{
			$config["total_rows"] = $this->m_user->record_count4($this->session->userdata('id_kabupaten'), $search);
		}
				
		$config["per_page"] = 30;
		$config["uri_segment"] = 4;
		$choice = $config["total_rows"] / $config["per_page"];
		$config["num_links"] = 2;
		
		//config css for pagination
		$config['full_tag_open'] = '<ul class="pagination">';
		$config['full_tag_close'] = '</ul>';
		$config['first_link'] = 'First';
		$config['last_link'] = 'Last';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['prev_link'] = 'Previous';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';
		$config['next_link'] = 'Next';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['cur_tag_open'] = '<li class="active"><a href="#">';
		$config['cur_tag_close'] = '</a></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		
		if($this->uri->segment(4)=="") {
			$data['number']=0;
		} else {
			$data['number'] = $this->uri->segment(4);
		}
	
		$this->pagination->initialize($config);
		$page = ($this->uri->segment(4))?$this->uri->segment(4) : 0;
				
		if($this->session->userdata('group_id')=="group1000"){
			$data["user"] = $this->m_user->search_user($config["per_page"], $page, $search);
		}else{
			$data["user"] = $this->m_user->search_user2($config["per_page"], $page, $search, $this->session->userdata('id_kabupaten'));
		}
				
		$data["links"] = $this->pagination->create_links();
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		$this->load->view('admin/header');
		$this->load->view('admin/notification');
		$this->load->view('admin/menu', $data);
		$this->load->view('module_user/user', $data);
		$this->load->view('admin/footer');		

    }
	

    public function insert(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data["group"]=$this->m_group->data();
		}else{
			$data["group"]=$this->m_group->data2();
		}
		
        $this->load->view('admin/header');
        $this->load->view('admin/notification');
        $this->load->view('admin/menu', $data);
        $this->load->view('module_user/insert', $data);
        $this->load->view('admin/footer');
    }
    
    public function update(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_user->get($this->input->get('id'));
		
		$data['id_kabupaten'] = $this->session->userdata('id_kabupaten');
		$data['group_id'] = $this->session->userdata('group_id');
		if($this->session->userdata('group_id')=="group1000"){
			$data['kabupaten'] = $this->m_kabupaten->data2();
		}else{
			$data['kabupaten'] = $this->m_kabupaten->data3($this->session->userdata('id_kabupaten'));
			$kabupaten2 = $data['kabupaten'][0];
		}
		
		if($this->session->userdata('group_id')=="group1000"){
			$data["group"]=$this->m_group->data_active();
		}else{
			$data["group"]=$this->m_group->data_active2();
		}
		
		if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('user/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('module_user/update', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function detail(){
		$data['menu'] = $this->m_menu->menu($this->session->userdata('group_id'));
		$data['submenu'] = $this->m_menu->submenu($this->session->userdata('group_id'));
        $data['entry'] =  $this->m_user->get($this->input->get('id'));
		$data['group'] = $this->m_group->data_active();
		$data["instansi"]=$this->m_instansi->data();
        if(!isset($data['entry'][0]) || $data['entry'][0] == ""){
            redirect('user/data');
        } else {
            $data['entry'] = $data['entry'][0];
            $this->load->view('admin/header');
            $this->load->view('admin/notification');
            $this->load->view('admin/menu', $data);
            $this->load->view('module_user/detail', $data);
            $this->load->view('admin/footer');
        }
    }
    
    public function create_data() {
        
        //get data
        $data['userid'] = "usr".rand(999, 100);
        $data['username'] = $this->input->post('username');
		$data['fullname'] = $this->input->post('fullname');
  		//$enkripsi = md5($this->input->post('password'));
		$data['password'] = md5($this->input->post('password'));
	    $data['email'] = $this->input->post('email');
        $data['group_id'] = $this->input->post('group_id');
        $data['register_date'] = date('Y-m-d');
        $data['register_time'] = date('H:i:s');
		$data['nama_instansi'] = $this->input->post('nama_instansi');
		$data['id_kabupaten'] = $this->input->post('id_kabupaten');
        $data['active'] = $this->input->post('active');
       
	   //call function
        $this->m_user->create($data);
        //log system
		$this->m_log->create($this->session->userdata('userid'), 
			"Insert Data User dengan userid = ".$data['userid']);
        //redirect to page
		redirect('user/data');
		
    }
    
    public function update_data() {
        
        //get data
        $data['userid'] = $this->input->post('userid');
        $data['username'] = $this->input->post('username');
		$data['fullname'] = $this->input->post('fullname');
		$data['email'] = $this->input->post('email');
        $data['active'] = $this->input->post('active');
		$data['nama_instansi'] = $this->input->post('nama_instansi');
		$data['id_kabupaten'] = $this->input->post('id_kabupaten');
		$data['group_id'] = $this->input->post('group_id');
		$data['id_kabupaten'] = $this->input->post('id_kabupaten');
		
		//set up password field
		$oldpassword = $this->input->post('oldpassword');
		$checkoldpassword = md5($this->input->post('checkoldpassword'));
		$password = md5($this->input->post('password'));
		$matchpassword = md5($this->input->post('matchpassword'));
		
		if($oldpassword==$checkoldpassword){
			//echo "password benar";
			if($password==$matchpassword){
			
				if($this->input->post('password')==""){
					//call function
					$this->m_user->update_nopassword($data);	

					//log system
					$this->m_log->create($this->session->userdata('userid'), 
					"Update Data User dengan userid = ".$data['userid']);
					
					redirect('user/data');
				}else{
					$data['password'] = md5($this->input->post('password'));
					//call function
					$this->m_user->update_withpassword($data);	

					//log system
					$this->m_log->create($this->session->userdata('userid'), 
					"Update Data User dengan userid = ".$data['userid']);
					
					redirect('user/data');
				}
						
			}else{
				echo "Password Baru Tidak Cocok";
			}
		}else{
			echo "Password Lama Salah";
		}
		
        
    }
    
    public function delete() {
        
        if($this->input->get('id')!="") {
            $this->m_user->delete($this->input->get('id'));
			
			//log system
			$this->m_log->create($this->session->userdata('userid'), 
				"Hapus Data User dengan userid = ".$this->input->get('id'));
			
        }
        //redirect to page
        redirect('user/data');
        
    }
	
	public function encrypt(){
         //sebuah string yang akan kita enkripsi
        $string = "Faber Nainggolan";
        $encript =  $this->encryption->encrypt($string); //enkripsi string
        $decript = $this->encryption->decrypt($encript); //dekripsi string (mengembalikan string ke semula setelah di enkripsi
 
        echo $encript;
        echo $decript;
    }
	
	public function encrypt2(){
		
		$crypt_md5=md5("suikoden");
		$crypt_sha1=sha1("qwerty");
		$crypt_crc32=crc32("qwerty");
		$b = "technos";
		$a =	md5(sha1(crc32($b)));

		echo "enkripsi kata 'dicoba' <br>";
		echo "enkripsi md5 : ".$crypt_md5."<br>";
		echo "enkripsi sha1 : ".$crypt_sha1."<br>";
		echo "enkripsi crc32 : ".$crypt_crc32."<br><br><br>";
		echo "enkripsi gabungan : ".$a."<br><br><br>";

	

		/*====================== enkrip dan dekrip
		
		$string="sembunyikan aku ya";

		$encrypt=base64_encode($string);
		$decrypt=base64_decode($encrypt);

		echo "Kata Yang di Enkripsi : ".$string."<br>";
		echo "Hasil Enkrispi : ".$encrypt."<br>";
		echo "Hasil Dekripsi : ".$decrypt."<br>";*/
    }
    
	public function encryptexample(){
		$value = "tes";
		$this->encrypt->set_cipher(MCRYPT_RIJNDAEL_256);
        $this->encrypt->set_mode(MCRYPT_MODE_CBC);
        $encrypted_string = $this->encrypt->encode($value);
		echo $encrypted_string;
	}
	
	 public function decrypt(){
		$value = "TcHfGuVrhKoTcoz7H3dSPDEZVqJN0TsratSj8mkmRXfeQq+iRVh0McvQtnWMqqE/esm2qH0SaTMwJy4l209NrA==";
		$this->encrypt->set_cipher(MCRYPT_RIJNDAEL_256);
        $this->encrypt->set_mode(MCRYPT_MODE_CBC);
        $encrypted_string = $this->encrypt->decode($value);
		echo $encrypted_string;
    }
      
}