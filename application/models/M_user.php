<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query  = $this->db->query("SELECT u.userid, u.username, u.password, u.email, u.fullname, 
                                        u.register_date, u.register_time, u.group_id, u.active, g.group_name
                                        FROM table_user u, table_group g WHERE u.group_id=g.group_id");
        return $query->result();
    }
	
	public function data_active() {
        $query  = $this->db->query("SELECT u.userid, u.username, u.password, u.email, u.fullname
                                        u.register_date, u.register_time, u.group_id, u.active 
                                        FROM table_user WHERE u.active='Y'");
        return $query->result();
    }
    
    public function create($data) {
        //get data
        
        $this->userid = $data['userid'];
        $this->username = $data['username'];
		$this->fullname = $data['fullname'];
        $this->password = $data['password'];
        $this->email = $data['email'];
        $this->register_date = $data['register_date'];
        $this->register_time = $data['register_time'];
        $this->group_id = $data['group_id'];
		$this->nama_instansi = $data['nama_instansi'];
		$this->id_kabupaten = $data['id_kabupaten'];
        $this->active = $data['active'];
        
        //insert data
        $this->db->insert('table_user', $this);
    }
    
    public function update_nopassword($data) {
        //get data
        $this->username = $data['username'];
		$this->fullname = $data['fullname'];
        $this->email = $data['email'];
        $this->group_id = $data['group_id'];
		$this->nama_instansi = $data['nama_instansi'];
		$this->id_kabupaten = $data['id_kabupaten'];
        $this->active = $data['active'];
        
        //update data
        $this->db->update('table_user', $this, array('userid'=>$data['userid']));
    }
	
	public function update_withpassword($data) {
        //get data
        $this->username = $data['username'];
		$this->fullname = $data['fullname'];
        $this->password = $data['password'];
        $this->email = $data['email'];
        $this->group_id = $data['group_id'];
		$this->nama_instansi = $data['nama_instansi'];
		$this->id_kabupaten = $data['id_kabupaten'];
        $this->active = $data['active'];
        
        //update data
        $this->db->update('table_user', $this, array('userid'=>$data['userid']));
    }
    
    public function delete($id) {
        $this->db->delete('table_user', array('userid' => $id));
    }


    public function get($id){
        $this->db->where('userid', $id);
        $query = $this->db->get('table_user', 1);
        return $query->result();
    }
    
	public function record_count() {
			return $this->db->count_all("table_user u, table_group g
										WHERE g.group_id=u.group_id AND u.id_kabupaten!='1'");
	}
	
	public function record_count2($id_kabupaten) {
			return $this->db->count_all("table_user u, table_group g
										WHERE g.group_id=u.group_id AND u.id_kabupaten='".$id_kabupaten."' AND u.group_id!='group1000'");
	}
	
	public function record_count3($key) {
			return $this->db->count_all("table_user u, table_group g
										WHERE g.group_id=u.group_id 
										AND u.id_kabupaten!='1'
										AND  (	u.username LIKE '%".$key."%' ESCAPE '!' 
												OR u.email LIKE '%".$key."%' ESCAPE '!'
												OR u.active LIKE '%".$key."%' ESCAPE '!'
												OR g.group_name LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function record_count4($id_kabupaten, $key) {
			return $this->db->count_all("table_user u, table_group g
										WHERE g.group_id=u.group_id 
										AND u.id_kabupaten='".$id_kabupaten."'
										AND u.group_id!='group1000'
										AND  (	u.username LIKE '%".$key."%' ESCAPE '!' 
												OR u.email LIKE '%".$key."%' ESCAPE '!'
												OR u.active LIKE '%".$key."%' ESCAPE '!'
												OR g.group_name LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function record_count_search($key) {
		$this->db->like("username", $key);
		$this->db->or_like("email", $key);
		//$this->db->or_like("group", $key);
		$this->db->or_like("active", $key);
		return $this->db->count_all_results("table_user");
	}
	
	public function fetch_user($limit, $start) {
		$this->db->select('u.userid, u.username, u.email as usermail, g.group_name, u.nama_instansi, u.active, u.fullname');
		$this->db->from('table_user u, table_group g');
		$this->db->where('g.group_id=u.group_id AND u.id_kabupaten!="1"');
		$this->db->order_by('u.register_date DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetch_user2($limit, $start, $id_kabupaten) {
		$this->db->select('u.userid, u.username, u.email as usermail, g.group_name, u.nama_instansi, u.active, u.fullname');
		$this->db->from('table_user u, table_group g');
		$this->db->where("g.group_id=u.group_id AND u.id_kabupaten='".$id_kabupaten."' AND u.group_id!='group1000'");
		$this->db->order_by('u.register_date DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_user($limit, $start, $key) {
		$this->db->select('u.*, g.*');
		$this->db->from('table_user u, table_group g');
		$this->db->where("g.group_id=u.group_id 
						  AND u.id_kabupaten!='1'
						  AND  (	u.username LIKE '%".$key."%' ESCAPE '!' 
									OR u.email LIKE '%".$key."%' ESCAPE '!'
									OR u.active LIKE '%".$key."%' ESCAPE '!'
									OR g.group_name LIKE '%".$key."%' ESCAPE '!'
									OR u.nama_instansi LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->order_by("u.register_date DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	public function search_user2($limit, $start, $key, $id_kabupaten) {
		$this->db->select('u.*, g.*');
		$this->db->from('table_user u, table_group g');
		$this->db->where("g.group_id=u.group_id 
						  AND u.id_kabupaten='".$id_kabupaten."'
						  AND u.group_id!='group1000'
						  AND  (	u.username LIKE '%".$key."%' ESCAPE '!' 
									OR u.email LIKE '%".$key."%' ESCAPE '!'
									OR u.active LIKE '%".$key."%' ESCAPE '!'
									OR g.group_name LIKE '%".$key."%' ESCAPE '!'
									OR u.nama_instansi LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->order_by("u.register_date DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>