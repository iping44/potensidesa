<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_agenda extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_agenda");
        return $query->result();
    }		
	
	public function agenda(){
        $query = $this->db->get('table_agenda');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->download = $data['download'];
		$this->judul = $data['judul'];
		$this->post_date = $data['post_date'];
		$this->tanggal = $data['tanggal'];
		$this->kegiatan = $data['kegiatan'];
		$this->tempat = $data['tempat'];
		$this->id_jenisagenda = $data['id_jenisagenda'];	
        
        //insert data
        $this->db->insert('table_agenda', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		$this->judul = $data['judul'];
		$this->post_date = $data['post_date'];
		$this->tanggal = $data['tanggal'];
		$this->kegiatan = $data['kegiatan'];
		$this->tempat = $data['tempat'];
		$this->id_jenisagenda = $data['id_jenisagenda'];
		
        //insert data
        $this->db->insert('table_agenda', $this);
    }
    
    public function update($data) {
        //get data
        
        $this->download = $data['download'];
		$this->judul = $data['judul'];
		$this->post_date = $data['post_date'];
		$this->tanggal = $data['tanggal'];
		$this->kegiatan = $data['kegiatan'];
		$this->tempat = $data['tempat'];
		$this->id_jenisagenda = $data['id_jenisagenda'];		
		
        //update data
        $this->db->update('table_agenda', $this, array('id_agenda'=>$data['id_agenda']));
    }
    
    public function delete($id) {
        $this->db->delete('table_agenda', array('id_agenda' => $id));
    }


    public function get($id){
        $this->db->where('id_agenda', $id);
        $query = $this->db->get('table_agenda');
        return $query->result();
    }
	
	public function get_jenis($id){
        $this->db->where('id_jenisagenda', $id);
        $query = $this->db->get('table_agenda');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_agenda");
	}
	
	public function record_count_search($key) {
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		return $this->db->count_all_results("table_berita");
	}
	
	public function fetch_agenda($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_agenda');
		$this->db->order_by('id_agenda DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_agenda($limit, $start, $key) {
	
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_berita");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_agenda',$id);
		$query = $getData = $this->db->get('table_agenda');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->judul = $data['judul'];
		$this->post_date = $data['post_date'];
		$this->tanggal = $data['tanggal'];
		$this->kegiatan = $data['kegiatan'];
		$this->tempat = $data['tempat'];
		$this->id_jenisagenda = $data['id_jenisagenda'];
        
        //update data
        $this->db->update('table_agenda', $this, array('id_agenda'=>$data['id_agenda']));
    }
	
}
?>