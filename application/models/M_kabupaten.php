<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perencanaan

class M_kabupaten extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM industri_table_kabupaten");
        return $query->result();
    }
	
	public function data2() {
        $query  = $this->db->query("SELECT * FROM industri_table_kabupaten where id_kabupaten != '1'");
        return $query->result();
    }
	
	public function data3($id_kabupaten) {
        $query  = $this->db->query("SELECT * FROM industri_table_kabupaten where id_kabupaten = '$id_kabupaten'");
        return $query->result();
    }
	
	//front kabupaten content2
	public function data4() {
        $query  = $this->db->query("SELECT b.id_kabupaten, c.nama_kabupaten FROM industri_table_harga_barang a, industri_table_pasar b, industri_table_kabupaten c
									WHERE a.id_pasar = b.id_pasar
									AND b.id_kabupaten = c.id_kabupaten
									GROUP BY b.id_kabupaten");
        return $query->result();
    }
	
	public function kabupaten(){
        $query = $this->db->get('industri_table_kabupaten');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		$this->id_kabupaten = $data['id_kabupaten'];
        $this->nama_kabupaten = $data['nama_kabupaten'];
		$this->keterangan = $data['keterangan'];
        
        //insert data
        $this->db->insert('industri_table_kabupaten', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_kabupaten = $data['id_kabupaten'];
		$this->nama_kabupaten = $data['nama_kabupaten'];
		$this->keterangan = $data['keterangan'];
		
        //update data
        $this->db->update('industri_table_kabupaten', $this, array('id_kabupaten'=>$data['id_kabupaten']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_kabupaten', array('id_kabupaten' => $id));
    }


    public function get($id){
        $this->db->where('id_kabupaten', $id);
        $query = $this->db->get('industri_table_kabupaten');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("industri_table_kabupaten WHERE id_kabupaten != '1'");
	}
	
	public function record_count2($id_kabupaten) {
			return $this->db->count_all("industri_table_kabupaten WHERE id_kabupaten != '1' AND id_kabupaten='".$id_kabupaten."'");
	}
	
	public function record_count3($key) {
			return $this->db->count_all("industri_table_kabupaten
										 WHERE	id_kabupaten != '1'
										 AND  (	nama_kabupaten LIKE '%".$key."%' ESCAPE '!' 
												OR keterangan LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function record_count4($id_kabupaten, $key) {
			return $this->db->count_all("industri_table_kabupaten
										 WHERE	id_kabupaten != '1'
										 AND id_kabupaten='".$id_kabupaten."'
										 AND  (	nama_kabupaten LIKE '%".$key."%' ESCAPE '!' 
												OR keterangan LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function fetch_kabupaten($limit, $start) {
		$this->db->select('*');
		$this->db->from('industri_table_kabupaten');
		$this->db->where("id_kabupaten != '1'");
		$this->db->order_by("id_kabupaten DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetch_kabupaten2($limit, $start, $id_kabupaten) {
		$this->db->select('*');
		$this->db->from('industri_table_kabupaten');
		$this->db->where("id_kabupaten != '1'");
		$this->db->where("id_kabupaten='".$id_kabupaten."'");
		$this->db->order_by("id_kabupaten DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_kabupaten($limit, $start, $key) {
		$this->db->select('*');
		$this->db->from('industri_table_kabupaten');
		$this->db->where("id_kabupaten != '1'
						AND  (`nama_kabupaten` LIKE '%".$key."%' ESCAPE '!' OR `keterangan` LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	public function search_kabupaten2($limit, $start, $key, $id_kabupaten) {
		$this->db->select('*');
		$this->db->from('industri_table_kabupaten');
		$this->db->where("id_kabupaten != '1'");
		$this->db->where("id_kabupaten='".$id_kabupaten."'
						AND  (`nama_kabupaten` LIKE '%".$key."%' ESCAPE '!' OR `keterangan` LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>