<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_berita extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_berita order by tanggal desc");
        return $query->result();
    }		
	
	public function berita(){
        $query  = $this->db->query("SELECT * FROM table_berita order by tanggal desc");
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->gambar = $data['gambar'];
		$this->judul = $data['judul'];
		$this->kategori = $data['kategori'];	
		$this->content = $data['content'];
		$this->tanggal = $data['tanggal'];		
        
        //insert data
        $this->db->insert('table_berita', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		$this->judul = $data['judul'];
		$this->kategori = $data['kategori'];
		$this->content = $data['content'];
		$this->tanggal = $data['tanggal'];
		
        //insert data
        $this->db->insert('table_berita', $this);
    }
    
    public function update($data) {
        //get data
        
        $this->gambar = $data['gambar'];
		$this->kategori = $data['kategori'];
		$this->judul = $data['judul'];
		$this->content = $data['content'];
		$this->tanggal = $data['tanggal'];			
		
        //update data
        $this->db->update('table_berita', $this, array('id_berita'=>$data['id_berita']));
    }
    
    public function delete($id) {
        $this->db->delete('table_berita', array('id_berita' => $id));
    }


    public function get($id){
        $this->db->where('id_berita', $id);
        $query = $this->db->get('table_berita');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_berita");
	}
	
	public function record_count_search($key) {
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		return $this->db->count_all_results("table_berita");
	}
	
	public function fetch_berita($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_berita');
		$this->db->order_by('id_berita DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_berita($limit, $start, $key) {
	
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_berita");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_berita',$id);
		$query = $getData = $this->db->get('table_berita');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_berita = $data['id_berita'];		
		$this->judul = $data['judul'];
		$this->content = $data['content'];
		$this->tanggal = $data['tanggal'];	
		$this->kategori = $data['kategori'];
        
        //update data
        $this->db->update('table_berita', $this, array('id_berita'=>$data['id_berita']));
    }
	
}
?>