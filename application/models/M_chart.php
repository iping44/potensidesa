<?php
class M_chart extends Ci_Model{
	
	function laporanTahunan()
	{
		
		$bc = $this->db->query("
		
		select
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=1)AND (YEAR(tanggal)=2016))),0) AS `Januari`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=2)AND (YEAR(tanggal)=2016))),0) AS `Februari`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=3)AND (YEAR(tanggal)=2016))),0) AS `Maret`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=4)AND (YEAR(tanggal)=2016))),0) AS `April`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=5)AND (YEAR(tanggal)=2016))),0) AS `Mei`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=6)AND (YEAR(tanggal)=2016))),0) AS `Juni`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=7)AND (YEAR(tanggal)=2016))),0) AS `Juli`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=8)AND (YEAR(tanggal)=2016))),0) AS `Agustus`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=9)AND (YEAR(tanggal)=2016))),0) AS `September`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=10)AND (YEAR(tanggal)=2016))),0) AS `Oktober`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=11)AND (YEAR(tanggal)=2016))),0) AS `November`,
		ifnull((SELECT sum(harga)	FROM	(industri_table_harga_barang)WHERE((Month(tanggal)=12)AND (YEAR(tanggal)=2016))),0) AS `Desember`
	from industri_table_harga_barang GROUP BY YEAR(tanggal) 
		
		");
		
		return $bc;
		
	}
	
	
}

?>