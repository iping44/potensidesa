<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_pekerjaan extends CI_Model{

    function __construct() {
        parent::__construct();
    }

	public function data($id) {
    $query  = $this->db->query("SELECT * FROM table_jenis_pekerjaan d, table_pekerjaan g
			WHERE d.jenis_pekerjaan_id=g.jenis_pekerjaan_id AND g.desa_id='$id'");
        return $query->result();
	}

  public function create($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->jenis_pekerjaan_id = $data['jenis_pekerjaan_id'];
        $this->jumlah = $data['jumlah'];
        $this->tahun = $data['tahun'];

        //insert data
        $this->db->insert('table_pekerjaan', $this);
    }

	public function jenis_pekerjaan()
	{
		$query = $this->db->get('table_jenis_pekerjaan');
		if($query->num_rows() > 0) {
			foreach($query->result () as $row){
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

    public function update($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->pekerjaan_id = $data['pekerjaan_id'];
        $this->jenis_pekerjaan_id = $data['jenis_pekerjaan_id'];
        $this->jumlah = $data['jumlah'];
        $this->tahun = $data['tahun'];

        //update data
        $this->db->update('table_pekerjaan', $this, array('desa_id'=>$data['desa_id'], 'pekerjaan_id'=>$data['pekerjaan_id']));
    }

    public function delete($id1, $id2) {
        $this->db->delete('table_pekerjaan', array('desa_id'=>$id1, 'pekerjaan_id'=>$id2));
    }


    public function get($id1, $id2){
        $this->db->where('desa_id', $id1);
		    $this->db->where('pekerjaan_id', $id2);
        $query = $this->db->get('table_pekerjaan', 1);
        return $query->result();
    }

	public function record_count($id) {
		$this->db->where("desa_id", $id);
		return $this->db->count_all_results("table_pekerjaan");
	}

	public function record_count_search($key, $id) {
		$this->db->where("desa_id", $id);
		$this->db->like("pekerjaan_id", $key);
    $this->db->like("jenis_pekerjaan_id", $key);
		$this->db->or_like("jumlah", $key);
		$this->db->or_like("tahun", $key);
		return $this->db->count_all_results("table_pekerjaan");
	}

	public function fetch_pekerjaan($limit, $start, $id) {
		$this->db->select('*');
		$this->db->from('table_pekerjaan j');
		$this->db->join('table_jenis_pekerjaan p', 'p.jenis_pekerjaan_id=j.jenis_pekerjaan_id');
		$this->db->where("j.desa_id", $id);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_pekerjaan($limit, $start, $key, $id) {
    $this->db->select('*');
    $this->db->from('table_pekerjaan j');
    $this->db->join('table_jenis_pekerjaan p', 'p.jenis_pekerjaan_id=j.jenis_pekerjaan_id');
		$this->db->where("j.desa_id", $id);
    $this->db->like("p.jenis_pekerjaan", $key);
		$this->db->or_like("j.jumlah", $key);
		$this->db->or_like("j.tahun", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}
}
?>
