<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_desageografi extends CI_Model{

    function __construct() {
        parent::__construct();
    }

	public function data($id) {
		$query  = $this->db->query("SELECT * FROM table_desa d, table_desageografi g
			WHERE d.desa_id=g.desa_id AND d.desa_id='$id'");
        return $query->result();
	}

    public function create($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->ketinggian = $data['ketinggian'];
        $this->curah_hujan = $data['curah_hujan'];
        $this->topografi = $data['topografi'];
        $this->iklim = $data['iklim'];
        $this->tahun = $data['tahun'];

        //insert data
        $this->db->insert('table_desageografi', $this);
    }

    public function update($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->ketinggian = $data['ketinggian'];
        $this->curah_hujan = $data['curah_hujan'];
        $this->topografi = $data['topografi'];
        $this->iklim = $data['iklim'];
        $this->tahun = $data['tahun'];

        //update data
        $this->db->update('table_desageografi', $this, array('desa_id'=>$data['desa_id'], 'desageografi_id'=>$data['desageografi_id']));
    }

    public function delete($id1, $id2) {
        $this->db->delete('table_desageografi', array('desa_id'=>$id1, 'desageografi_id'=>$id2));
    }


    public function get($id1, $id2){
        $this->db->where('desa_id', $id1);
		    $this->db->where('desageografi_id', $id2);
        $query = $this->db->get('table_desageografi', 1);
        return $query->result();
    }

	public function record_count($id) {
		$this->db->where("desa_id", $id);
		return $this->db->count_all_results("table_desageografi");
	}

	public function record_count_search($key, $id) {
		$this->db->where("desa_id", $id);
		$this->db->like("desageografi_id", $key);
		$this->db->or_like("ketinggian", $key);
		$this->db->or_like("curah_hujan", $key);
		$this->db->or_like("topografi", $key);
    $this->db->or_like("tahun", $key);
		return $this->db->count_all_results("table_desageografi");
	}

	public function fetch_desageografi($limit, $start, $id) {
		$this->db->select('*');
		$this->db->from('table_desa d');
		$this->db->join('table_desageografi g', 'g.desa_id=d.desa_id');
		$this->db->where("d.desa_id", $id);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_desageografi($limit, $start, $key, $id) {
    $this->db->where("desa_id", $id);
		$this->db->like("desageografi_id", $key);
		$this->db->or_like("ketinggian", $key);
		$this->db->or_like("curah_hujan", $key);
		$this->db->or_like("topografi", $key);
    $this->db->or_like("tahun", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_desageografi");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}
}
?>
