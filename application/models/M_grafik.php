<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindutrian

class M_grafik extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	
	 public function grafik($id_komoditi,$id_pasar,$bulan) {
		$bc = $this->db->query("
   
			  select
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '1' ORDER BY `tanggal` ASC),0) AS `1`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '2' ORDER BY `tanggal` ASC),0) AS `2`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '3' ORDER BY `tanggal` ASC),0) AS `3`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '4' ORDER BY `tanggal` ASC),0) AS `4`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '5' ORDER BY `tanggal` ASC),0) AS `5`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '6' ORDER BY `tanggal` ASC),0) AS `6`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '7' ORDER BY `tanggal` ASC),0) AS `7`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '8' ORDER BY `tanggal` ASC),0) AS `8`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '9' ORDER BY `tanggal` ASC),0) AS `9`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '10' ORDER BY `tanggal` ASC),0) AS `10`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '11' ORDER BY `tanggal` ASC),0) AS `11`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '12' ORDER BY `tanggal` ASC),0) AS `12`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '13' ORDER BY `tanggal` ASC),0) AS `13`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '14' ORDER BY `tanggal` ASC),0) AS `14`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '15' ORDER BY `tanggal` ASC),0) AS `15`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '16' ORDER BY `tanggal` ASC),0) AS `16`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '17' ORDER BY `tanggal` ASC),0) AS `17`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '18' ORDER BY `tanggal` ASC),0) AS `18`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '19' ORDER BY `tanggal` ASC),0) AS `19`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '20' ORDER BY `tanggal` ASC),0) AS `20`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '21' ORDER BY `tanggal` ASC),0) AS `21`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '22' ORDER BY `tanggal` ASC),0) AS `22`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '23' ORDER BY `tanggal` ASC),0) AS `23`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '24' ORDER BY `tanggal` ASC),0) AS `24`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '25' ORDER BY `tanggal` ASC),0) AS `25`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '26' ORDER BY `tanggal` ASC),0) AS `26`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '27' ORDER BY `tanggal` ASC),0) AS `27`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '28' ORDER BY `tanggal` ASC),0) AS `28`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '29' ORDER BY `tanggal` ASC),0) AS `29`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '30' ORDER BY `tanggal` ASC),0) AS `30`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)=year(curdate()) and day(a.tanggal) = '31' ORDER BY `tanggal` ASC),0) AS `31`
			   
			  ");
			   
			  return $bc;
	
    }
	
	public function grafik_stok($id_komoditi_stok,$tahun) {
		$query = $this->db->query("SELECT a.*, b.*, c.* FROM industri_table_stok a, industri_table_komoditi_stok b, industri_table_kabupaten c 
									WHERE a.id_kabupaten = c.id_kabupaten
									AND a.id_komoditi_stok = b.id_komoditi_stok
									AND a.id_komoditi_stok = '".$id_komoditi_stok."'  
									AND a.tahun = '".$tahun."'");
			   
			 return $query->result();
	
    }
	
	
	public function grafik_stok2($id_komoditi_stok,$tahun, $id_kabupaten) {
		$query = $this->db->query("SELECT a.*, b.*, c.* FROM industri_table_stok a, industri_table_komoditi_stok b, industri_table_kabupaten c 
									WHERE a.id_kabupaten = c.id_kabupaten
									AND a.id_komoditi_stok = b.id_komoditi_stok
									AND a.id_komoditi_stok = '".$id_komoditi_stok."'
									and a.id_kabupaten = '".$id_kabupaten."'
									AND a.tahun = '".$tahun."'");
			   
			 return $query->result();
	
    }
	
	public function grafik_antar_pulau($id_komoditi_tetap, $tahun) {
        $query = $this->db->query("SELECT a.*, SUM(a.volume) as volume2, SUM(a.nilai) as nilai2, b.*,c.*, d.*
								FROM industri_table_antar_pulau a, industri_table_komoditi_tetap b, industri_table_kabupaten c, industri_table_jenis_komoditi d 	
								WHERE a.id_komoditi_tetap=b.id_komoditi_tetap 
								AND a.id_kabupaten=c.id_kabupaten
								AND b.id_jenis_komoditi=d.id_jenis_komoditi 
								AND b.id_komoditi_tetap='".$id_komoditi_tetap."'
								AND YEAR(a.tanggal)='".$tahun."'
								GROUP BY a.id_kabupaten");
        return $query->result();
    }
	
	public function grafik_harian_pasar($tanggal,$id_pasar,$jenis_komoditi) {
		$query = $this->db->query("
			  SELECT a.harga, b.nama_komoditi FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` 
			  WHERE `a`.`id_komoditi` = `b`.`id_komoditi` 
			  AND `a`.`id_pasar` = `c`.`id_pasar`  
			  AND `a`.`tanggal` = '".$tanggal."' 
			  AND `a`.`id_pasar` = '".$id_pasar."' 
			  AND `b`.`jenis_komoditi` = '".$jenis_komoditi."'  
			  ");
			   
			 return $query->result();
	
    }
	
	public function grafik_harian_kabupaten($tanggal,$id_kabupaten,$jenis_komoditi) {
		$query = $this->db->query("
			  SELECT ROUND(avg(a.harga)) as harga, b.nama_komoditi FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` 
			  WHERE `a`.`id_komoditi` = `b`.`id_komoditi` 
			  AND `a`.`id_pasar` = `c`.`id_pasar`  
			  AND `a`.`tanggal` = '".$tanggal."' 
			  AND `c`.`id_kabupaten` = '".$id_kabupaten."' 
			  AND `b`.`jenis_komoditi` = '".$jenis_komoditi."'
				group by b.nama_komoditi			  
			  ");
			   
			 return $query->result();
	
    }
	
	public function grafik_mingguan_tanggal($tanggal) {
		$bc = $this->db->query("
   
			  select
			  ifnull((SELECT DATE_ADD('".$tanggal."', INTERVAL 0 DAY)),0) AS `1`,
			  ifnull((SELECT DATE_ADD('".$tanggal."', INTERVAL 1 DAY)),0) AS `2`,
			  ifnull((SELECT DATE_ADD('".$tanggal."', INTERVAL 2 DAY)),0) AS `3`,
			  ifnull((SELECT DATE_ADD('".$tanggal."', INTERVAL 3 DAY)),0) AS `4`,
			  ifnull((SELECT DATE_ADD('".$tanggal."', INTERVAL 4 DAY)),0) AS `5`,
			  ifnull((SELECT DATE_ADD('".$tanggal."', INTERVAL 5 DAY)),0) AS `6`,
			  ifnull((SELECT DATE_ADD('".$tanggal."', INTERVAL 6 DAY)),0) AS `7`
			  ");
			   
			  return $bc;
	
    }
	
	public function grafik_mingguan_pasar($tanggal,$id_pasar,$id_komoditi) {
		$bc = $this->db->query("
   
			  select
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = '".$tanggal."' AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."'),0) AS `1`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 1 DAY) AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."'),0) AS `2`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 2 DAY) AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."'),0) AS `3`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 3 DAY) AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."'),0) AS `4`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 4 DAY) AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."'),0) AS `5`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 5 DAY) AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."'),0) AS `6`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 6 DAY) AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."'),0) AS `7`
		
			  ");
			   
			  return $bc;
	
    }
	
	
	public function grafik_mingguan_kabupaten($tanggal,$id_kabupaten,$id_komoditi) {
		$bc = $this->db->query("
   
			  select
			  ifnull((SELECT ROUND(avg(a.harga)) as harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = '".$tanggal."' AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' group by b.nama_komoditi ),0) AS `1`,
			  ifnull((SELECT ROUND(avg(a.harga)) as harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 1 DAY) AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' group by b.nama_komoditi),0) AS `2`,
			  ifnull((SELECT ROUND(avg(a.harga)) as harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 2 DAY) AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' group by b.nama_komoditi),0) AS `3`,
			  ifnull((SELECT ROUND(avg(a.harga)) as harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 3 DAY) AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' group by b.nama_komoditi),0) AS `4`,
			  ifnull((SELECT ROUND(avg(a.harga)) as harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 4 DAY) AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' group by b.nama_komoditi),0) AS `5`,
			  ifnull((SELECT ROUND(avg(a.harga)) as harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 5 DAY) AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' group by b.nama_komoditi),0) AS `6`,
			  ifnull((SELECT ROUND(avg(a.harga)) as harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`tanggal` = DATE_ADD('".$tanggal."', INTERVAL 6 DAY) AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' group by b.nama_komoditi),0) AS `7`
		
			  ");
			   
			  return $bc;
	
    }
	
	public function grafik_bulanan_tanggal($bulan,$tahun) {
		$bc = $this->db->query("
   
			select
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-01', INTERVAL 0 DAY)),0) AS `1`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-02', INTERVAL 0 DAY)),0) AS `2`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-03', INTERVAL 0 DAY)),0) AS `3`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-04', INTERVAL 0 DAY)),0) AS `4`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-05', INTERVAL 0 DAY)),0) AS `5`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-06', INTERVAL 0 DAY)),0) AS `6`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-07', INTERVAL 0 DAY)),0) AS `7`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-08', INTERVAL 0 DAY)),0) AS `8`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-09', INTERVAL 0 DAY)),0) AS `9`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-10', INTERVAL 0 DAY)),0) AS `10`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-11', INTERVAL 0 DAY)),0) AS `11`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-12', INTERVAL 0 DAY)),0) AS `12`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-13', INTERVAL 0 DAY)),0) AS `13`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-14', INTERVAL 0 DAY)),0) AS `14`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-15', INTERVAL 0 DAY)),0) AS `15`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-16', INTERVAL 0 DAY)),0) AS `16`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-17', INTERVAL 0 DAY)),0) AS `17`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-18', INTERVAL 0 DAY)),0) AS `18`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-19', INTERVAL 0 DAY)),0) AS `19`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-20', INTERVAL 0 DAY)),0) AS `20`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-21', INTERVAL 0 DAY)),0) AS `21`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-22', INTERVAL 0 DAY)),0) AS `22`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-23', INTERVAL 0 DAY)),0) AS `23`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-24', INTERVAL 0 DAY)),0) AS `24`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-25', INTERVAL 0 DAY)),0) AS `25`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-26', INTERVAL 0 DAY)),0) AS `26`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-27', INTERVAL 0 DAY)),0) AS `27`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-28', INTERVAL 0 DAY)),0) AS `28`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-29', INTERVAL 0 DAY)),0) AS `29`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-30', INTERVAL 0 DAY)),0) AS `30`,
			  ifnull((SELECT DATE_ADD('".$tahun."-".$bulan."-31', INTERVAL 0 DAY)),0) AS `31`
			  ");
			   
			  return $bc;
	
    }
	
	public function grafik_bulanan_pasar($bulan,$tahun,$id_pasar,$id_komoditi) {
		$bc = $this->db->query("
   
			 select
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '1' ORDER BY `tanggal` ASC),0) AS `1`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '2' ORDER BY `tanggal` ASC),0) AS `2`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '3' ORDER BY `tanggal` ASC),0) AS `3`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '4' ORDER BY `tanggal` ASC),0) AS `4`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '5' ORDER BY `tanggal` ASC),0) AS `5`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '6' ORDER BY `tanggal` ASC),0) AS `6`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '7' ORDER BY `tanggal` ASC),0) AS `7`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '8' ORDER BY `tanggal` ASC),0) AS `8`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '9' ORDER BY `tanggal` ASC),0) AS `9`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '10' ORDER BY `tanggal` ASC),0) AS `10`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '11' ORDER BY `tanggal` ASC),0) AS `11`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '12' ORDER BY `tanggal` ASC),0) AS `12`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '13' ORDER BY `tanggal` ASC),0) AS `13`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '14' ORDER BY `tanggal` ASC),0) AS `14`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '15' ORDER BY `tanggal` ASC),0) AS `15`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '16' ORDER BY `tanggal` ASC),0) AS `16`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '17' ORDER BY `tanggal` ASC),0) AS `17`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '18' ORDER BY `tanggal` ASC),0) AS `18`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '19' ORDER BY `tanggal` ASC),0) AS `19`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '20' ORDER BY `tanggal` ASC),0) AS `20`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '21' ORDER BY `tanggal` ASC),0) AS `21`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '22' ORDER BY `tanggal` ASC),0) AS `22`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '23' ORDER BY `tanggal` ASC),0) AS `23`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '24' ORDER BY `tanggal` ASC),0) AS `24`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '25' ORDER BY `tanggal` ASC),0) AS `25`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '26' ORDER BY `tanggal` ASC),0) AS `26`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '27' ORDER BY `tanggal` ASC),0) AS `27`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '28' ORDER BY `tanggal` ASC),0) AS `28`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '29' ORDER BY `tanggal` ASC),0) AS `29`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '30' ORDER BY `tanggal` ASC),0) AS `30`,
			  ifnull((SELECT a.harga FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '31' ORDER BY `tanggal` ASC),0) AS `31`
			 
			  ");
			   
			  return $bc;
	
    }
	
	public function grafik_bulanan_kabupaten($bulan,$tahun,$id_kabupaten,$id_komoditi) {
		$bc = $this->db->query("
   
			  select
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '1' group by b.nama_komoditi ),0) AS `1`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '2' group by b.nama_komoditi ),0) AS `2`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '3' group by b.nama_komoditi ),0) AS `3`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '4' group by b.nama_komoditi ),0) AS `4`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '5' group by b.nama_komoditi ),0) AS `5`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '6' group by b.nama_komoditi ),0) AS `6`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '7' group by b.nama_komoditi ),0) AS `7`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '8' group by b.nama_komoditi ),0) AS `8`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '9' group by b.nama_komoditi ),0) AS `9`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '10' group by b.nama_komoditi ),0) AS `10`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '11' group by b.nama_komoditi ),0) AS `11`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '12' group by b.nama_komoditi ),0) AS `12`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '13' group by b.nama_komoditi ),0) AS `13`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '14' group by b.nama_komoditi ),0) AS `14`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '15' group by b.nama_komoditi ),0) AS `15`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '16' group by b.nama_komoditi ),0) AS `16`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '17' group by b.nama_komoditi ),0) AS `17`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '18' group by b.nama_komoditi ),0) AS `18`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '19' group by b.nama_komoditi ),0) AS `19`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '20' group by b.nama_komoditi ),0) AS `20`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '21' group by b.nama_komoditi ),0) AS `21`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '22' group by b.nama_komoditi ),0) AS `22`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '23' group by b.nama_komoditi ),0) AS `23`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '24' group by b.nama_komoditi ),0) AS `24`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '25' group by b.nama_komoditi ),0) AS `25`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '26' group by b.nama_komoditi ),0) AS `26`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '27' group by b.nama_komoditi ),0) AS `27`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '28' group by b.nama_komoditi ),0) AS `28`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '29' group by b.nama_komoditi ),0) AS `29`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '30' group by b.nama_komoditi ),0) AS `30`,
			  ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar`  AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan."' AND year(a.tanggal)='".$tahun."' and day(a.tanggal) = '31' group by b.nama_komoditi ),0) AS `31`
			 
			  ");
			   
			  return $bc;
	
    }
	
	public function grafik_triwulan_tanggal($bulan1,$bulan2,$bulan3,$tahun,$id_pasar,$id_komoditi) {
		$bc = $this->db->query("
   
			 select
			ifnull((SELECT  monthname('2016-".$bulan1."-01')),0) AS `1`, 
			ifnull((SELECT  monthname('2016-".$bulan2."-01')),0) AS `2`, 
			ifnull((SELECT  monthname('2016-".$bulan3."-01')),0) AS `3`
			 
			  ");
			   
			  return $bc;
	
    }
	
	public function grafik_triwulan_pasar($bulan1,$bulan2,$bulan3,$tahun,$id_pasar,$id_komoditi) {
		$bc = $this->db->query("
   
			 select
			ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan1."' ORDER BY `tanggal` ASC),0) AS `1`, 
			ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan2."' ORDER BY `tanggal` ASC),0) AS `2`, 
			ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `a`.`id_pasar` = '".$id_pasar."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan3."' ORDER BY `tanggal` ASC),0) AS `3` 
			 
			  ");
			   
			  return $bc;
	
    }
	
	public function grafik_triwulan_kabupaten($bulan1,$bulan2,$bulan3,$tahun,$id_kabupaten,$id_komoditi) {
		$bc = $this->db->query("
   
			 select
			ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan1."' ORDER BY `tanggal` ASC),0) AS `1`, 
			ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan2."' ORDER BY `tanggal` ASC),0) AS `2`, 
			ifnull((SELECT ROUND(avg(a.harga)) FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` WHERE `a`.`id_komoditi` = `b`.`id_komoditi` AND `a`.`id_pasar` = `c`.`id_pasar` AND `c`.`id_kabupaten` = '".$id_kabupaten."' AND `b`.`id_komoditi` = '".$id_komoditi."' AND month(a.tanggal)='".$bulan3."' ORDER BY `tanggal` ASC),0) AS `3` 
			 
			  ");
			   
			  return $bc;
	
    }
	
	public function grafik_tahunan_pasar($tahun1,$tahun2,$id_pasar,$jenis_komoditi) {
		$query = $this->db->query("
			      SELECT year(a.tanggal) as tahun, ROUND(avg(a.harga)) as harga, b.nama_komoditi FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` 
				  WHERE `a`.`id_komoditi` = `b`.`id_komoditi` 
				  AND `a`.`id_pasar` = `c`.`id_pasar` 
				  AND `a`.`id_pasar` = '".$id_pasar."' 
				  AND `a`.`id_komoditi` = '".$jenis_komoditi."'
				  AND year(a.tanggal)>='".$tahun1."'  
				  and year(a.tanggal)<='".$tahun2."'
				  group by  year(a.tanggal)		  
			  ");
			   
			 return $query->result();
	
    }
	
	public function grafik_tahunan_kabupaten($tahun1,$tahun2,$id_kabupaten,$jenis_komoditi) {
		$query = $this->db->query("
			      SELECT year(a.tanggal) as tahun, ROUND(avg(a.harga)) as harga, b.nama_komoditi FROM `industri_table_harga_barang` `a`, `industri_table_komoditi` `b`, `industri_table_pasar` `c` 
				  WHERE `a`.`id_komoditi` = `b`.`id_komoditi` 
				  AND `a`.`id_pasar` = `c`.`id_pasar` 
				  AND `c`.`id_kabupaten` = '".$id_kabupaten."' 
				  AND `a`.`id_komoditi` = '".$jenis_komoditi."'
				  AND year(a.tanggal)>='".$tahun1."'  
				  and year(a.tanggal)<='".$tahun2."'
				  group by  year(a.tanggal)		  
			  ");
			   
			 return $query->result();
	
    }
	
	public function nama_komoditi($id_komoditi){
		$query = $this->db->query("select * from industri_table_komoditi where id_komoditi = '".$id_komoditi."'");
		return $query->result();
	}
		
	public function nama_pasar($id_pasar){
		$query = $this->db->query("select * from industri_table_pasar where id_pasar = '".$id_pasar."'");
		return $query->result();
	}
	public function tanggal(){
		$query = $this->db->query("select year(now()) as tahun");
		return $query->result();
	}
	
}
?>