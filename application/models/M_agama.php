<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_agama extends CI_Model{

    function __construct() {
        parent::__construct();
    }

	public function data($id) {
    $query  = $this->db->query("SELECT * FROM table_desa d, table_agama g
			WHERE d.desa_id=g.desa_id AND d.desa_id='$id'");
        return $query->result();
	}

  public function create($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->agama_id = $data['agama_id'];
        $this->islam = $data['islam'];
        $this->kristen = $data['kristen'];
        $this->katolik = $data['katolik'];
    		$this->hindu = $data['hindu'];
    		$this->budha = $data['budha'];
    		$this->lain = $data['lain'];

        //insert data
        $this->db->insert('table_agama', $this);
    }

    public function update($data) {
        //get data
        $this->desa_id = $data['desa_id'];
        $this->agama_id = $data['agama_id'];
        $this->islam = $data['islam'];
        $this->kristen = $data['kristen'];
        $this->katolik = $data['katolik'];
    		$this->hindu = $data['hindu'];
    		$this->budha = $data['budha'];
    		$this->lain = $data['lain'];

        //update data
        $this->db->update('table_agama', $this, array('desa_id'=>$data['desa_id'], 'agama_id'=>$data['agama_id']));
    }

    public function delete($id1, $id2) {
        $this->db->delete('table_agama', array('desa_id'=>$id1, 'agama_id'=>$id2));
    }


    public function get($id1, $id2){
        $this->db->where('desa_id', $id1);
		    $this->db->where('agama_id', $id2);
        $query = $this->db->get('table_agama', 1);
        return $query->result();
    }

	public function record_count($id) {
		$this->db->where("desa_id", $id);
		return $this->db->count_all_results("table_agama");
	}

	public function record_count_search($key, $id) {
		$this->db->where("desa_id", $id);
		$this->db->like("agama_id", $key);
		$this->db->or_like("islam", $key);
		$this->db->or_like("kristen", $key);
		$this->db->or_like("katolik", $key);
		$this->db->or_like("hindu", $key);
		$this->db->or_like("budha", $key);
		$this->db->or_like("lain", $key);
		return $this->db->count_all_results("table_agama");
	}

	public function fetch_agama($limit, $start, $id) {
		$this->db->select('*');
		$this->db->from('table_agama j');
		$this->db->join('table_kelamin p', 'p.id_kelamin=j.agama_id');
		$this->db->where("j.agama_id", $id);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_agama($limit, $start, $key, $id) {
		$this->db->where("agama_id", $id);
		$this->db->like("desa_id", $key);
		$this->db->or_like("islam", $key);
		$this->db->or_like("kristen", $key);
		$this->db->or_like("katolik", $key);
		$this->db->or_like("hindu", $key);
		$this->db->or_like("budha", $key);
		$this->db->or_like("lain", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_agama");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}
}
?>
