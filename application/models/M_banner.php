<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_banner extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_banner");
        return $query->result();
    }		
	
	public function banner(){
        $query = $this->db->get('table_banner');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->download = $data['download'];
		$this->judul = $data['judul'];
		
        //insert data
        $this->db->insert('table_banner', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		$this->judul = $data['judul'];
		
		
        //insert data
        $this->db->insert('table_banner', $this);
    }
    
    public function update($data) {
        //get data
        
        $this->download = $data['download'];
		$this->judul = $data['judul'];
		
        //update data
        $this->db->update('table_banner', $this, array('id_banner'=>$data['id_banner']));
    }
    
    public function delete($id) {
        $this->db->delete('table_banner', array('id_banner' => $id));
    }


    public function get($id){
        $this->db->where('id_banner', $id);
        $query = $this->db->get('table_banner');
        return $query->result();
    }
	
	
	public function record_count() {
			return $this->db->count_all("table_banner");
	}
	
	public function record_count_search($key) {
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		return $this->db->count_all_results("table_berita");
	}
	
	public function fetch_banner($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_banner');
		$this->db->order_by('id_banner DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_banner($limit, $start, $key) {
	
		$this->db->like("gambar", $key);
		$this->db->or_like("judul", $key);
		$this->db->or_like("content", $key);
		$this->db->or_like("tanggal", $key);		
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_berita");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_banner',$id);
		$query = $getData = $this->db->get('table_banner');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->judul = $data['judul'];
        
        //update data
        $this->db->update('table_banner', $this, array('id_banner'=>$data['id_banner']));
    }
	
}
?>