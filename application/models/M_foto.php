<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_foto extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_foto");
        return $query->result();
    }		
	
	public function foto(){
        $query = $this->db->get('table_foto');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->judul = $data['judul'];		
		$this->deskripsi = $data['deskripsi'];	
		$this->gambar = $data['gambar'];
        
        //insert data
        $this->db->insert('table_foto', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		
		$this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];
		
		
        //insert data
        $this->db->insert('table_foto', $this);
    }
    
    public function update($data) {
        //get data        
        
		$this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];
		$this->gambar = $data['gambar'];	
		
        //update data
        $this->db->update('table_foto', $this, array('id_foto'=>$data['id_foto']));
    }
    
    public function delete($id) {
        $this->db->delete('table_foto', array('id_foto' => $id));
    }


    public function get($id){
        $this->db->where('id_foto', $id);
        $query = $this->db->get('table_foto');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_foto");
	}
	
	public function record_count_search($key) {		
		$this->db->or_like("judul", $key);
		$this->db->or_like("deskripsi", $key);		
		$this->db->like("gambar", $key);
		return $this->db->count_all_results("table_foto");
	}
	
	public function fetch_foto($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_foto');
		$this->db->order_by('id_foto DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_foto($limit, $start, $key) {	
		
		$this->db->or_like("judul", $key);
		$this->db->or_like("deskripsi", $key);		
		$this->db->like("gambar", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_foto");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_foto',$id);
		$query = $getData = $this->db->get('table_foto');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_foto = $data['id_foto'];			
		$this->judul = $data['judul'];
		$this->deskripsi = $data['deskripsi'];
        
        //update data
        $this->db->update('table_foto', $this, array('id_foto'=>$data['id_foto']));
    }
	
}
?>