<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_template_report extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query  = $this->db->query("SELECT * FROM izin_table_template");
        return $query->result();
    }
    
    public function create($data) {
        $this->id_template = $data['id_template'];
        $this->judul = $data['judul'];
        $this->desain_template = $data['desain_template'];
        $this->keterangan = $data['keterangan'];
        //insert data
        $this->db->insert('izin_table_template', $this);
    }
    
    public function update($data) {
        //get data
        $this->judul = $data['judul'];
        $this->desain_template = $data['desain_template'];
        $this->keterangan = $data['keterangan'];
        
        //update data
        $this->db->update('izin_table_template', $this, array('id_template'=>$data['id_template']));
    }
    
    public function delete($id) {
        $this->db->delete('izin_table_template', array('id_template' => $id));
    }


    public function get($id){
        $this->db->where('id_template', $id);
        $query = $this->db->get('izin_table_template', 1);
        return $query->result();
    }
    
	public function record_count() {
		return $this->db->count_all("izin_table_template");
	}
	
	public function record_count_search($key) {
		$this->db->like("desain_template", $key);
		$this->db->or_like("keterangan", $key);
		//$this->db->or_like("group", $key);
		$this->db->or_like("id_template", $key);
		return $this->db->count_all_results("izin_table_template");
	}
	
	public function fetch_template($limit, $start) {
		$this->db->select('id_template,judul,desain_template,keterangan');
		$this->db->from('izin_table_template');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_template($limit, $start, $key) {
		$this->db->select('*');
		$this->db->from('izin_table_template');
		$this->db->like("id_template", $key);
		$this->db->or_like("desain_template", $key);
		$this->db->or_like("keterangan", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>