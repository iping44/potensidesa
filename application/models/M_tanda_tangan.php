<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindutrian

class M_tanda_tangan extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
	
    public function create($data) {
        //get data
		$this->nama = $data['nama'];
		$this->nip = $data['nip'];
        $this->golongan = $data['golongan'];
        $this->jabatan = $data['jabatan'];
        $this->id_kabupaten = $data['id_kabupaten'];
		
        //insert data
        $this->db->insert('industri_table_tanda_tangan', $this);
    }
    
    public function update($data) {
        //get data
		$this->id_tanda_tangan = $data['id_tanda_tangan'];
		$this->nama = $data['nama'];
		$this->nip = $data['nip'];
        $this->golongan = $data['golongan'];
        $this->jabatan = $data['jabatan'];
        $this->id_kabupaten = $data['id_kabupaten'];
        
        //update data
        $this->db->update('industri_table_tanda_tangan', $this, array('id_tanda_tangan'=>$data['id_tanda_tangan']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_tanda_tangan', array('id_tanda_tangan' => $id));
    }

    public function get($id){
        $this->db->where('a.id_tanda_tangan', $id);
        $this->db->where('a.id_kabupaten = b.id_kabupaten');
        $query = $this->db->get('industri_table_tanda_tangan a, industri_table_kabupaten b');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("industri_table_tanda_tangan a, industri_table_kabupaten b 
											WHERE a.id_kabupaten = b.id_kabupaten 
										AND b.id_kabupaten != '1'");
	}
	
	public function record_count2($id_kabupaten) {
			return $this->db->count_all("industri_table_tanda_tangan a, industri_table_kabupaten b 
										 WHERE a.id_kabupaten=b.id_kabupaten
										 AND b.id_kabupaten != '1' AND b.id_kabupaten='".$id_kabupaten."'");
	}
	
	public function fetch_tanda_tangan($limit, $start) {
		$this->db->select('a.*, b.*');
		$this->db->from(' industri_table_tanda_tangan a, industri_table_kabupaten b');
		$this->db->where('a.id_kabupaten=b.id_kabupaten');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetch_tanda_tangan2($limit, $start, $id_kabupaten) {
		$this->db->select('a.*, b.*');
		$this->db->from(' industri_table_tanda_tangan a, industri_table_kabupaten b');
		$this->db->where('a.id_kabupaten=b.id_kabupaten');
		$this->db->where("b.id_kabupaten='$id_kabupaten'");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_tanda_tangan($key) {
		$this->db->select('a.*, b.nama_kabupaten');
		$this->db->from('industri_table_tanda_tangan a, industri_table_kabupaten b');
		$this->db->where("b.id_kabupaten != '1'");
		$this->db->where("a.id_kabupaten=b.id_kabupaten
							AND  (	a.nama LIKE '%".$key."%' ESCAPE '!' 
									OR a.nip LIKE '%".$key."%' ESCAPE '!'
									OR a.golongan LIKE '%".$key."%' ESCAPE '!'
									OR a.jabatan LIKE '%".$key."%' ESCAPE '!'
									OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')
						");
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	public function search_tanda_tangan2($key, $id_kabupaten) {
		$this->db->select('a.*, b.nama_kabupaten');
		$this->db->from('industri_table_tanda_tangan a, industri_table_kabupaten b');
		$this->db->where("b.id_kabupaten != '1'");
		$this->db->where("a.id_kabupaten=b.id_kabupaten
							AND b.id_kabupaten='".$id_kabupaten."'
							AND  (	a.nama LIKE '%".$key."%' ESCAPE '!' 
									OR a.nip LIKE '%".$key."%' ESCAPE '!'
									OR a.golongan LIKE '%".$key."%' ESCAPE '!'
									OR a.jabatan LIKE '%".$key."%' ESCAPE '!'
									OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')
						");
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>