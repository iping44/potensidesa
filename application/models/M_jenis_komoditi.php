<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_jenis_komoditi extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM industri_table_jenis_komoditi");
        return $query->result();
    }
	
	public function data2() {
        $query  = $this->db->query("SELECT * FROM industri_table_jenis_komoditi WHERE id_jenis_komoditi='1'");
        return $query->result();
    }
	
    public function dataPokok() {
        $query  = $this->db->query("SELECT * FROM industri_table_komoditi WHERE jenis_komoditi='Bahan Pokok'");
        return $query->result();
    }
	
	public function dataStrategis() {
        $query  = $this->db->query("SELECT * FROM industri_table_komoditi WHERE jenis_komoditi='Bahan Strategis'");
        return $query->result();
    }
	
	public function komoditi(){
        $query = $this->db->get('industri_table_komoditi');
        return $query->result();
    }
	
    public function create($data) {
        //get data
        $this->nama_jenis_komoditi = $data['nama_jenis_komoditi'];
		$this->keterangan = $data['keterangan'];
		
        //insert data
        $this->db->insert('industri_table_jenis_komoditi', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_jenis_komoditi = $data['id_jenis_komoditi'];
        $this->nama_jenis_komoditi = $data['nama_jenis_komoditi'];
		$this->keterangan = $data['keterangan'];
		
        //update data
        $this->db->update('industri_table_jenis_komoditi', $this, array('id_jenis_komoditi'=>$data['id_jenis_komoditi']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_jenis_komoditi', array('id_jenis_komoditi' => $id));
    }


    public function get($id){
        $this->db->where('id_jenis_komoditi', $id);
        $query = $this->db->get('industri_table_jenis_komoditi');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("industri_table_jenis_komoditi");
	}
	
	public function record_count_search($key) {
		$this->db->like("nama_jenis_komoditi", $key);
		$this->db->or_like("keterangan", $key);
		return $this->db->count_all_results("industri_table_jenis_komoditi");
	}
	
	public function fetch_jenis_komoditi($limit, $start) {
		$this->db->select('*');
		$this->db->from('industri_table_jenis_komoditi');
		$this->db->order_by('id_jenis_komoditi DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_jenis_komoditi($limit, $start, $key) {
		
		$this->db->like("nama_jenis_komoditi", $key);
		$this->db->or_like("keterangan", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("industri_table_jenis_komoditi");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>