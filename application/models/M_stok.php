<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindutrian

class M_stok extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    /*public function data() {
        $query = $this->db->query("SELECT * FROM stok a, industri_table_komoditi b, industri_table_pasar c WHERE a.id_komoditi=b.id_komoditi AND a.id_pasar=c.id_pasar AND b.jenis_komoditi='Bahan pokok' ORDER BY tanggal ASC");
        return $query->result();
    }*/
	
	 public function data() {
        $query = $this->db->query("SELECT * FROM industri_table_komoditi_stok ");
        return $query->result();
    }
	
	public function cekStok($id_kabupaten, $tahun) {
			return $this->db->count_all("industri_table_stok 
										WHERE id_kabupaten='$id_kabupaten' 
										AND tahun = '$tahun' ");
	}
	
	public function cekStokMingguan($id_kabupaten, $bulan, $minggu, $tahun) {
			return $this->db->count_all("industri_table_stok_mingguan 
										WHERE id_kabupaten='$id_kabupaten' 
										AND bulan = '$bulan' 
										AND minggu = '$minggu' 
										AND tahun = '$tahun' 
										");
	}
	
	
    public function create($data) {
        //get data
		//$this->id_harga_barang = $data['id_harga_barang'];
		$this->id_komoditi_stok = $data['id_komoditi_stok'];
		$this->tahun = $data['tahun'];
		$this->stok = $data['stok'];
		$this->ketahanan = $data['ketahanan'];
		$this->id_kabupaten = $data['id_kabupaten'];
		
        //insert data
        $this->db->insert('industri_table_stok', $this);
    }
	
    
    public function create_mingguan($data) {
        //get data
		$this->id_komoditi_stok = $data['id_komoditi_stok'];
		$this->bulan = $data['bulan'];
		$this->minggu = $data['minggu'];
		$this->tahun = $data['tahun'];
		$this->stok = $data['stok'];
		$this->ketahanan = $data['ketahanan'];
		$this->id_kabupaten = $data['id_kabupaten'];
		
        //insert data
        $this->db->insert('industri_table_stok_mingguan', $this);
    }
	
    
    public function update($data) {
        //get data
        $this->id_stok = $data['id_stok'];
        $this->stok = $data['stok'];
        $this->ketahanan = $data['ketahanan'];
        
        //update data
        $this->db->update('industri_table_stok', $this, array('id_stok'=>$data['id_stok']));
    }
    
    public function update_mingguan($data) {
        //get data
        $this->id_stok = $data['id_stok'];
        $this->stok = $data['stok'];
        $this->ketahanan = $data['ketahanan'];
        
        //update data
        $this->db->update('industri_table_stok_mingguan', $this, array('id_stok'=>$data['id_stok']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_stok', array('id_stok' => $id));
    }

    public function delete_mingguan($id) {
        $this->db->delete('industri_table_stok_mingguan', array('id_stok' => $id));
    }

    public function get($id){
        $this->db->where('a.id_stok', $id);
        $this->db->where('a.id_komoditi_stok = b.id_komoditi_stok');
        $query = $this->db->get('industri_table_stok a, industri_table_komoditi_stok b');
        return $query->result();
    }
	
    public function get_mingguan($id){
        $this->db->where('a.id_stok', $id);
        $this->db->where('a.id_komoditi_stok = b.id_komoditi_stok');
        $query = $this->db->get('industri_table_stok_mingguan a, industri_table_komoditi_stok b');
        return $query->result();
    }
	
	public function get2($id){
        $this->db->where('id_komoditi_stok', $id);
        $query = $this->db->get('industri_table_komoditi_stok');
        return $query->result();
    }
	
	public function search_stok($id_kabupaten, $tahun) {
		$this->db->select('a.*, b.*');
		$this->db->from('industri_table_stok a, industri_table_komoditi_stok b');
		$this->db->where("a.id_komoditi_stok=b.id_komoditi_stok 
						  AND a.tahun='$tahun' 
						  AND a.id_kabupaten='$id_kabupaten'");
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	public function search_stok_mingguan($id_kabupaten, $bulan, $minggu, $tahun) {
		$this->db->select('a.*, b.*');
		$this->db->from('industri_table_stok_mingguan a, industri_table_komoditi_stok b');
		$this->db->where("a.id_komoditi_stok=b.id_komoditi_stok 
						  AND a.bulan='$bulan' 
						  AND a.minggu='$minggu' 
						  AND a.tahun='$tahun' 
						  AND a.id_kabupaten='$id_kabupaten'");
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>