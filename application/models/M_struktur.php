<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_struktur extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_struktur");
        return $query->result();
    }		
	
	public function struktur(){
        $query = $this->db->get('table_struktur');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->nama = $data['nama'];
		$this->jabatan = $data['jabatan'];
		$this->deskripsi = $data['deskripsi'];
		$this->gambar = $data['gambar'];		
        
        //insert data
        $this->db->insert('table_struktur', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		$this->nama = $data['nama'];
		$this->jabatan = $data['jabatan'];
		$this->deskripsi = $data['deskripsi'];
		
        //insert data
        $this->db->insert('table_struktur', $this);
    }
    
    public function update($data) {
        //get data
        
        $this->nama = $data['nama'];
		$this->jabatan = $data['jabatan'];
		$this->deskripsi = $data['deskripsi'];
		$this->gambar = $data['gambar'];		
		
        //update data
        $this->db->update('table_struktur', $this, array('id_struktur'=>$data['id_struktur']));
    }
    
    public function delete($id) {
        $this->db->delete('table_struktur', array('id_struktur' => $id));
    }


    public function get($id){
        $this->db->where('id_struktur', $id);
        $query = $this->db->get('table_struktur');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_struktur");
	}
	
	public function record_count_search($key) {
		$this->db->like("nama", $key);
		$this->db->or_like("jabatan", $key);
		$this->db->or_like("deskripsi", $key);
		$this->db->or_like("gambar", $key);		
		return $this->db->count_all_results("table_struktur");
	}
	
	public function fetch_struktur($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_struktur');
		$this->db->order_by('id_struktur DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_struktur($limit, $start, $key) {
	
		$this->db->like("nama", $key);
		$this->db->or_like("jabatan", $key);
		$this->db->or_like("deskripsi", $key);
		$this->db->or_like("gambar", $key);		
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_struktur");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_struktur',$id);
		$query = $getData = $this->db->get('table_struktur');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_struktur = $data['id_struktur'];		
		$this->nama = $data['nama'];
		$this->jabatan = $data['jabatan'];
		$this->deskripsi = $data['deskripsi'];
        
        //update data
        $this->db->update('table_struktur', $this, array('id_struktur'=>$data['id_struktur']));
    }
	
}
?>