<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_about extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_about");
        return $query->result();
    }		
	
	public function about(){
        $query = $this->db->get('table_about');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->judul = $data['judul'];
		$this->isi = $data['isi'];
		$this->gambar = $data['gambar'];		
        
        //insert data
        $this->db->insert('table_about', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		$this->judul = $data['judul'];
		$this->isi = $data['isi'];
		
        //insert data
        $this->db->insert('table_about', $this);
    }
    
    public function update($data) {
        //get data
        
        $this->judul = $data['judul'];
		$this->isi = $data['isi'];
		$this->gambar = $data['gambar'];		
		
        //update data
        $this->db->update('table_about', $this, array('id_about'=>$data['id_about']));
    }
    
    public function delete($id) {
        $this->db->delete('table_about', array('id_about' => $id));
    }

    public function get($id){
        $this->db->where('id_about', $id);
        $query = $this->db->get('table_about');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_about");
	}
	
	public function record_count_search($key) {
		$this->db->like("judul", $key);
		$this->db->or_like("isi", $key);
		$this->db->or_like("gambar", $key);		
		return $this->db->count_all_results("table_about");
	}
	
	public function fetch_about($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_about');
		$this->db->order_by('id_about DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_about($limit, $start, $key) {
	
		$this->db->like("judul", $key);
		$this->db->or_like("isi", $key);
		$this->db->or_like("gambar", $key);		
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_about");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_about',$id);
		$query = $getData = $this->db->get('table_about');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_about = $data['id_about'];		
		$this->judul = $data['judul'];
		$this->isi = $data['isi'];
        
        //update data
        $this->db->update('table_about', $this, array('id_about'=>$data['id_about']));
    }
	
}
?>