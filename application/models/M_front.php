<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_front extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query  = $this->db->query("SELECT id_kategori, nama_jenis, keterangan FROM table_category");
        return $query->result();
    }
     
    public function data_id($id)
    {
        $query  = $this->db->query("SELECT id_kategori, nama_jenis, keterangan FROM table_category "
                                        . "where id_kategori = $id");
        return $query->result();
    }
    
    public function data_jenis_izin($id)
    {
        $query  = $this->db->query("SELECT a.id_jenis_izin, a.nama_jenis_izin, a.deskripsi, a.syarat, a.formulir, a.id_kategori, b.id_kategori, b.nama_jenis "
                                        . "FROM tabel_jenis_izin a, table_category b "
                                        . "WHERE a.id_kategori=b.id_kategori and a.id_kategori = '$id'");
        return $query->result();
    }
    
     public function data_izin($id)
    {
        $query  = $this->db->query("SELECT a.id_jenis_izin, a.nama_jenis_izin, a.deskripsi, a.syarat, a.formulir, a.id_kategori, b.id_kategori, b.nama_jenis "
                                        . "FROM tabel_jenis_izin a, table_category b "
                                        . "WHERE a.id_kategori=b.id_kategori and a.id_jenis_izin = '$id'");
        return $query->result();
    }
    
     public function createsaran($data) {
        //get data
        $this->nama_pengadu = $data['nama_pengadu'];
		$this->no_hp = $data['no_hp'];
		$this->isi_aduan = $data['isi_aduan'];
		$this->tgl = $data['tgl'];
		$this->jam = $data['jam'];
		$this->tgl_update = $data['tgl_update'];
		$this->jam_update = $data['jam_update'];
		$this->status = $data['status'];
        
        //insert data
        $this->db->insert('izin_table_pengaduan', $this);
    }
   
    public function create($data) {
        //get data
        $this->nama_jenis = $data['nama_jenis'];
        $this->keterangan = $data['keterangan'];
        
        
        //insert data
        $this->db->insert('table_category', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_kategori = $data['id_kategori'];
        $this->nama_jenis = $data['nama_jenis'];
        $this->keterangan = $data['keterangan'];
        
        //update data
        $this->db->update('table_category', $this, array('id_kategori'=>$data['id_kategori']));
    }
    
    public function delete($id) {
        $this->db->delete('table_category', array('id_kategori' => $id));
    }


    public function get($id){
        $this->db->where('id_kategori', $id);
        $query = $this->db->get('table_category', 1);
        return $query->result();
    }
    
}
?>