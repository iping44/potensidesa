<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//model for table_menu
class M_menu extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    //select menu by group_id
    public function menu($id) {
        $query  = $this->db->query("SELECT m.menu_id, m.menu_name, m.attribute, m.link, m.active, m.description,
      			m.adminmenu, m.position FROM table_menu m, table_group g, table_groupaccess a
      			WHERE g.group_id=a.group_id AND a.menu_id=m.menu_id AND m.active='Y' AND g.group_id='$id' ORDER BY m.position");
        return $query->result();
    }
    //select submenu by group_id
	  public function submenu($id) {
        $query  = $this->db->query("SELECT s.submenu_id, s.menu_id, s.submenu_name, s.attribute, s.link, s.active FROM
					table_menu m, table_group g, table_groupaccess a, table_submenu s
					WHERE g.group_id=a.group_id AND a.menu_id=m.menu_id AND s.menu_id=m.menu_id AND
					m.active='Y' AND g.group_id='$id' AND s.active='Y'");
        return $query->result();
    }
  //select all menu
	public function data() {
		$query  = $this->db->query("SELECT m.menu_id, m.menu_name, m.attribute, m.link, m.active,  m.description,
			m.adminmenu, m.position FROM table_menu m");
        return $query->result();
	}
    //insert data
    public function create($data) {

        //init data
        $this->menu_id = $data['menu_id'];
        $this->menu_name = $data['menu_name'];
        $this->attribute = $data['attribute'];
        $this->link = $data['link'];
        $this->active = $data['active'];
        $this->adminmenu = $data['adminmenu'];
        $this->position = $data['position'];
		    $this->description = $data['description'];

        //insert data
        $this->db->insert('table_menu', $this);
    }
    //update data
    public function update($data) {
        //init data
        $this->menu_name = $data['menu_name'];
        $this->attribute = $data['attribute'];
        $this->link = $data['link'];
        $this->active = $data['active'];
        $this->adminmenu = $data['adminmenu'];
        $this->position = $data['position'];
		    $this->description = $data['description'];

        //update data
        $this->db->update('table_menu', $this, array('menu_id'=>$data['menu_id']));
    }
    //delete data by menu_id
    public function delete($id) {
        //delete data
        $this->db->delete('table_menu', array('menu_id' => $id));
    }

    //get data by id
    public function get($id){
        $this->db->where('menu_id', $id);
        $query = $this->db->get('table_menu', 1);
        return $query->result();
    }

    //count all data
    public function record_count() {
		    return $this->db->count_all("table_menu");
	  }

  //count data by key
	public function record_count_search($key) {
		$this->db->like("menu_name", $key);
		$this->db->or_like("attribute", $key);
		$this->db->or_like("link", $key);
		$this->db->or_like("active", $key);
		$this->db->or_like("adminmenu", $key);
		$this->db->or_like("position", $key);
		$this->db->or_like("description", $key);
		return $this->db->count_all_results("table_menu");
	}
  //select data with limit for pagination
	public function fetch_menu($limit, $start) {
		$this->db->limit($limit, $start);
		$this->db->order_by('position');
		$query = $this->db->get("table_menu");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

  //select data with limit and key for pagination
	public function search_menu($limit, $start, $key) {
		$this->db->like("menu_name", $key);
		$this->db->or_like("attribute", $key);
		$this->db->or_like("link", $key);
		$this->db->or_like("active", $key);
		$this->db->or_like("adminmenu", $key);
		$this->db->or_like("position", $key);
		$this->db->or_like("description", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_menu");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;

	}

}
?>
