<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_testi extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_testi");
        return $query->result();
    }		
	
	public function testi(){
        $query = $this->db->get('table_testi');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		$this->id_testi = $data['id_testi'];
        $this->nama = $data['nama'];
		$this->jabatan = $data['jabatan'];
		$this->testimoni = $data['testimoni'];
		
        
        //insert data
        $this->db->insert('table_testi', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_testi = $data['id_testi'];
        $this->nama = $data['nama'];
		$this->jabatan = $data['jabatan'];
		$this->testimoni = $data['testimoni'];		
		
        //update data
        $this->db->update('table_testi', $this, array('id_testi'=>$data['id_testi']));
    }
    
    public function delete($id) {
        $this->db->delete('table_testi', array('id_testi' => $id));
    }


    public function get($id){
        $this->db->where('id_testi', $id);
        $query = $this->db->get('table_testi');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_testi");
	}
	
	public function record_count_search($key) {
		$this->db->like("nama", $key);
		$this->db->or_like("jabatan", $key);
		$this->db->or_like("testimoni", $key);		
		return $this->db->count_all_results("table_testi");
	}
	
	public function fetch_testimoni($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_testi');
		$this->db->order_by('id_testi DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_testimoni($limit, $start, $key) {
	
		$this->db->like("nama", $key);
		$this->db->or_like("jabatan", $key);
		$this->db->or_like("testimoni", $key);		
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_testi");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>