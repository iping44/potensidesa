<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_dokter extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_dokter");
        return $query->result();
    }		
	
	public function dokter(){
        $query = $this->db->get('table_dokter');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->nama_dokter = $data['nama_dokter'];
		$this->divisi = $data['divisi'];
		$this->deskripsi = $data['deskripsi'];
		$this->gambar = $data['gambar'];
		$this->fb = $data['fb'];
		$this->twitter = $data['twitter'];
		$this->instagram = $data['instagram'];
        
        //insert data
        $this->db->insert('table_dokter', $this);
    }
	
	//upload image
	public function createnoimage($data) {
        //get data
		
		$this->nama_dokter = $data['nama_dokter'];
		$this->divisi = $data['divisi'];
		$this->deskripsi = $data['deskripsi'];
		$this->fb = $data['fb'];
		$this->twitter = $data['twitter'];
		$this->instagram = $data['instagram'];
		
        //insert data
        $this->db->insert('table_dokter', $this);
    }
    
    public function update($data) {
        //get data
        
        $this->nama_dokter = $data['nama_dokter'];
		$this->divisi = $data['divisi'];
		$this->deskripsi = $data['deskripsi'];
		$this->gambar = $data['gambar'];
		$this->fb = $data['fb'];
		$this->twitter = $data['twitter'];
		$this->instagram = $data['instagram'];
		
        //update data
        $this->db->update('table_dokter', $this, array('id_dokter'=>$data['id_dokter']));
    }
    
    public function delete($id) {
        $this->db->delete('table_dokter', array('id_dokter' => $id));
    }


    public function get($id){
        $this->db->where('id_dokter', $id);
        $query = $this->db->get('table_dokter');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_dokter");
	}
	
	public function record_count_search($key) {
		$this->db->like("nama_dokter", $key);
		$this->db->or_like("divisi", $key);
		$this->db->or_like("deskripsi", $key);
		$this->db->or_like("gambar", $key);
		$this->db->or_like("fb", $key);
		$this->db->or_like("twitter", $key);
		$this->db->or_like("instagram", $key);
		return $this->db->count_all_results("table_dokter");
	}
	
	public function fetch_dokter($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_dokter');
		$this->db->order_by('id_dokter DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_dokter($limit, $start, $key) {
	
		$this->db->like("nama_dokter", $key);
		$this->db->or_like("divisi", $key);
		$this->db->or_like("deskripsi", $key);
		$this->db->or_like("gambar", $key);
		$this->db->or_like("fb", $key);
		$this->db->or_like("twitter", $key);
		$this->db->or_like("instagram", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_dokter");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_dokter',$id);
		$query = $getData = $this->db->get('table_dokter');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_dokter = $data['id_dokter'];		
		$this->nama_dokter = $data['nama_dokter'];
		$this->divisi = $data['divisi'];
		$this->deskripsi = $data['deskripsi'];		
		$this->fb = $data['fb'];
		$this->twitter = $data['twitter'];
		$this->instagram = $data['instagram'];
        
        //update data
        $this->db->update('table_dokter', $this, array('id_dokter'=>$data['id_dokter']));
    }
	
}
?>