<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_jenisjurnal extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_jenisjurnal");
        return $query->result();
    }		
	
	public function jenisjurnal(){
        $query = $this->db->get('table_jenisjurnal');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		
        $this->nama_jenisjurnal = $data['nama_jenisjurnal'];
		$this->deskripsi = $data['deskripsi'];				
        
        //insert data
        $this->db->insert('table_jenisjurnal', $this);
    }

    //upload image
	public function createnoimage($data) {
        //get data
		
		
		$this->nama_jenisjurnal = $data['nama_jenisjurnal'];
		$this->deskripsi = $data['deskripsi'];
		
		
        //insert data
        $this->db->insert('table_jenisjurnal', $this);
    }
    
    public function update($data) {
        //get data
        
        
		$this->nama_jenisjurnal = $data['nama_jenisjurnal'];
		$this->deskripsi = $data['deskripsi'];
			
		
        //update data
        $this->db->update('table_jenisjurnal', $this, array('id_jenisjurnal'=>$data['id_jenisjurnal']));
    }
    
    public function delete($id) {
        $this->db->delete('table_jenisjurnal', array('id_jenisjurnal' => $id));
    }


    public function get($id){
        $this->db->where('id_jenisjurnal', $id);
        $query = $this->db->get('table_jenisjurnal');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_jenisjurnal");
	}
	
	public function record_count_search($key) {
		$this->db->like("gambar", $key);
		$this->db->or_like("nama_jenisjurnal", $key);
		$this->db->or_like("deskripsi", $key);		
		return $this->db->count_all_results("table_jenisjurnal");
	}
	
	public function fetch_jenisjurnal($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_jenisjurnal');
		$this->db->order_by('id_jenisjurnal DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_jenisjurnal($limit, $start, $key) {
	
		
		$this->db->like("nama_jenisjurnal", $key);
		$this->db->or_like("deskripsi", $key);			
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_jenisjurnal");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}

	//upload image
	public function link_gambar($id)
	{
		
		$this->db->where('id_jenisjurnal',$id);
		$query = $getData = $this->db->get('table_jenisjurnal');

		if($getData->num_rows() > 0)
		return $query;
		else
		return null;
			
	}
	
	//upload image
	public function updatenoimage($data) {
         //get data
		$this->id_jenisjurnal = $data['id_jenisjurnal'];			
		$this->nama_jenisjurnal = $data['nama_jenisjurnal'];
		$this->deskripsi = $data['deskripsi'];
        
        //update data
        $this->db->update('table_jenisjurnal', $this, array('id_jenisjurnal'=>$data['id_jenisjurnal']));
    }
	
}
?>