<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perizinan

class M_backoffice extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data($nup) {
        $query  = $this->db->query("SELECT * 
                                        FROM izin_table_ceklist n, table_user u WHERE n.userid=u.userid AND n.nup = '$nup'");
        return $query->result();
    }
	
    public function create_bayar($data) {
        //get data
        $this->ceklist_id = $data['ceklist_id'];
		$this->nup = $data['nup'];
        $this->pratinjau = $data['pratinjau'];
		$this->keterangan_backoffice = $data['keterangan_backoffice'];
		$this->userid = $data['userid'];
		$this->logdate = date('Y-m-d');
		$this->logtime = date('H:i:s');
        
        //insert data
        $this->db->insert('izin_table_ceklist', $this);
    }
	
	  
    public function update_bayar($data) {
        //get data
        $$this->pratinjau = $data['pratinjau'];
		$this->keterangan_backoffice = $data['keterangan_backoffice'];
		$this->userid = $data['userid'];
        
        //update data
        $this->db->update('izin_table_ceklist', $this, array('nup'=>$data['nup'], 'ceklist_id'=>$data['ceklist_id']));
    }
	
	
    public function delete($id) {
        $this->db->delete('izin_table_ceklist', array('nup' => $id));
    }


	public function get($nup){
		$this->db->select('*');
		$this->db->from('izin_table_ceklist r');
		$this->db->join('table_user u', 'u.userid=r.userid');
		$this->db->where('r.nup', $nup);
        $query = $this->db->get();
        return $query->result();
    }
	
	public function get_kecamatan(){
        $query = $this->db->get('izin_table_kecamatan');
        return $query->result();
    }
	
	public function update_permohonan($data) {
        //get data
        $this->nup = $data['nup'];
		$this->izin_status = $data['izin_status'];
        $this->izin_tgl_terbit = $data['izin_tgl_terbit'];
        $this->izin_lama_berlaku = $data['izin_lama_berlaku'];
        $this->izin_berlaku_sd = $data['izin_berlaku_sd'];
        $this->izin_reg_ulang = $data['izin_reg_ulang'];
        $this->izin_no = $data['izin_no'];
        $this->izin_no_surat_stp = $data['izin_no_surat_stp'];
        $this->izin_no_seri = $data['izin_no_seri'];
        $this->izin_no_pengantar = $data['izin_no_pengantar'];
        $this->izin_no_penetap_ret = $data['izin_no_penetap_ret'];
        $this->izin_no_rekom_tek = $data['izin_no_rekom_tek'];
        $this->izin_no_lap_rek_tek = $data['izin_no_lap_rek_tek'];
        $this->izin_no_permohonan = $data['izin_no_permohonan'];
        
        //update data
        $this->db->update('izin_table_permohonan', $this, array('nup'=>$data['nup']));
    }

    public function update_perusahaan($data) {
        //get data
        $this->badan_usaha = $data['badan_usaha'];
        $this->bidang_usaha = $data['bidang_usaha'];
        $this->nama_perusahaan = $data['nama_perusahaan'];
        $this->nama_direktur = $data['nama_direktur'];
        $this->status_badan_hukum = $data['status_badan_hukum'];
        $this->alamat_perusahaan = $data['alamat_perusahaan'];
        $this->telepon_perusahaan = $data['telepon_perusahaan'];
        $this->email_perusahaan = $data['email_perusahaan'];
        $this->nomor_akta = $data['nomor_akta'];
        $this->jum_tenaga_kerja = $data['jum_tenaga_kerja'];
        
        //update data
        $this->db->update('izin_table_perusahaan', $this, array('npwp_perusahaan'=>$data['npwp_perusahaan']));
    }

    public function cek_survey($nup, $npwp_perusahaan){
        $this->db->select('*');
        $this->db->from('izin_table_survey s');
        $this->db->join('izin_table_permohonan i', 'i.nup=s.nup');
        $this->db->join('izin_table_perusahaan p', 'p.npwp_perusahaan=s.npwp_perusahaan');
        $this->db->where('i.nup', $nup);
        $this->db->where('p.npwp_perusahaan', $npwp_perusahaan);
        $query = $this->db->get();
        return $query->result();
    }

    public function cek_keterangan($nup, $npwp_perusahaan){
        $this->db->select('*');
        $this->db->from('izin_table_keterangan s');
        $this->db->join('izin_table_permohonan i', 'i.nup=s.nup');
        $this->db->join('izin_table_perusahaan p', 'p.npwp_perusahaan=s.npwp_perusahaan');
        $this->db->where('i.nup', $nup);
        $this->db->where('p.npwp_perusahaan', $npwp_perusahaan);
        $query = $this->db->get();
        return $query->result();
    }
	
	public function get_kelurahan ($id_kecamatan) {
        $this->db->select('*');
        $this->db->where('id_kecamatan',$id_kecamatan);
        $result = $this->db->get('izin_table_kelurahan');
        if ($result->num_rows() > 0)
        {
            return $result->result_array();
        }
        else{
            return array();
        }
    }

    public function get_kelurahanx($id){
        $this->db->where('id_kecamatan', $id);
        $query = $this->db->get('izin_table_kelurahan');
        return $query->result();
    }
	
	public function sms($sent){
        
		$this->DestinationNumber = $sent['DestinationNumber'];
		$this->TextDecoded = $sent['TextDecoded'];
		
		$this->db->insert('outbox', $this);
            
	}
    
}
?>