<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindutrian

class M_pelabuhan extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function data() {
        $query  = $this->db->query("SELECT * FROM industri_table_pelabuhan a, industri_table_kabupaten b WHERE a.id_kabupaten = b.id_kabupaten");
        return $query->result();
    }
	
	 public function data2() {
        $query  = $this->db->query("SELECT * FROM industri_table_pelabuhan a, industri_table_kabupaten b WHERE a.id_kabupaten = b.id_kabupaten GROUP BY a.id_kabupaten");
        return $query->result();
    }
	
	public function data3($id_kabupaten) {
        $query  = $this->db->query("SELECT * FROM industri_table_pelabuhan a, industri_table_kabupaten b WHERE a.id_kabupaten = b.id_kabupaten AND a.id_kabupaten='$id_kabupaten'  GROUP BY a.id_kabupaten");
        return $query->result();
    }
	
	public function data4($id_kabupaten) {
        $query  = $this->db->query("SELECT * FROM industri_table_pelabuhan a, industri_table_kabupaten b WHERE a.id_kabupaten = b.id_kabupaten AND a.id_kabupaten='$id_kabupaten' ");
        return $query->result();
    }
	
	public function pelabuhan(){
        $query = $this->db->get('industri_table_pelabuhan');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		$this->id_pelabuhan = $data['id_pelabuhan'];
		$this->nama_pelabuhan = $data['nama_pelabuhan'];
        $this->id_kabupaten = $data['id_kabupaten'];
		
        //insert data
        $this->db->insert('industri_table_pelabuhan', $this);
    }
    
    public function update($data) {
        //get data
		$this->id_pelabuhan = $data['id_pelabuhan'];
		$this->nama_pelabuhan = $data['nama_pelabuhan'];
        $this->id_kabupaten = $data['id_kabupaten'];
        
        //update data
        $this->db->update('industri_table_pelabuhan', $this, array('id_pelabuhan'=>$data['id_pelabuhan']));
    }
    
    public function delete($id) {
        $this->db->delete('industri_table_pelabuhan', array('id_pelabuhan' => $id));
    }

    public function get($id){
        $this->db->where('id_pelabuhan', $id);
        $query = $this->db->get('industri_table_pelabuhan');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("industri_table_kabupaten a, industri_table_pelabuhan b WHERE a.id_kabupaten=b.id_kabupaten AND b.id_kabupaten!='1'");
	}
	
	public function record_count2($id_kabupaten) {
			return $this->db->count_all("industri_table_pelabuhan a, industri_table_kabupaten b WHERE a.id_kabupaten=b.id_kabupaten 
										AND b.id_kabupaten='".$id_kabupaten."'");
	}
	
	public function record_count3($key) {
			return $this->db->count_all("industri_table_pelabuhan a, industri_table_kabupaten b 
										 WHERE	a.id_kabupaten=b.id_kabupaten
										 AND b.id_kabupaten!='1'
										AND  (	a.nama_pelabuhan LIKE '%".$key."%' ESCAPE '!' 
												OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function record_count4($id_kabupaten, $key) {
			return $this->db->count_all("industri_table_pelabuhan a, industri_table_kabupaten b 
										 WHERE	a.id_kabupaten=b.id_kabupaten
										AND b.id_kabupaten='".$id_kabupaten."'
										AND  (	a.nama_pelabuhan LIKE '%".$key."%' ESCAPE '!' 
												OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')");
	}
	
	public function fetch_pelabuhan($limit, $start) {
		$this->db->select('a.*, b.*');
		$this->db->from('industri_table_pelabuhan a, industri_table_kabupaten b');
		$this->db->where('a.id_kabupaten=b.id_kabupaten');
		$this->db->where("b.id_kabupaten!='1'");
		$this->db->order_by("a.id_pelabuhan DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function fetch_pelabuhan2($limit, $start, $id_kabupaten) {
		$this->db->select('a.*, b.*');
		$this->db->from('industri_table_pelabuhan a, industri_table_kabupaten b');
		$this->db->where('a.id_kabupaten=b.id_kabupaten');
		$this->db->where("b.id_kabupaten='$id_kabupaten'");
		$this->db->order_by("a.id_pelabuhan DESC");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_pelabuhan($limit, $start, $key) {
		$this->db->select('a.*, b.*');
		$this->db->from('industri_table_pelabuhan a, industri_table_kabupaten b');
		$this->db->where("a.id_kabupaten=b.id_kabupaten 
							AND  (a.nama_pelabuhan LIKE '%".$key."%' ESCAPE '!' OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
	public function search_pelabuhan2($limit, $start, $key, $id_kabupaten) {
		$this->db->select('a.*, b.*');
		$this->db->from('industri_table_pelabuhan a, industri_table_kabupaten b');
		$this->db->where("a.id_kabupaten=b.id_kabupaten");
		$this->db->where("b.id_kabupaten='".$id_kabupaten."'
						AND  (a.nama_pelabuhan LIKE '%".$key."%' ESCAPE '!' OR b.nama_kabupaten LIKE '%".$key."%' ESCAPE '!')
						");
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>