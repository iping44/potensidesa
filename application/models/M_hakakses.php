<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_hakakses extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data($id) {
		$query  = $this->db->query("SELECT * FROM table_groupaccess s, table_menu m 
			WHERE s.menu_id=m.menu_id AND s.group_id='$id'");
        return $query->result();
	}
	
    public function create($data) {
        //get data
        $this->group_id = $data['group_id'];
        $this->menu_id = $data['menu_id'];
        $this->c = $data['c'];
        $this->r = $data['r'];
        $this->u = $data['u'];
        $this->d = $data['d'];
		$this->s = $data['s'];
        
        //insert data
        $this->db->insert('table_groupaccess', $this);
    }
    
    public function update($data) {
        //get data
		$this->menu_id = $data['menu_id'];
        $this->c = $data['c'];
        $this->r = $data['r'];
        $this->u = $data['u'];
        $this->d = $data['d'];
		$this->s = $data['s'];
        
        //update data
        $this->db->update('table_groupaccess', $this, array('group_id'=>$data['group_id'], 'menu_id'=>$data['menu_id2']));
    }
    
    public function delete($id1, $id2) {
        $this->db->delete('table_groupaccess', array('group_id'=>$id1, 'menu_id'=>$id2));
    }


    public function get($id1, $id2){
        $this->db->where('group_id', $id1);
		$this->db->where('menu_id', $id2);
        $query = $this->db->get('table_groupaccess', 1);
        return $query->result();
    }
    
	public function record_count($id) {
		$this->db->where("group_id", $id);
		return $this->db->count_all_results("table_groupaccess");
	}
	
	public function record_count_search($key, $id) {
		$this->db->where("menu_id", $id);
		$this->db->like("submenu_name", $key);
		$this->db->or_like("attribute", $key);
		$this->db->or_like("link", $key);
		$this->db->or_like("active", $key);
		return $this->db->count_all_results("table_groupaccess");
	}
    
	public function fetch_hakakses($limit, $start, $id) {
		$this->db->select('j.*, p.*');
		$this->db->from('table_groupaccess j');
		$this->db->join('table_menu p', 'p.menu_id=j.menu_id');
		$this->db->where("j.group_id", $id);
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_hakakses($limit, $start, $key, $id) {
		$this->db->where("menu_id", $id);
		$this->db->like("submenu_name", $key);
		$this->db->or_like("attribute", $key);
		$this->db->or_like("link", $key);
		$this->db->or_like("active", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_groupaccess");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}
}
?>