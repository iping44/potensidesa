<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_jadwal extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_jadwal");
        return $query->result();
    }		
	
	public function jadwal(){
        $query = $this->db->get('table_jadwal');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		$this->id_jadwal = $data['id_jadwal'];
        $this->nama_dokter = $data['nama_dokter'];
		$this->senin = $data['senin'];
		$this->selasa = $data['selasa'];
		$this->rabu = $data['rabu'];
		$this->kamis = $data['kamis'];
		$this->jumat = $data['jumat'];
		$this->sabtu = $data['sabtu'];
        
        //insert data
        $this->db->insert('table_jadwal', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_jadwal = $data['id_jadwal'];
        $this->nama_dokter = $data['nama_dokter'];
		$this->senin = $data['senin'];
		$this->selasa = $data['selasa'];
		$this->rabu = $data['rabu'];
		$this->kamis = $data['kamis'];
		$this->jumat = $data['jumat'];
		$this->sabtu = $data['sabtu'];
		
        //update data
        $this->db->update('table_jadwal', $this, array('id_jadwal'=>$data['id_jadwal']));
    }
    
    public function delete($id) {
        $this->db->delete('table_jadwal', array('id_jadwal' => $id));
    }


    public function get($id){
        $this->db->where('id_jadwal', $id);
        $query = $this->db->get('table_jadwal');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_jadwal");
	}
	
	public function record_count_search($key) {
		$this->db->like("nama_dokter", $key);
		$this->db->or_like("senin", $key);
		$this->db->or_like("selasa", $key);
		$this->db->or_like("rabu", $key);
		$this->db->or_like("kamis", $key);
		$this->db->or_like("jumat", $key);
		$this->db->or_like("sabtu", $key);
		return $this->db->count_all_results("table_jadwal");
	}
	
	public function fetch_jadwal($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_jadwal');
		$this->db->order_by('id_jadwal DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_jadwal($limit, $start, $key) {
	
		$this->db->like("nama_dokter", $key);
		$this->db->or_like("senin", $key);
		$this->db->or_like("selasa", $key);
		$this->db->or_like("rabu", $key);
		$this->db->or_like("kamis", $key);
		$this->db->or_like("jumat", $key);
		$this->db->or_like("sabtu", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_jadwal");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>