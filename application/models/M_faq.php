<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//model for modul sistem perindustrian

class M_faq extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
	public function data() {
        $query  = $this->db->query("SELECT * FROM table_faq");
        return $query->result();
    }		
	
	public function faq(){
        $query = $this->db->get('table_faq');
        return $query->result();
    }
	
    public function create($data) {
        //get data
		$this->id_faq = $data['id_faq'];
        $this->tanya = $data['tanya'];
		$this->jawab = $data['jawab'];
		        
        //insert data
        $this->db->insert('table_faq', $this);
    }
    
    public function update($data) {
        //get data
        $this->id_faq = $data['id_faq'];
        $this->tanya = $data['tanya'];
		$this->jawab = $data['jawab'];
				
        //update data
        $this->db->update('table_faq', $this, array('id_faq'=>$data['id_faq']));
    }
    
    public function delete($id) {
        $this->db->delete('table_faq', array('id_faq' => $id));
    }


    public function get($id){
        $this->db->where('id_faq', $id);
        $query = $this->db->get('table_faq');
        return $query->result();
    }
	
	public function record_count() {
			return $this->db->count_all("table_faq");
	}
	
	public function record_count_search($key) {
		$this->db->like("tanya", $key);
		$this->db->or_like("jawab", $key);
		return $this->db->count_all_results("table_faq");
	}
	
	public function fetch_faq($limit, $start) {
		$this->db->select('*');
		$this->db->from('table_faq');
		$this->db->order_by('id_faq DESC');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}
	
	public function search_faq($limit, $start, $key) {
	
		$this->db->like("tanya", $key);
		$this->db->or_like("jawab", $key);		
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_faq");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
		
	}
	
}
?>