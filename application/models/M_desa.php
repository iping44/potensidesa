<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//model for table_menu
class M_desa extends CI_Model{

    function __construct() {
        parent::__construct();
    }

  //select all menu
	public function data() {
		$query  = $this->db->query("SELECT * FROM table_desa d Left Join table_kecamatan k ON k.kec_id=d.kec_id");
        return $query->result();
	}
    //insert data
    public function create($data) {

        //init data
        $this->nama_desa = $data['nama_desa'];
        $this->jenis = $data['jenis'];
        $this->keterangan = $data['keterangan'];
        $this->kec_id = $data['kec_id'];
        $this->luas = $data['luas'];

        //insert data
        $this->db->insert('table_desa', $this);
    }
    //update data
    public function update($data) {
        //init data
        $this->nama_desa = $data['nama_desa'];
        $this->jenis = $data['jenis'];
        $this->keterangan = $data['keterangan'];
        $this->kec_id = $data['kec_id'];
        $this->luas = $data['luas'];

        //update data
        $this->db->update('table_desa', $this, array('desa_id'=>$data['desa_id']));
    }
    //delete data by menu_id
    public function delete($id) {
        //delete data
        $this->db->delete('table_desa', array('desa_id' => $id));
    }

    //get data by id
    public function get($id){
        $this->db->where('desa_id', $id);
        $query = $this->db->get('table_desa', 1);
        return $query->result();
    }

    //count all data
    public function record_count() {
		    return $this->db->count_all("table_desa");
	  }

  //count data by key
	public function record_count_search($key) {
		$this->db->like("nama_desa", $key);
		return $this->db->count_all_results("table_desa");
	}
  //select data with limit for pagination
	public function fetch_desa($limit, $start) {
    $this->db->select('*');
    $this->db->from('table_desa d');
    $this->db->join('table_kecamatan k', 'k.kec_id=d.kec_id');
		$this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

  //select data with limit and key for pagination
	public function search_desa($limit, $start, $key) {
		$this->db->like("nama_desa", $key);
		$this->db->limit($limit, $start);
		$query = $this->db->get("table_desa");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;

	}

}
?>
