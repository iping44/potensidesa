<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//Model for table_log
class M_log extends CI_Model{

    function __construct() {
        parent::__construct();
    }

    //select data
    public function data() {
        $query  = $this->db->query("SELECT log_id, userid, logdate, logtime, description
                                        FROM table_log");
        return $query->result();
    }
    //select data bu user_id
	  public function data_by_user($userid) {
        $query  = $this->db->query("SELECT log_id, userid, logdate, logtime, description
                                        FROM table_log WHERE userid='$userid'");
        return $query->result();
    }

    //insert data
    public function create($userid, $description) {
        //get data

        $this->userid = $userid;
    		$this->logdate = date('Y-m-d');
    		$this->logtime = date('H:i:s');
    		$this->description = $description;

        //insert data
        $this->db->insert('table_log', $this);

    }

    //get data by id
    public function get($id){
        $this->db->where('log_id', $id);
        $query = $this->db->get('table_log', 1);
        return $query->result();
    }



}
?>
