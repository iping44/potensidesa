<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user_profil extends CI_Model{
    
    function __construct() {
        parent::__construct();
    }
    
    public function update_nopassword($data) {
        //get data
        $this->username = $data['username'];
		$this->fullname = $data['fullname'];
        $this->email = $data['email'];
        
        //update data
        $this->db->update('table_user', $this, array('userid'=>$data['userid']));
    }
	
	public function update_withpassword($data) {
        //get data
        $this->username = $data['username'];
		$this->fullname = $data['fullname'];
        $this->password = $data['password'];
        $this->email = $data['email'];
        
        //update data
        $this->db->update('table_user', $this, array('userid'=>$data['userid']));
    }
    
    public function get($id){
        $this->db->where('userid', $id);
        $query = $this->db->get('table_user', 1);
        return $query->result();
    }
	
	public function tambah_photo($data) {
        //get data
        $this->photo = $data['file'];
        
        //update data
        $this->db->update('table_user', $this, array('userid'=>$data['userid']));
    }
    
}
?>