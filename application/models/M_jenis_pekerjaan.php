<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_jenis_pekerjaan extends CI_Model{

    function __construct() {
        parent::__construct();
    }

	public function data($id) {
		$query  = $this->db->query("SELECT * FROM table_jenis_pekerjaan ");
        return $query->result();
	}

    public function create($data) {
        //get data
        $this->jenis_pekerjaan_id = $data['jenis_pekerjaan_id'];
        $this->jenis_pekerjaan = $data['jenis_pekerjaan'];

        //insert data
        $this->db->insert('table_jenis_pekerjaan', $this);
    }

    public function update($data) {
        //get data
        $this->jenis_pekerjaan_id = $data['jenis_pekerjaan_id'];
        $this->jenis_pekerjaan = $data['jenis_pekerjaan'];

        //update data
        $this->db->update('table_jenis_pekerjaan', $this, array('jenis_pekerjaan_id'=>$data['jenis_pekerjaan_id']));
    }

    public function delete($id) {
        $this->db->delete('table_jenis_pekerjaan', array('jenis_pekerjaan_id'=>$id));
    }


    public function get($id){
        // $this->db->where('desa_id', $id);
		      $this->db->where('jenis_pekerjaan_id', $id);
        $query = $this->db->get('table_jenis_pekerjaan', 1);
        return $query->result();
    }

	public function record_count($id) {
		$this->db->where("jenis_pekerjaan_id", $id);
		return $this->db->count_all_results("table_jenis_pekerjaan");
	}

	public function record_count_search($key, $id) {
		$this->db->where("jenis_pekerjaan_id", $id);
		$this->db->like("jenis_pekerjaan_id", $key);
		$this->db->or_like("jenis_pekerjaan", $key);
		return $this->db->count_all_results("table_jenis_pekerjaan");
	}

	public function fetch_jenis_pekerjaan($limit, $start, $id) {
		$this->db->select('*');
		$this->db->from('table_jenis_pekerjaan');
		// $this->db->join('table_jenis_pekerjaan g', 'g.desa_id=d.desa_id');
		// $this->db->where("d.jenis_pekerjaan_id", $id);
		// $this->db->limit($limit, $start);
		$query = $this->db->get();
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return false;
	}

	public function search_jenis_pekerjaan($limit, $start, $key, $id) {
    $this->db->where("jenis_pekerjaan_id", $id);
		$this->db->like("jenis_pekerjaan_id", $key);
		$this->db->or_like("jenis_pekerjaan", $key);
		$query = $this->db->get("table_jenis_pekerjaan");
		if($query->num_rows() > 0) {
			foreach($query->result() as $row) {
				$data[] = $row;
			}
			return $data;
		}
		return null;
	}
}
?>
