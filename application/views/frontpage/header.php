<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="author" content="">
		<title>Aplikasi Perindustrian Prov. Sulawesi Tenggara</title>
		<link rel="shortcut icon" href="<?php echo base_url()."asset/"?>template/images/sultra.png">
		<!-- Bootstrap Core CSS -->
		<link href="<?php echo base_url()?>asset/template/css/bootstrap.min.css" rel="stylesheet">
		<!-- Custom Fonts -->
		<link href="<?php echo base_url()?>asset/template/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<!-- Custom CSS -->
		<link rel="stylesheet" href="<?php echo base_url()?>asset/template/css/patros.css" >
		<link rel="stylesheet" href="<?php echo base_url()?>asset/template/css/beranda.css" >
		<link href="<?php echo base_url()."asset/"?>css/datepicker3.css" rel="stylesheet">
		
		
		<!-- Custom Chart -->
		<!--<script src="https://code.highcharts.com"></script>
        <script type="text/javascript" src="<?php echo base_url('/asset/highcharts/modules/exporting.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/asset/highcharts/exporting.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/asset/highcharts/themes/skies.js'); ?>"></script>-->
		<script type="text/javascript" src="<?php echo base_url('/asset/highcharts/jquery-1.7.2.min.js'); ?>"></script>
		
		<link type="text/css" rel="stylesheet" href="<?php echo base_url()."asset/"?>css/bootstrap-select.css"/>
		<script src="<?php echo base_url()."asset/"?>js/dataTables/jquery.min.js"></script>
		<script src="<?php echo base_url()."asset/"?>js/bootstrap-select.js"></script>
        
		<!--grafik-->
		<script type="text/javascript" src="<?php echo base_url('/asset/highcharts/highchart.js'); ?>"></script>
		
	
		

		<!-- Additional files for the Highslide popup effect 
		<script src="https://www.highcharts.com/samples/static/highslide-full.min.js"></script>
		<script src="https://www.highcharts.com/samples/static/highslide.config.js" charset="utf-8"></script>
		<link rel="stylesheet" type="text/css" href="https://www.highcharts.com/samples/static/highslide.css" />-->
		
		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
			<![endif]-->
	</head>
