<!--==============================
              footer
=================================-->
<footer id="footer">
  <div class="container">
    <div class="row">
      <div class="grid_12"> 
          <div class="ani" id="div4"><div class="copyright"><span class="brand">Kios Informasi Perizinan</span> &copy; <span id="copyright-year"></span> | 
          <div class="sub-copy">Kota Kendari Sulawesi Tenggara </div>
        </div>
          </div>
      </div>
    </div>
  </div>  
</footer>
<a href="#" id="toTop" class="fa fa-chevron-up"></a>
</body>
</html>