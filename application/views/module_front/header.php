<!DOCTYPE html>
<html lang="en">
<head>
<title>Perizinan</title>
<meta charset="utf-8">
<meta name="format-detection" content="telephone=no" />
<link rel="icon" href="<?php echo base_url()."asset/front/"?>images/favicon.ico">
<link rel="shortcut icon" href="<?php echo base_url()."asset/front/"?>images/favicon.ico" />
<link rel="stylesheet" href="<?php echo base_url()."asset/front/"?>css/style.css">
<link rel="stylesheet" href="<?php echo base_url()."asset/front/"?>css/vendor.css">
<script src="<?php echo base_url()."asset/front/"?>js/jquery.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery-migrate-1.1.1.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/script.js"></script> 
<script src="<?php echo base_url()."asset/front/"?>js/superfish.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery.equalheights.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery.mobilemenu.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/tmStickUp.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery.ui.totop.js"></script>
<link rel="icon" href="images/favicon.ico">
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" href="<?php echo base_url()."asset/front/"?>css/contact-form.css">
<link rel="stylesheet" href="<?php echo base_url()."asset/front/"?>css/new2.css">
<link rel="stylesheet" href="<?php echo base_url()."asset/front/"?>css/bootstrap.min.css">

<script src="<?php echo base_url()."asset/front/"?>js/jquery.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery-migrate-1.1.1.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery.easing.1.3.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/script.js"></script> 
<script src="<?php echo base_url()."asset/front/"?>js/superfish.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery.equalheights.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery.mobilemenu.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/tmStickUp.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/jquery.ui.totop.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/TMForm.js"></script>
<script src="<?php echo base_url()."asset/front/"?>js/modal.js"></script>


<!-- jQuery (required) & jQuery UI + theme (optional) -->
	<link href="<?php echo base_url()."asset/front/jquery/"?>docs/css/jquery-ui.min.css" rel="stylesheet">
	<script src="<?php echo base_url()."asset/front/jquery/"?>docs/js/jquery-1.12.0.min.js"></script>
	<script src="<?php echo base_url()."asset/front/jquery/"?>docs/js/jquery-ui.min.js"></script>
	<script src="<?php echo base_url()."asset/front/jquery/"?>docs/js/jquery-migrate-1.3.0.min.js"></script>


<!-- keyboard widget css & script (required) -->
<link href="<?php echo base_url()."asset/front/jquery/"?>css/keyboard.css" rel="stylesheet">
<script src="<?php echo base_url()."asset/front/jquery/"?>js/jquery.keyboard.js"></script>

<!-- keyboard extensions (optional) -->
<script src="<?php echo base_url()."asset/front/jquery/"?>js/jquery.mousewheel.js"></script>
<script src="<?php echo base_url()."asset/front/jquery/"?>js/jquery.keyboard.extension-typing.js"></script>
<script src="<?php echo base_url()."asset/front/jquery/"?>js/jquery.keyboard.extension-autocomplete.js"></script>
<script src="<?php echo base_url()."asset/front/jquery/"?>js/jquery.keyboard.extension-caret.js"></script>

<script>
 $(window).load(function(){
  $().UItoTop({ easingType: 'easeOutQuart' });
  $('#stuck_container').tmStickUp({});  
 }); 
</script>

<style> 
.ani{
   
    position: relative;
    -webkit-animation: mymove 1s; /* Chrome, Safari, Opera */
    animation: mymove 1s;
   
}
.colorchanger{  
    -webkit-animation-name: example; /* Chrome, Safari, Opera */
    -webkit-animation-duration: 4s; /* Chrome, Safari, Opera */
    animation-name: example;
    animation-duration: 4s;
}
#div1 {-webkit-animation-timing-function: linear;}
#div3 {-webkit-animation-timing-function: ease-in;}
#div4 {-webkit-animation-timing-function: ease-out;}
/* Standard syntax */
#div1 {animation-timing-function: linear;}
#div3 {animation-timing-function: ease-in;}
#div4 {animation-timing-function: ease-out;}

/* color changer Chrome, Safari, Opera */
@-webkit-keyframes example {
    from {background-color: red;}
    to {background-color: yellow;}
}/* Standard syntax */
@keyframes example {
    from {background-color: red;}
    to {background-color: yellow;}
}

/* Chrome, Safari, Opera */
@-webkit-keyframes mymove {
    from {left: -300px;}
    to {left: 0px;}
}
/* Standard syntax */
@keyframes mymove {
    from {left: -300px;}
    to {left: 0px;}
}

</style>
<!--[if lt IE 8]>
 <div style=' clear: both; text-align:center; position: relative;'>
   <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home?ocid=ie6_countdown_bannercode">
     <img src="http://storage.ie6countdown.com/assets/100/images/banners/warning_bar_0000_us.jpg" border="0" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today." />
   </a>
</div>
<![endif]-->
<!--[if lt IE 9]>
<script src="js/html5shiv.js"></script>
<link rel="stylesheet" media="screen" href="css/ie.css">
<![endif]-->
</head>
<body class="page1" id="top">
<!--==============================
              header
=================================-->
<header>
  <div class="container">
    <div class="row">
      <div class="grid_12 rel">
          <h1>
              <div class="ani" id="div1">
            <a href="<?php echo base_url().'index.php/Frontpage/';?>">
                <img src="<?php echo base_url()."asset/front/"?>images/kotakendari.png" alt="Logo alt" width="300" height="80">
            </a></div>
        </h1>
      </div>
    </div>
  </div>
<section id="stuck_container">