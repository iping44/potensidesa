<script>
    $(document).ready(function(){
    $('#txtKeyboard').keyboard();
    $('#txtNum') .keyboard({
     layout : 'num',
     restrictInput : true, 
     preventPaste : true,  
     autoAccept : true
    }).addTyping();
});
</script>

<section id="content"><div class="ic">More Website Templates @ TemplateMonster.com - July 28, 2014!</div>
    

    <div id="section-3">
    <div class="container">
      <div class="row">
        <div class="ani" id="div3">
        <div class="grid_12" >
            <h3>REPORT PERIZINAN</h3><p></p>
            <form method="POST" action="reportdata#section-3">
                <input name="key" type="text" class="form-control" id="txtKeyboard">
                <input type="submit" value="Search" class="btn1">
            </form>
            
            <?php foreach ($pembayaran as $v) 
            { ?>
            <table class="table table-condensed table-striped table-hover table-bordered pull-left" style="border-width: 0px;">
                <tr>
                    <th>NUP</th>
                    <td>:</td>
                    <td><?php echo $v->nup; ?></td>
                </tr>
                <tr>
                    <th>Indentitas Pemohon</th>
                    <td>:</td>
                    <td><?php echo $v->no_identitas." <br> ".$v->nama_pemohon?></td>
                </tr>
                <tr>
                    <th>Indentitas Perusahaan</th>
                    <td>:</td>
                    <td><?php echo $v->npwp_perusahaan." <br> ".$v->nama_perusahaan?></td>
                </tr>
                <tr>
                    <th>Tanggal Masuk</th>
                    <td>:</td>
                    <td><?php echo $v->tanggal_masuk." / ".$v->jam_masuk."</i>" ?></td>
                </tr>
                <tr>
                    <th>Penerima / Pemroses</th>
                    <td>:</td>
                    <td><?php echo $v->frontoffice." / ".$v->backoffice?></td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td>:</td>
                    <td>SELESAI</td>
                </tr>
                                    
            </table>
            <?php }?>
            
          
           
        </div>
        </div>
      </div>
    </div>
    </div>
  
    <div class="clear"></div>
</section>