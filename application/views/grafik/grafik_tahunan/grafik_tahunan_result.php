<script type="text/javascript" src="<?php echo base_url('/asset/highcharts/exporting.js'); ?>"></script>
<script type="text/javascript">

$(function () {
    $('#container').highcharts({
        /*chart: {
            type: 'column'
        },*/
		chart: {
            type: 'area',
            spacingBottom: 30
        },
        title: {
            text: 
			<?php if ($tempat2->nama_pasar=="")
			{
			?>
			'Harga Rata-rata <?php echo $jenis_komoditi2->nama_komoditi?> di <b> Kabupaten <?php echo $tempat2->nama_kabupaten?> </b>Pertahun'
			<?php
			} else if($tempat2->nama_kabupaten=="")
			{
			?>
			'Harga Rata-rata <?php echo $jenis_komoditi2->nama_komoditi?> di <b> <?php echo $tempat2->nama_pasar?> </b> Pertahun'
			<?php
			} else
			{
			?>
			''
			<?php
			}
			?>
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [
                <?php 
					foreach($grafik as $v){
					?>
					<?php 
					echo  "'".$v->tahun."'";
					?>
					,
					<?php
					} 
					?>
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rupiah (Rp)'
            }
        },
      tooltip: {
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>Rp. {point.y:.0f} </b></td></tr>',
           
        },
		legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '<?php echo $jenis_komoditi2->nama_komoditi?>',
            data: [
			<?php 
					foreach($grafik as $v){
					?>
					<?php	echo $v->harga;
					?>
					,
					<?php
					} 
					?>
			]

        }]
    });
}); </script>
 
<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Grafik Tahunan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                     <?php echo form_open("grafik/tahunan_result", 
                            "class='form-horizontal' row-border")?> 
					 <div class="form-group">
                        <label class="col-md-2 control-label">Dari tahun :</label>
                        <div class="col-md-2">
                          <select class="form-control selectpicker" data-live-search="true" name="tahun1" required>
										<option value="">- Tahun -</option>
										<?php
										$thn = date("Y")+ 2;
											for($i=2016;$i<=$thn;$i++){
											  echo "<option value='".$i."'>".$i."</option>";
											}
										?>
						  </select>
                        </div>
						
						
						<label class="col-md-2 control-label">Sampai Tahun :</label>
						<div class="col-md-2">
                           <select class="form-control selectpicker" data-live-search="true" name="tahun2" required>
										<option value="">- Tahun -</option>
										<?php
										$thn = date("Y")+ 2;
											for($i=2016;$i<=$thn;$i++){
											  echo "<option value='".$i."'>".$i."</option>";
											}
										?>
						  </select>
                        </div>
					  </div>
					  
				
				<div class="form-group">
					<label class="control-label col-md-2">Cari Berdasarkan :</label>
					<div class="col-md-2">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" value="kota" name="cakupan" id="cek" checked="true">Kabupaten
							</label>
							<label class="btn btn-default active">
								<input type="radio" value="pasar" name="cakupan" id="cek">Pasar
							</label>
						</div>

					</div>
				</div>
				
				<div class="form-group hidden" id="kota">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" >
								
                              <option value="">
									- Kabupaten -
									</option>
                                <?php
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten'>
									$cb->nama_kabupaten</option>";
									}
								
                                ?>
                         </select>	
				</div>
					
				</div>
				
				
				
				<div class="form-group" id="pasar">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="id_pasar" >
								
                               <option value="">
									- Pasar -
								</option>
                                <?php
								foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar'>
                                  $cb->nama_pasar</option>";
                                }
                                ?>
                         </select>					
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-2">Jenis Komoditi</label>
					<div class="col-md-2">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" value="pokok" name="cakupan" id="cek2" checked="true">Pokok
							</label>
							<label class="btn btn-default active">
								<input type="radio" value="strategis" name="cakupan" id="cek2">Strategis
							</label>
						</div>

					</div>
				</div>
				
				<div class="form-group hidden" id="pokok">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="bahan_pokok" >
						<option value="">- Bahan Pokok -</option>
                                <?php
									foreach ($pokok as $cb) {
                                    echo "
									<option value='$cb->id_komoditi'>
									$cb->nama_komoditi</option>";
									}
                                ?>
                         </select>	
				</div>
				</div>
				
				
				
				<div class="form-group" id="strategis">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="bahan_strategis" >
						<option value="">- Bahan Strategis -</option>
                                <?php
									foreach ($strategis as $cb) {
                                    echo "
									<option value='$cb->id_komoditi'>
									$cb->nama_komoditi</option>";
									}
                                ?>
                         </select>			
					</div>
					
				</div>
					
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary" >Cetak</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
				  
					<div class="span10">
						<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br><br><br>
					</div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   

<script type="text/javascript">
		$('label > #cek').change(function(){ if( $(this).is(":checked") ) 
			{ 
				$('#pasar').toggleClass('hidden'); 
				$('#kota').toggleClass('hidden'); 
			}; 
		}); 
		$('label > #cek2').change(function(){ if( $(this).is(":checked") ) 
			{ 
				$('#pokok').toggleClass('hidden'); 
				$('#strategis').toggleClass('hidden'); 
			}; 
		}); 
</script>
