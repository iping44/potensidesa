<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Grafik Mingguan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("grafik/mingguan_result", 
                            "class='form-horizontal' row-border")?> 
					 <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal :</label>
                        <div class="col-md-2">
							<div class='input-group date' >
								<input class="form-control" type="text"  id="weekpicker" name="tanggal" placeholder="Tanggal" maxlength="100" required>
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					  </div>
					  
				
				<div class="form-group">
					<label class="control-label col-md-2">Cari Berdasarkan :</label>
					<div class="col-md-2">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" value="kota" name="cakupan" id="cek" checked="true">Kabupaten
							</label>
							<label class="btn btn-default active">
								<input type="radio" value="pasar" name="cakupan" id="cek">Pasar
							</label>
						</div>

					</div>
				</div>
				
				<div class="form-group hidden" id="kota">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" >
								
                               <option value="">
									- Kabupaten -
									</option>
                                <?php
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten'>
									$cb->nama_kabupaten</option>";
									}
								
                                ?>
                         </select>	
				</div>
					
				</div>
				
				
				
				<div class="form-group" id="pasar">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="id_pasar" >
								
                                <option value="">
									- Pasar -
								</option>
                                <?php
								foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar'>
                                  $cb->nama_pasar</option>";
                                }
                                ?>
                         </select>					
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-2">Jenis Komoditi</label>
					<div class="col-md-2">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" value="pokok" name="cakupan" id="cek2" checked="true">Pokok
							</label>
							<label class="btn btn-default active">
								<input type="radio" value="strategis" name="cakupan" id="cek2">Strategis
							</label>
						</div>

					</div>
				</div>
				
				<div class="form-group hidden" id="pokok">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="bahan_pokok" >
						<option value="">- Bahan Pokok -</option>
                                <?php
									foreach ($pokok as $cb) {
                                    echo "
									<option value='$cb->id_komoditi'>
									$cb->nama_komoditi</option>";
									}
                                ?>
                         </select>	
				</div>
				</div>
				
				
				
				<div class="form-group" id="strategis">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="bahan_strategis" >
						<option value="">- Bahan Strategis -</option>
                                <?php
									foreach ($strategis as $cb) {
                                    echo "
									<option value='$cb->id_komoditi'>
									$cb->nama_komoditi</option>";
									}
                                ?>
                         </select>			
					</div>
					
				</div>
					
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary" >Cetak</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   

<script type="text/javascript">
		$('label > #cek').change(function(){ if( $(this).is(":checked") ) 
			{ 
				$('#pasar').toggleClass('hidden'); 
				$('#kota').toggleClass('hidden'); 
			}; 
		}); 
		$('label > #cek2').change(function(){ if( $(this).is(":checked") ) 
			{ 
				$('#pokok').toggleClass('hidden'); 
				$('#strategis').toggleClass('hidden'); 
			}; 
		}); 
</script>