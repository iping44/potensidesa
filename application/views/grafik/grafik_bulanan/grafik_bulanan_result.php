<script type="text/javascript" src="<?php echo base_url('/asset/highcharts/exporting.js'); ?>"></script>
<script type="text/javascript">

$(function () {
    $('#container').highcharts({
       /* chart: {
            type: 'line'
        },*/
		chart: {
            type: 'area',
            spacingBottom: 30
        },
        title: {
            text: 
			<?php if ($tempat2->nama_pasar=="")
			{
			?>
			'Harga Rata-rata <?php echo $jenis_komoditi2->nama_komoditi?> di <b> Kabupaten <?php echo $tempat2->nama_kabupaten?> </b>bulan <?php echo $bulan ?> Tahun <?php echo $tahun?>'
			<?php
			} else if($tempat2->nama_kabupaten=="")
			{
			?>
			'Harga <?php echo $jenis_komoditi2->nama_komoditi?> di <b> <?php echo $tempat2->nama_pasar?> </b>bulan <?php echo $bulan ?> Tahun <?php echo $tahun?>'
			<?php
			} else
			{
			?>
			''
			<?php
			}
			?>
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: <?php echo json_encode($grafik2); ?>,
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rupiah (Rp)'
            }
        },
       tooltip: {
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>Rp. {point.y:.0f} </b></td></tr>',
           
        },
       /*  plotOptions: {
            column: {
                pointPadding: 0.1,
                borderWidth: 1
            }
        },*/
		legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: '<?php echo $jenis_komoditi2->nama_komoditi?>',
            data: <?php echo json_encode($grafik); ?>

        }]
    });
}); </script>
 
<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Grafik Bulanan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                     <?php echo form_open("grafik/bulanan_result", 
                            "class='form-horizontal' row-border")?> 
					 <div class="form-group">
                        <label class="col-md-2 control-label">Bulan :</label>
                        <div class="col-md-2">
								<select class="form-control selectpicker" data-live-search="true" name="bulan" required>
								<option value="">- Bulan -</option>
								<option value="1">Januari</option>
								<option value="2">Februari</option>
								<option value="3">Maret</option>
								<option value="4">April</option>
								<option value="5">Mei</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">Agustus</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
								</select>
						</div>
						
						
						<label class="col-md-1 control-label">Tahun :</label>
						<div class="col-md-2">
                          <select class="form-control selectpicker" data-live-search="true" name="tahun" required>
										<option value="">- Tahun -</option>
										<?php
										$thn = date("Y")+ 2;
											for($i=2016;$i<=$thn;$i++){
											  echo "<option value='".$i."'>".$i."</option>";
											}
										?>
						  </select>
                        </div>
					  </div>
					  
				
				<div class="form-group">
					<label class="control-label col-md-2">Cari Berdasarkan :</label>
					<div class="col-md-2">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" value="kota" name="cakupan" id="cek" checked="true">Kabupaten
							</label>
							<label class="btn btn-default active">
								<input type="radio" value="pasar" name="cakupan" id="cek">Pasar
							</label>
						</div>

					</div>
				</div>
				
				<div class="form-group hidden" id="kota">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" >
								
                               <option value="">
									- Kabupaten -
									</option>
                                <?php
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten'>
									$cb->nama_kabupaten</option>";
									}
								
                                ?>
                         </select>	
				</div>
					
				</div>
				
				
				
				<div class="form-group" id="pasar">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="id_pasar" >
								
                              <option value="">
									- Pasar -
								</option>
                                <?php
								foreach ($pasar as $cb) {
                                    echo "<option value='$cb->id_pasar'>
                                  $cb->nama_pasar</option>";
                                }
                                ?>
                         </select>					
					</div>
				</div>
				
				<div class="form-group">
					<label class="control-label col-md-2">Jenis Komoditi</label>
					<div class="col-md-2">
						<div class="btn-group" data-toggle="buttons">
							<label class="btn btn-default">
								<input type="radio" value="pokok" name="cakupan" id="cek2" checked="true">Pokok
							</label>
							<label class="btn btn-default active">
								<input type="radio" value="strategis" name="cakupan" id="cek2">Strategis
							</label>
						</div>

					</div>
				</div>
				
				<div class="form-group hidden" id="pokok">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="bahan_pokok" >
						<option value="">- Bahan Pokok -</option>
                                <?php
									foreach ($pokok as $cb) {
                                    echo "
									<option value='$cb->id_komoditi'>
									$cb->nama_komoditi</option>";
									}
                                ?>
                         </select>	
				</div>
				</div>
				
				
				
				<div class="form-group" id="strategis">
					<label class="control-label col-md-3"></label>
					<div class="col-md-3">
						<select class="form-control selectpicker" data-live-search="true" name="bahan_strategis" >
						<option value="">- Bahan Strategis -</option>
                                <?php
									foreach ($strategis as $cb) {
                                    echo "
									<option value='$cb->id_komoditi'>
									$cb->nama_komoditi</option>";
									}
                                ?>
                         </select>			
					</div>
					
				</div>
					
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary" >Cetak</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
				  
					<div class="span10">
						<div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div><br><br><br>
					</div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   

<script type="text/javascript">
		$('label > #cek').change(function(){ if( $(this).is(":checked") ) 
			{ 
				$('#pasar').toggleClass('hidden'); 
				$('#kota').toggleClass('hidden'); 
			}; 
		}); 
		$('label > #cek2').change(function(){ if( $(this).is(":checked") ) 
			{ 
				$('#pokok').toggleClass('hidden'); 
				$('#strategis').toggleClass('hidden'); 
			}; 
		}); 
</script>
