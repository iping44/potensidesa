<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Berita & Artikel</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><span><i class="fa fa-long-arrow-right"></i>Berita & Artikel</span></div>
      </div>
    </div>
  </div>
</div>  
</section>


<!-- Blogs -->
<section id="blog" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-8 col-sm-7">
	  
		<?php foreach($info as $inf){ ?>
        <div class="blog_item padding-bottom">
           <h2><?php echo $inf->judul;?></h2>
           <ul class="comments">
             <li><a href="#"><?php echo $inf->tanggal;?></a></li>
             <li><a href="#"><i class="fa fa-wechat"></i>0</a></li>
			 <li><a href="#"><?php echo $inf->kategori;?></a></li>
           </ul>
          <div class="image_container">
          <img src="<?php echo base_url();?>files/image_struktur/<?php echo $inf->gambar;?>" height="100px" class="img-responsive" alt="blog post">
          </div>
          <p><?php $content = $inf->content;
			$content=character_limiter($content,600);?>
			<?php echo $content;?></p>
          <a class="btn-common button3" href="<?php echo base_url();?>home/info2/<?php echo $inf->id_berita?>" target="_blank">Selengkapnya</a>
        </div>
		<?php } ?>
        
      </div>
	  <div class="col-md-4 col-sm-5">
        <aside class="sidebar">
           <div class="widget">
             <h3>Kategori</h3>
             <ul class="widget_links">
               <li><a href="<?php echo base_url();?>home/kategori/1">Berita</a></li>
               <li><a href="<?php echo base_url();?>home/kategori/2">Artikel</a></li>
               <li><a href="<?php echo base_url();?>home/kategori/3">Promosi UMKM</a></li>
               <li><a href="<?php echo base_url();?>home/kategori/4">Informasi Penelitian</a></li>
               </ul>
           </div>
           </aside>
    </div>
    </div>
  </div>
</section>
