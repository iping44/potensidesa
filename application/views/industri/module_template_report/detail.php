<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Template</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("", 
                            "class='form-horizontal' row-border")?> 
                      <div class="form-group">
                        <label class="col-md-2 control-label">Judul Template</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="judul" value="<?php echo $entry->judul?>" disabled>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Desain</label>
                        <div class="col-md-10">
						  <textarea name="desain_template" class="textarea form-control ckeditor" disabled><?php echo $entry->desain_template?></textarea>
                          
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="keterangan" value="<?php echo $entry->keterangan?>" disabled>
                        </div>
                      </div>
					  
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <?php echo anchor('/template_report/data?id='.$entry->id_template, 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Surat Izin">
											Kembali</button>'); ?>
                        </div>
						
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   