<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="left-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Daftar Survey</a>
                    </div>
                  </div>
                  <div class="widget-body">
                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">
                        
                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>  
                       
                            <th style="width:20%">
                              Tanggal Survey
                            </th>
							<th style="width:20%">
                              Teknis 1
                            </th>
							<th style="width:20%">
                              Teknis 2
                            </th>
							<th style="width:20%">
                              Hasil Survey
                            </th>
							
                            
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = 1;
                            foreach ($survey as $v) {


                        ?>    
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>  
                           
                            <td>
                              <?php echo $v->tanggal_survey ?>
                            </td>
							 <td>
                              <?php echo $v->tim1_nama ?>
                            </td>
							 <td>
                              <?php echo $v->tim2_nama ?>
                            </td>
							<td>
                              <?php echo $v->status ?>
                            </td>
							 
                            
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   