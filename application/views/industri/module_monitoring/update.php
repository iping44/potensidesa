<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
			<!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      Monitoring Permohonan
                    </div>
                  </div>
				<div class='form-horizontal row-border'>  
				  
				<input type="hidden" name="nup" value="<?php echo $entry->nup?>">		
				<input type="hidden" name="npwp_perusahaan1" value="<?php echo $entry->npwp_perusahaan?>">		
				<input type="hidden" name="npwp_pemohon1" value="<?php echo $entry->npwp_pemohon?>">		
				<br>				
				 <div class="form-group">
                        <label class="col-md-3 control-label">Jenis Izin</label>
                        <div class="col-md-8">
                          <input class="form-control" type="text" name="nama_izin" value="<?php echo $entry->nama_izin ?>" readonly>
                        </div>
                  </div>
				  <div class="form-group">
                        <label class="col-md-3 control-label">Nama Instansi</label>
                        <div class="col-md-8">
                          <input class="form-control" type="text" name="nama_instansi" value="<?php echo $entry->nama_instansi ?>" readonly>
                        </div>
                  </div>
				  <div class="form-group">
                        <label class="col-md-3 control-label">Tanggal / Jam Permohonan Masuk</label>
                        <div class="col-md-4">
                          <input class="form-control" type="text" name="tanggal" 
							value="<?php echo $entry->tanggal_masuk." / ".$entry->jam_masuk ?>" readonly>
                        </div>
                  </div>
				  <div class="form-group">
                        <label class="col-md-3 control-label">Petugas Frontoffice</label>
                        <div class="col-md-4">
                          <input class="form-control" type="text" name="userid" value="<?php echo $entry->username ?>" readonly>
                        </div>
                  </div>
                  <div class="widget-body clearfix">
                    <ul id="myTab" class="nav nav-tabs">
						<?php
						if($entry->kategori=="Izin Usaha") {
							echo "
								<input type='hidden' name='kategori' value='Izin Usaha'>
								<li class='active'><a href='#pemohon' data-toggle='tab'>Pemohon</a></li>
								<li ><a href='#perusahaan' data-toggle='tab'>Perusahaan</a></li>
								<li><a href='#detail' data-toggle='tab'>Detail Usaha</a></li>
								<li><a href='#review' data-toggle='tab'>Review Kabid</a></li>
								<li><a href='#disposisi' data-toggle='tab'>Disposisi</a></li>
								<li><a href='#survey' data-toggle='tab'>Survey</a></li>
								<li><a href='#retribusi' data-toggle='tab'>Retribusi</a></li>
								<li><a href='#draft' data-toggle='tab'>Draft Surat</a></li>
							";
							
						} else if($entry->kategori=="Izin Non Usaha") {
							echo "
								<li class='active'><a href='#pemohon' data-toggle='tab'>Pemohon</a></li>
								<li><a href='#review' data-toggle='tab'>Review Kabid</a></li>
								<li><a href='#draft' data-toggle='tab'>Draft Surat</a></li>
								<li><a href='#disposisi' data-toggle='tab'>Disposisi</a></li>
								<li><a href='#survey' data-toggle='tab'>Survey</a></li>
								<li><a href='#retribusi' data-toggle='tab'>Retribusi</a></li>
								<input type='hidden' name='kategori' value='Izin Non Usaha'>
							";
						} else if($entry->kategori=="Penanaman Modal") {
							echo "
								<input type='hidden' name='kategori' value='Penanaman Modal'>
								<li class='active'><a href='#pemohon' data-toggle='tab'>Pemohon</a></li>
								<li ><a href='#perusahaan' data-toggle='tab'>Perusahaan</a></li>
								<li><a href='#detail' data-toggle='tab'>Detail Usaha</a></li>
								<li><a href='#review' data-toggle='tab'>Review Kabid</a></li>
								<li><a href='#disposisi' data-toggle='tab'>Disposisi</a></li>
								<li><a href='#survey' data-toggle='tab'>Survey</a></li>
								<li><a href='#retribusi' data-toggle='tab'>Retribusi</a></li>
								<li><a href='#draft' data-toggle='tab'>Draft Surat</a></li>
							";
						}
					?> 
					
					  
                    </ul>
                    <div id="myTabContent" class="tab-content">
                      <div class="tab-pane fade" id="perusahaan">
                        <?php include "perusahaan_surat.php"?>
                      </div>
                      <div class="tab-pane fade in active" id="pemohon">
                        <?php include "pemohon_surat.php"?>
                      </div><div class="tab-pane fade" id="detail">
                        <?php include "detailusaha_surat.php"?>
                      </div>
					  <div class="tab-pane fade" id="review">
                        <?php include "daftar_review.php"?>
                      </div>
					  <div class="tab-pane fade" id="disposisi">
                        <?php include "daftar_disposisi.php"?>
                      </div>
					  <div class="tab-pane fade" id="survey">
                        <?php include "survey_update.php"?>
                      </div>
					  <div class="tab-pane fade" id="retribusi">
                        <?php include "retribusi_update.php"?>
                      </div>
					  <div class="tab-pane fade" id="draft">
                        <?php include "surat_draft.php"?>
                      </div>

                    </div>
					<br>
                    
                  </div>
				 </div>  
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   