<?php
require_once('./asset/function/fungsi_indotgl.php');
ob_start();
foreach($front as $dp){
    
     $awal=$dp->desain_surat;
   
     $nama_perusahaan=$dp->nama_perusahaan;
     $str=  str_replace("perusahaan.nama",$nama_perusahaan,$awal);
     
     $alamat = $dp->alamat_perusahaan;
     $str=  str_replace("perusahaan.alamat",$alamat, $str);
     
     $telepon = $dp->telepon_perusahaan;
     $str=  str_replace("perusahaan.telp",$telepon, $str);
     
     $fax = $dp->fax_pemohon;
     $str=  str_replace("perusahaan.fax",$fax, $str);
     
     $email = $dp->email_perusahaan;
     $str=  str_replace("perusahaan.email",$email, $str);
     
     $npwp = $dp->npwp_perusahaan;
     $str=  str_replace("perusahaan.npwp",$npwp, $str);
     
     $akta = $dp->nomor_akta;
     $str=  str_replace("perusahaan.akta",$akta, $str);
     
     $direktur = $dp->nama_direktur;
     $str=  str_replace("perusahaan.direktur",$direktur, $str);
     
     
     $nama_surat = $dp->nama_izin;
     $str=  str_replace("nama.surat",$nama_surat, $str);
     
	 $nama_pemohon = $dp->nama_pemohon;
	 $str= str_replace("pemohon.nama",$nama_pemohon,$str);
	 
	 $alamat_pemohon= $dp->alamat_pemohon;
	 $str= str_replace("pemohon.alamat",$alamat_pemohon,$str);
	 
	 $tanggal_sekarang= tgl_indo(date("Y-m-d"));
	 $str= str_replace("tanggal.sekarang",$tanggal_sekarang,$str);
	 
	 $identitas_pemohon= $dp->no_identitas;
	 $str= str_replace("pemohon.identitas",$identitas_pemohon,$str);
	 
	 $bidang_usaha= $dp->bidang_usaha;
	 $str= str_replace("usaha.bidang", $bidang_usaha, $str);
     
}
echo $str;
$content = ob_get_contents();
ob_clean();

try
{
   $html2pdf = new HTML2PDF('P', 'LEGAL', 'en');
   $html2pdf->pdf->SetDisplayMode('fullpage');
   $html2pdf->writeHTML($content);
   $html2pdf->Output('Surat-'.$dp->kode_suratizin.'.pdf');
}
catch(HTML2PDF_exception $e) {
   echo $e;
   exit;
}
?>
