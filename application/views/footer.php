<!--Footer-->
<footer class="padding-top dark">
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Badan Penelitian & Pengembangan Provinsi Sulawesi Tenggara?</h4>
        <hr class="half_space">
        <p class="half_space"> </p>
        
        
      </div>
      <div class="col-md-3  col-sm-6 footer_column">
        <h4 class="heading">Pintasan</h4>
        <hr class="half_space">
        <ul class="widget_links">
          <li><a href="<?php echo base_url();?>home/beranda">Beranda</a></li>
          <li><a href="<?php echo base_url();?>home/tentang">Tentang Kami</a></li>
          <li><a href="<?php echo base_url();?>home/layanan">Layanan</a></li>
          <li><a href="<?php echo base_url();?>home/faq">FAQ's</a></li>
          <li><a href="<?php echo base_url();?>home/galeri">Galeri</a></li>
                    <li><a href="<?php echo base_url();?>home/hubungi">Hubungi Kami</a></li>
          
        </ul>
      </div>
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Paket Berita</h4>
        <hr class="half_space">
        <p class="icon"><i class="fa fa-envelope-o"></i>Sign up with your name and email to get updates fresh updates.</p>
        <div id="result1" class="text-center"></div>        
        
       <form action="http://themesindustry.us13.list-manage.com/subscribe/post-json?u=4d80221ea53f3a4487ddebd93&id=494727d648&c=?" method="get" onSubmit="return false" class="newsletter">
          <div class="form-group">
            <input type="email" placeholder="E-mail Address" required name="EMAIL" id="EMAIL" class="form-control" />
          </div>
          <div class="form-group">
            <input type="submit" class="btn-submit button3" value="Subscribe" />
          </div>
        </form>
      </div>
      <div class="col-md-3 col-sm-6 footer_column">
        <h4 class="heading">Hubungi Kami</h4>
        <hr class="half_space">
        <p>Saran dan kritik. Untuk lebih lanjut silahkan ke alamat dibawah ini :</p>
        <p class="address"><i class="fa fa-map-marker"></i>Kompleks Bumi Praja Anduonouhu</p>
        <p class="address"><i class="fa fa-phone"></i>(0401) 3008846</p>
        <p class="address"><i class="fa fa-envelope"></i><a href="yahoo.com">badanlitbang.sultra01@gmail.com</a></p>
      </div> 
    </div>
    <div class="row">
     <div class="col-md-12">
        <div class="copyright clearfix">
          <p>Copyright &copy; 2017 Balitbang Provinsi Sulawesi Tenggara. All Right Reserved</p>
          <ul class="social_icon">
            <li><a href="#" class="facebook"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#" class="google"><i class="fa fa-instagram"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</footer>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>

<script src="<?php echo base_url();?>js/jquery-2.2.3.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.geolocation.edit.min.js"></script>
<script src="<?php echo base_url();?>js/bootstrap-datetimepicker.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.themepunch.revolution.min.js"></script>
<script src="<?php echo base_url();?>js/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url();?>js/revolution.extension.navigation.min.js"></script>
<script src="<?php echo base_url();?>js/revolution.extension.parallax.min.js"></script>
<script src="<?php echo base_url();?>js/revolution.extension.slideanims.min.js"></script>
<script src="<?php echo base_url();?>js/revolution.extension.video.min.js"></script>
<script src="<?php echo base_url();?>js/slider.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>js/jquery.parallax-1.1.3.js"></script>
<script src="<?php echo base_url();?>js/parallax.js"></script>
<script src="<?php echo base_url();?>js/jquery.mixitup.min.js"></script>
<script src="<?php echo base_url();?>js/jquery.fancybox.js"></script>
<script src="<?php echo base_url();?>js/functions.js" type="text/javascript"></script>
<script src="<?php echo base_url()."assets/"?>js/jquery.min.js"></script>

</body>
</html>