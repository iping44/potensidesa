<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">

  <!-- Left Sidebar Start -->
  <div class="left-sidebar">
      <!-- Row Start -->
        <div class="row">
          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="widget">
              <div class="widget-header">
                <div class="title">
                  Panel basic example
                </div>
              </div>
              <div class="widget-body">
                <div class="panel panel-default no-margin">
                  <div class="panel-body">
                    Basic panel example
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Row End -->
      
  </div>

</div>