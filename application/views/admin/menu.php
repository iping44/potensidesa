<div class="dashboard-container">

      <div class="container">
        <!-- Top Nav Start -->
        <div id='cssmenu'>
          <ul>
            <li class='<?php //echo $state?>'>
                <?php echo anchor('/home/', "<i class='fa fa-dashboard'></i> Dashboard"); ?>
            </li>

			<?php
				foreach($menu as $m){
					echo "
						<li class=''>";
						echo anchor($m->link, $m->attribute);
					$x = 0;
					foreach($submenu as $s) {

						if($s->menu_id==$m->menu_id) {

							if($x==0) {
								echo "<ul>";
							}
							$link = $s->link;
							$name = $s->submenu_name;
							echo "<li>";
							echo anchor("$link", "$name");
							echo "</li>";
							$x++;
						}

					}
					if($x!=0) {
						echo "</ul>";
					}
					echo "</li>";
				}
			?>

          </ul>
        </div>
        <!-- Top Nav End -->

        <!-- Sub Nav End -->
        <div class="sub-nav hidden-sm hidden-xs">
          <ul>
              <li><a href="<?php echo base_url();?>" class="heading">POTENSI DESA KAB. KOLAKA TIMUR</a></li>
          </ul>
        </div>
        <!-- Sub Nav End -->
