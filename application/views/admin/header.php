<!DOCTYPE html>
<html>
<head>
    <title>Potensi Desa Kolaka Timur</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pring - Content Management System" />
    <meta name="keywords" content="Notifications, Admin, Dashboard, Bootstrap3, Sass, transform,
				CSS3, HTML5, Web design, UI Design, Responsive Dashboard,
						Responsive Admin, Admin Theme, Best Admin UI, Bootstrap Theme,
								Wrapbootstrap, Bootstrap, bootstrap.gallery" />
    <meta name="author" content="Bootstrap Gallery" />
    <!--<link rel="shortcut icon" href="<?php echo base_url()."asset/"?>img/favicon.ico">-->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

    <link rel="shortcut icon" href="<?php echo base_url();?>images/logo.png">

    <link href="<?php echo base_url();?>css/bootstrap.min.css" rel="stylesheet">

    <link href="<?php echo base_url();?>css/new.css" rel="stylesheet">

	<link href="<?php echo base_url();?>css/datepicker3.css" rel="stylesheet">

	<link href="<?php echo base_url();?>css/datatables-bootstrap3.css" rel="stylesheet">

	<link href="<?php echo base_url();?>css/alertify.core.css" rel="stylesheet" id="toggleCSS">

    <link href="<?php echo base_url();?>fonts/font-awesome.min.css" rel="stylesheet">


	<script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','../../www.google-analytics.com/analytics.js','ga');
      ga('create', 'UA-40304444-1', 'iamsrinu.com');
      ga('send', 'pageview');

    </script>

	<!-- CKEDITOR -->
	<script type="text/javascript" src="<?php echo base_url();?>asset/ckeditor/ckeditor.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/ckfinder/ckfinder.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>asset/ckeditor/styles.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/ckeditor/md5.js"></script>



	<script>

            function validateUser() {
                var pass = document.forms["form_user_profil"]["oldpassword"].value;
                var chckpass = document.forms["form_user_profil"]["checkoldpassword"].value;
                var newpass = document.forms["form_user_profil"]["password"].value;
				var renewpass = document.forms["form_user_profil"]["matchpassword"].value;

				var chckpassMd5 = calcMD5(chckpass);
                    if (chckpass != "" && chckpassMd5 != pass) {
                        alert("Password Lama Salah");
                        return false;
                    }
					else if (chckpass != "" && newpass != "" && newpass != renewpass) {
                        alert("Password Baru Tidak Cocok");
                        return false;
                    }
					else if (chckpass != "" && renewpass != "" && renewpass != newpass) {
                        alert("Password Baru Tidak Cocok");
                        return false;
                    }
					else if (chckpass != "" && newpass == "") {
                        alert("Password Baru Harus Terisi");
                        return false;
                    }
					else if (chckpass != "" && renewpass == "") {
                        alert("Password Lagi Harus Terisi");
                        return false;
                    }
                    else {
                        return true;
                    }
            }

			function formatRupiah(objek, separator) {
                  a = objek.value;
                  b = a.replace(/[^\d]/g,"");
                  c = "";
                  panjang = b.length;
                  j = 0;
                  for (i = panjang; i > 0; i--) {
                    j = j + 1;
                    if (((j % 3) == 1) && (j != 1)) {
                      c = b.substr(i-1,1) + separator + c;
                    } else {
                      c = b.substr(i-1,1) + c;
                    }
                  }
                  objek.value = c;
            }

        </script>

    <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/bootstrap-select.css"/>
	<!--script src="<?php echo base_url()."asset/"?>js/jquery-2.1.4.min.js"></script-->
	<script src="<?php echo base_url();?>js/dataTables/jquery.min.js"></script>
	<script src="<?php echo base_url();?>js/bootstrap-select.js"></script>

	<!-- Datatables JS -->
	<script src="<?php echo base_url();?>js/dataTables/jquery.dataTables.js"></script>
	<script src="<?php echo base_url();?>js/dataTables/datatables-bootstrap3.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#example-table').dataTable({
				"pagingType": "full_numbers",
				"bSort": false
			});
		});
	</script>

	<!--script type="text/javascript" src="<?php echo base_url()."asset/"?>js/jcombo/sh/shCore.js"></script>
	<script type="text/javascript" src="<?php echo base_url()."asset/"?>js/jcombo/sh/shBrushJScript.js"></script>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url()."asset/"?>js/jcombo/sh/shCore.css"/>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url()."asset/"?>js/jcombo/sh/shThemeDefault.css"/>
	<script src="<?php echo base_url()."asset/"?>js/jquery-2.1.4.min.js"></script>
	<script src="<?php echo base_url()."asset/"?>js/jcombo/jquery.searchabledropdown-1.0.8.min.js"></script>
	<script type="text/javascript">
		SyntaxHighlighter.all();
	</script>
	<!-- END syntax highlighter -->
	<!--script type="text/javascript">
		$(document).ready(function() {
			$("select").searchable();
		});
		// demo functions
		$(document).ready(function() {
			$("#value").html($("#myselect :selected").text() + " (VALUE: " + $("#myselect").val() + ")");
			$("select").change(function(){
				$("#value").html(this.options[this.selectedIndex].text + " (VALUE: " + this.value + ")");
			});
		});

		function modifySelect() {
			$("select").get(0).selectedIndex = 5;
		}

		function appendSelectOption(str) {
			$("select").append("<option value=\"" + str + "\">" + str + "</option>");
		}

		function applyOptions() {
			$("select").searchable({
				maxListSize: $("#maxListSize").val(),
				maxMultiMatch: $("#maxMultiMatch").val(),
				latency: $("#latency").val(),
				exactMatch: $("#exactMatch").get(0).checked,
				wildcards: $("#wildcards").get(0).checked,
				ignoreCase: $("#ignoreCase").get(0).checked
			});

			alert(
				"OPTIONS\n---------------------------\n" +
				"maxListSize: " + $("#maxListSize").val() + "\n" +
				"maxMultiMatch: " + $("#maxMultiMatch").val() + "\n" +
				"exactMatch: " + $("#exactMatch").get(0).checked + "\n"+
				"wildcards: " + $("#wildcards").get(0).checked + "\n" +
				"ignoreCase: " + $("#ignoreCase").get(0).checked + "\n" +
				"latency: " + $("#latency").val()
			);
		}
	</script-->
	<!-- Custom Chart -->

      <!--  <script type="text/javascript" src="<?php echo base_url('/asset/highcharts/modules/exporting.js'); ?>"></script>
        <script type="text/javascript" src="<?php echo base_url('/asset/highcharts/exporting.js'); ?>"></script>-->

		<!--grafik-->
		<script type="text/javascript" src="<?php echo base_url('/asset/highcharts/highchart.js'); ?>"></script>

  </head>

  <body>
  <input type="hidden" class="" id="alert">
