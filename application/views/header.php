<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<title>Website Balitbang SULTRA</title>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/medical-guide-icons.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/animate.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/settings.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/navigation.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/owl.transitions.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/jquery.fancybox.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/zerogrid.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/style.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/loader.css">
<link rel="shortcut icon" href="<?php echo base_url();?>images/kendari.png">
<link href='http://fonts.googleapis.com/css?family=Open+Sans&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>