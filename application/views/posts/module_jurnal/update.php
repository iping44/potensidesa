<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Jurnal</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open_multipart("jurnal/update_data", 
                            "class='form-horizontal' row-border")?> 
                      <input type="hidden" name="id_jurnal" value="<?php echo $entry->id_jurnal?>">
                      
					  <div class="form-group">
					  
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Tulisan</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="id_jenisjurnal">
								<?php
                                foreach ($jenisjurnal as $cb) {
									if($cb->id_jenisjurnal==$entry->id_jenisjurnal) {
										echo "<option value='$cb->id_jenisjurnal' selected>
										$cb->nama_jenisjurnal</option>";
									} else {
										echo "<option value='$cb->id_jenisjurnal'>
                                  $cb->nama_jenisjurnal</option>";
									}
                                     
                                }
                                ?>

                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Edisi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="edisi" 
							value="<?php echo $entry->edisi?>" maxlength="100">
                        </div>
					   </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Judul</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="judul" 
							value="<?php echo $entry->judul?>" maxlength="100">
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Penyusun</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="penyusun" 
							value="<?php echo $entry->penyusun?>" maxlength="100">
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Tahun</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="tahun" 
							value="<?php echo $entry->tahun?>" maxlength="100">
                        </div>
					   </div>
					   
					   <div class="form-group">
                        <label class="col-md-2 control-label">Kata Kunci</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="katakunci" 
							value="<?php echo $entry->kata_kunci?>">
                        </div>
					   </div>
					 <div class="form-group">
                        <label class="col-md-2 control-label">Abstrak</label>
                        <div class="col-sm-10">
                            <textarea name="abstrak" id="content" >
							<?php echo $entry->abstrak?>
							</textarea>
							<?php echo display_ckeditor($editor['ckeditor1']);?>
                        </div>
                      </div>
					  <div class="form-group">
					  <label class="col-md-2 control-label" >File</label>
					  <div class="col-md-6">
						  <a href="<?php echo base_url()."files/uploads/".$entry->download?>" target="_blank"><?php echo $entry->download?></a>
					  </div>
					</div>
					  <div class="form-group">
					  <label class="col-md-2 control-label" >Upload File</label>
					  <div class="col-md-6">
						  <input class="form-control" type="file" name="userfile">
					  </div>
					</div>
					   
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						              <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						                <?php echo anchor('/jurnal/data', 
									                           '<button type="button" class="btn btn-success 
										                          " data-toggle="tooltip" data-placement="top" title="Kembali Data Jurnal">
										                          	Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   