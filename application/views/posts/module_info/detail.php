<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Info Sehat</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("", 
                            "class='form-horizontal' row-border")?> 
                      
                      
					   <div class="form-group">
                        <label class="col-md-2 control-label">Judul</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="judul" 
							value="<?php echo $entry->judul?>" maxlength="100" readonly>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="tanggal" 
							value="<?php echo $entry->tanggal?>" maxlength="100" readonly>
                        </div>
					   </div>
					   <div class="form-group">
							  <label class="col-md-2 control-label" >gambar</label>
							  <div class="col-md-6">
								  <?php     
				  
								$image = array(
								  'src' => '/files/image_struktur/'.($entry->gambar),
								  'class' => 'photo',
								  'width' => '200',
								  'height' => '200',
								);

								echo img($image); ?>
							  </div>
							</div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Content</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="content" 
							value="<?php echo $entry->content?>" maxlength="100" readonly>
                        </div>
					   </div>
					   				   
					   
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <?php echo anchor('/info/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali ke Data struktur">
											Kembali</button>'); ?>
                        </div>
						
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   