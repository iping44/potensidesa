<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Jasakip</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open_multipart("jasakip/create_data", 
                            "class='form-horizontal' row-border")?> 
					       <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Jasakip</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="id_jenisjasakip">
								<?php
                                foreach ($jenisjasakip as $cb) {
                                    echo "<option value='$cb->id_jenisjasakip'>
                                  $cb->nama_jenisjasakip</option>"; 
                                }
                                ?>
								</select>
                                </div>
                            </div>    
                        </div>
                      </div>
                       <div class="form-group">
                        <label class="col-md-2 control-label">Judul</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="judul" 
							placeholder="judul" maxlength="100">
                        </div>
					   </div>
					   					  
					<div class="form-group">
					  <label class="col-md-2 control-label" >Upload File</label>
					  <div class="col-md-6">
						  <input class="form-control" type="file" name="userfile">
					  </div>
					</div>
					  
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/jasakip/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali ke Data jasakip">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   