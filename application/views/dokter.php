<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Dokter</h2>
         <div class="page_link">
         <a href="<?php echo base_url();?>home/beranda">Beranda</a><i class="fa fa-long-arrow-right"></i><a href="#">Halaman</a><i class="fa fa-long-arrow-right"></i><span>Dokter</span>
         </div>
      </div>
    </div>
  </div>
</div>  
</section>





<section id="gallery" class="padding-top">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="work-filter">
          <ul class="text-center">
             <li><a href="javascript:;" data-filter="all" class="active filter">Semua Dokter</a></li>
            
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-12">
      <div class="row">
      <div class="zerogrid">
        <div class="wrap-container">
          <div class="wrap-content clearfix">
		  
            <?php foreach($nama_dokter as $dok){ ?>
			<div class="col-1-3 mix work-item hod heading_space">
              <div class="wrap-col">
                <div class="item-container">
                  <div class="image">
                    <img src="<?php echo base_url();?>/files/image_struktur/<?php echo $dok->gambar;?>" width="300px" height="300px" alt="work"/>
                    <div class="overlay">
                      <div class="overlay-inner">
                        <ul class="social_icon">
                        <li><a href="<?php echo $dok->fb;?>" class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="<?php echo $dok->twitter;?>" class="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="<?php echo $dok->instagram;?>" class="google"><i class="fa fa-instagram"></i></a></li>
                        </ul>
                      </div>
                    </div>
                  </div> 
                  <div class="gallery_content text-left">
                    <h3><?php echo $dok->nama_dokter;?></h3>
                    <small><?php echo $dok->divisi;?></small>
                    <p><?php echo $dok->deskripsi;?></p>
                  </div>
                </div>
              </div>
            </div>         
			<?php } ?>
			
          </div>
        </div>
       </div>
      </div>
    </div>
  </div>
</section>
