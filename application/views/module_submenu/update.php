<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Subenu</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("submenu/update_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <input type="hidden" name="submenu_id" value="<?php echo $entry->submenu_id?>">
					  <input type="hidden" name="menu_id" value="<?php echo $entry->menu_id?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Submenu</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="submenu_name" 
                                 value="<?php echo $entry->submenu_name ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Attribute</label>
                        <div class="col-md-6">
						  <textarea name="attribute" class="textarea form-control"><?php echo $entry->attribute ?></textarea>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Link</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="link" 
                                 value="<?php echo $entry->link ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Aktif</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                    <select id="active" class="form-control" name="active">
                                <?php 
                                if($entry->active=="Y") {
                                    echo "
                                        <option value='Y' selected> Ya</option>
                                        <option value='T'>Tidak</option>"
                                    ;
                                } else if($entry->active=="T") {
                                    echo "
                                        <option value='Y' > Ya</option>
                                        <option value='T' selected>Tidak</option>"
                                    ;
                                }
                                ?>    
                                
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/submenu/data?id='.$entry->menu_id, 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Submenu">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   