<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Menu</a>
                    </div>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/menu/insert', 
									'<button type="button" class="btn btn-info 
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Menu">
											Tambah</button>'); ?>
									<?php echo anchor('/menu/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Refresh Data Menu">
											Refresh</button>'); ?>
								</div>
							</div>
							
							<?php echo form_open("menu/result");?>
							
							<div class="col-md-3 col-sm-4 col-xs-4">
								<input type="text" class="form-control" name="key" placeholder="Cari">
							</div>
							
							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data Menu">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
                    <div class="clearfix">
                    </div> 
                      
                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">
                        
                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>  
                            <th style="width:20%">
                              Nama Menu
                            </th>
                            <th style="width:20%">
                              Link
                            </th>
                            <th style="width:20%">
                              Aktif
                            </th>
							<th style="width:30%">
                              Keterangan
                            </th>
                            <th style="width:5%">
                              Pilihan
                            </th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($mmenu as $v) {


                        ?>    
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>  
                            <td>
                              <?php echo $v->menu_name ?>
                            </td>
                            <td>
                              <?php echo $v->link ?>
                            </td>
                            <td>
                              <?php if($v->active=='Y') {
                                  echo "Ya";
                              } else {
                                  echo "Tidak";
                              } ?>
                            </td>
							<td>
                              <?php echo $v->description ?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan 
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
								<li><?php echo anchor("/submenu/data?id=".$v->menu_id, "Tambah Submenu"); ?></li>
                                <li><?php echo anchor("/menu/detail?id=".$v->menu_id, "Detail"); ?></li>
                                <li><?php echo anchor("/menu/update?id=".$v->menu_id, "Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("/menu/delete?id=".$v->menu_id, "
                                    Hapus
                                ", "onclick=\"return confirm('Anda Yakin Akan Menghapus?')\"");
                                ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <div align="right"><?php echo $links?></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   