<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Menu</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("menu/create_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Menu</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="menu_name" placeholder="Nama Menu"
							 maxlength="100" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Attribute</label>
                        <div class="col-md-6">
						  <textarea name="attribute" class="textarea form-control"></textarea>
                          
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Link</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="link" placeholder="Link" 
							 maxlength="100" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-sm-2 control-label">Aktif</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="active">
                                <option>
                                  - Status -
                                </option>
                                <option value="Y">
                                  Ya
                                </option>
                                <option value="T">
                                  Tidak
                                </option>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Menu Admin</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="adminmenu">
                                <option>
                                  - Status -
                                </option>
                                <option value="Y">
                                  Ya
                                </option>
                                <option value="T">
                                  Tidak
                                </option>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Posisi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="position" placeholder="Posisi">
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
						  <textarea name="description" class="textarea form-control"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/menu/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Menu">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   