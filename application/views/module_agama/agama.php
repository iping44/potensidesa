<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data agama</a>
                    </div>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/agama/insert?id='.$id,
									'<button type="button" class="btn btn-info
										" data-toggle="tooltip" data-placement="top" title="Tambah Data agama">
											Tambah</button>'); ?>
									<?php echo anchor('/agama/data?id='.$id,
									'<button type="button" class="btn btn-success
										" data-toggle="tooltip" data-placement="top" title="Refresh Data agama">
											Refresh</button>'); ?>
									<?php echo anchor('/desa/data',
									'<button type="button" class="btn btn-warning
										" data-toggle="tooltip" data-placement="top" title="Kembali">
											Kembali</button>'); ?>
								</div>
							</div>

							<?php echo form_open("agama/result");?>

							<div class="col-md-3 col-sm-4 col-xs-4">
								<input type="hidden" name="agama_id" value="<?php echo $id?>">
								<input type="text" class="form-control" name="key" placeholder="Cari">
							</div>

							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data Submenu">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
                    <div class="clearfix">
                    </div>

                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">

                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>
                            <th style="width:10%">
                              Islam
                            </th>
                            <th style="width:10%">
                              Kristen
                            </th>
							              <th style="width:10%">
                              Katolik
                            </th>
                            <th style="width:10%">
                              Hindu
                            </th>
                            <th style="width:10%">
                              Budhha
                            </th>
                            <th style="width:10%">
                              Lain
                            </th>
							              <th style="width:16%">
                              Pilihan
                            </th>

                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            if ($agama){
                            foreach ($agama as $v) {
                        ?>
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>
                            <td>
                              <?php echo $v->islam ?>
                            </td>
                            <td>
                              <?php echo $v->kristen ?>
                            </td>
							              <td>
                              <?php echo $v->katolik ?>
                            </td>
							              <td>
                              <?php echo $v->hindu ?>
                            </td>
							              <td>
                              <?php echo $v->budha ?>
                            </td>
							              <td>
                              <?php echo $v->lain ?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
                                <li><?php echo anchor("/agama/detail?id1=".$v->desa_id."&id2=".$v->agama_id, "Detail"); ?></li>
                                <li><?php echo anchor("/agama/update?id1=".$v->desa_id."&id2=".$v->agama_id, "Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("/agama/delete?id1=".$v->desa_id."&id2=".$v->agama_id, "
                                    Hapus
                                ", "onclick=\"return confirm('Anda Yakin Akan Menghapus?')\"");
                                ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          }else{
                            
                          }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					             <?php echo $links?>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
