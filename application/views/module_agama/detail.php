<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Agama</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("",
                            "class='form-horizontal' row-border")?>

                      <input type="hidden" name="agama_id" value="<?php echo $entry->agama_id?>">
                      <div class="form-group">
                        <!-- <label class="col-sm-2 control-label">Id agama</label> -->
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="hidden" name="agama_id" value="<?php echo $entry->agama_id?>" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Islam</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="islam" value="<?php echo $entry->islam?>" maxlength="100" disabled>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Kristen</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="kristen" value="<?php echo $entry->kristen?>" maxlength="4" disabled>
                                </div>
                            </div>
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Katolik</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="katolik" value="<?php echo $entry->katolik?>" maxlength="4" disabled>
                                </div>
                            </div>
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Hindu</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="hindu" value="<?php echo $entry->hindu?>" maxlength="4" disabled>
                                </div>
                            </div>
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Budha</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="budha" value="<?php echo $entry->budha?>" maxlength="4" disabled>
                                </div>
                            </div>
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Lain</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="lain" value="<?php echo $entry->lain?>" maxlength="4" disabled>
                                </div>
                            </div>
                        </div>
                      </div>


                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
            						  <?php echo anchor('/agama/data?id='.$entry->desa_id,
            									'<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Kembali Data Group">Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
