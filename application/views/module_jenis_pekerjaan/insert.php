<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Data Jenis Pekerjaan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("jenis_pekerjaan/create_data",
                            "class='form-horizontal' row-border")?>
                      <input type="hidden" name="desa_id" value="<?php echo $id?>">
                      <div class="form-group">
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="hidden" name="jenis_pekerjaan_id" placeholder="Jenis Pekerjaan" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Pekerjaan</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="jenis_pekerjaan" placeholder="Jenis Pekerjaan" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
            						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
            						  <?php echo anchor('/jenis_pekerjaan/data?id='.$id,
            									'<button type="button" class="btn btn-success
            										" data-toggle="tooltip" data-placement="top" title="Kembali Data Geografi Desa">
            											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
