<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Tanda Tangan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("tanda_tangan/create_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama" 
							placeholder="Nama" maxlength="100" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">NIP</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nip" 
							placeholder="NIP" maxlength="100" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Golongan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="golongan" 
							placeholder="Golongan" maxlength="100" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Jabatan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="jabatan" 
							placeholder="Jabatan" maxlength="100" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-sm-2 control-label">Kabupaten/Kota</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="id_kabupaten" required>
                                <option>
                                  - Kabupaten/Kota -
                                </option>
                                <?php
                                foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten'>
                                  $cb->nama_kabupaten</option>"; 
                                }
                                ?>
                              </select>
                                </div>
                            </div>    
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm" title="Simpan Data">Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/tanda_tangan/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data User">
											Kembali</button>'); ?>
                        </div>
						
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   