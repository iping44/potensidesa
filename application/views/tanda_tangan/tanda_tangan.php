<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Tanda Tangan</a>
                    </div>
                  </div>
                  <div class="widget-body">
					<div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/tanda_tangan/insert', 
									'<button type="button" class="btn btn-info 
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Tanda Tangan">
											Tambah</button>'); ?>
									<?php echo anchor('/tanda_tangan/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Refresh Data Tanda Tangan">
											Refresh</button>'); ?>
								</div>
							</div>
							
							
							<?php echo form_open("tanda_tangan/result");?>
							
							<div class="col-md-3 col-sm-4 col-xs-4">
								<input type="text" class="form-control" name="key" placeholder="Cari">
							</div>
							
							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data User">
									<i class="fa fa-search"></i>
								</button>
							</div>
							<?php echo form_close();?>
						</div>
					</div>
                      
					  
                    <div class="clearfix">
                    </div>  
                    <div id="dt_example" class="example_alt_pagination">
                        <br>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left" >
                        
                        <thead>
                          <tr>
                            <th style="width:5%">
                              No
                            </th>  
                            <th style="width:20%">
                              Nama
                            </th>
                            <th style="width:20%">
                              NIP
                            </th>
                            <th style="width:20%">
                              Golongan
                            </th>
							<th style="width:20%">
                              Jabatan
                            </th>
                            <th style="width:20%">
                              Kabupaten
                            </th>
                            <th style="width:16%">
                              Pilihan
                            </th>
                            
                          </tr>
                        </thead>
                        <tbody>
                        <?php
							
                            $no = $number + 1;
                            foreach ($tanda_tangan as $v) {


                        ?>    
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td>  
                            <td>
                              <?php echo $v->nama ?>
                            </td>
                            <td>
                              <?php echo $v->nip ?>
                            </td>
                            <td>
                              <?php echo $v->golongan?>
                            </td>
							<td>
                              <?php echo $v->jabatan?>
                            </td>
							<td>
                              <?php echo $v->nama_kabupaten?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan 
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu" role="menu">
                                <li><?php echo anchor("/tanda_tangan/detail?id=".$v->id_tanda_tangan, "Detail"); ?></li>
                                <li><?php echo anchor("/tanda_tangan/update?id=".$v->id_tanda_tangan, "Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("/tanda_tangan/delete?id=".$v->id_tanda_tangan, "Hapus", 
									"onclick=\"return confirm('Anda Yakin Akan Menghapus?')\"");
                                ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
						<div align="right"><?php echo $links?> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   

 