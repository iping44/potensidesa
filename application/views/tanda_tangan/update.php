<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Tanda Tangan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("tanda_tangan/update_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <input type="hidden" name="id_tanda_tangan" value="<?php echo $entry->id_tanda_tangan?>">
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nama</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama" 
							placeholder="Nama" maxlength="100" value="<?php echo $entry->nama?>" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">NIP</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nip" 
							placeholder="NIP" maxlength="100" value="<?php echo $entry->nip?>" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Golongan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="golongan" 
							placeholder="Golongan" maxlength="100" value="<?php echo $entry->golongan?>" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Jabatan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="jabatan" 
							placeholder="Jabatan" maxlength="100" value="<?php echo $entry->jabatan?>" required>
                        </div>
                      </div>
                      
					  <div class="form-group">
                        <label class="col-md-2 control-label">Kabupaten</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control" name="id_kabupaten" required>
								
								
								<?php
								if($id_kabupaten==1){?>
									<option value="">
									- Kabupaten -
									</option>
									<?php
									foreach ($kabupaten as $cb) {
									if ($cb->id_kabupaten == $entry->id_kabupaten) {
										echo "<option value='$cb->id_kabupaten' selected>
											  $cb->nama_kabupaten</option>";
									} else {
										echo "<option value='$cb->id_kabupaten'>
											  $cb->nama_kabupaten</option>";
									}
									}
								} else {
									foreach ($kabupaten as $cb) {
									if ($cb->id_kabupaten == $entry->id_kabupaten) {
										echo "<option value='$cb->id_kabupaten' selected>
											  $cb->nama_kabupaten</option>";
									} else {
										echo "<option value='$cb->id_kabupaten'>
											  $cb->nama_kabupaten</option>";
									}
                                }
								}
								
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/tanda_tangan/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data User">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   