<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Tanda Tangan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("tanda_tangan/update_data", 
                            "class='form-horizontal' row-border")?> 
                      
                      <input type="hidden" name="id_tanda_tangan" value="<?php echo $entry->id_tanda_tangan?>">
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nama</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama" 
							placeholder="Nama" maxlength="100" value="<?php echo $entry->nama?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">NIP</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nip" 
							placeholder="NIP" maxlength="100" value="<?php echo $entry->nip?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Golongan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="golongan" 
							placeholder="Golongan" maxlength="100" value="<?php echo $entry->golongan?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Jabatan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="jabatan" 
							placeholder="Jabatan" maxlength="100" value="<?php echo $entry->jabatan?>" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Kabupaten</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="kabupaten" 
							placeholder="Kabupaten" maxlength="100" value="<?php echo $entry->nama_kabupaten?>" readonly>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
						  <?php echo anchor('/tanda_tangan/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Tanda Tangan">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   