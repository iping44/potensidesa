<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Profil</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><i class="fa fa-long-arrow-right"></i><span>Profil</span></div>
      </div>
    </div>
  </div>
</div>  
</section>



<!--Welcome-->
<section id="welcome" class="padding">
  <div class="container">
    <div class="row">
      <div class="">
		<?php foreach($about as $ttg){ ?>
         <h2 class="heading"><?php echo $ttg->judul;?></h2>
         <hr class="heading_space">
      </div>
      <div class="col-md-7 col-sm-6">
        <p class="half_space"><?php echo $ttg->isi;?></p>
       
		 <?php } ?>

      </div>
      <div class="col-md-5 col-sm-6">
       <img class="img-responsive" src="<?php echo base_url();?>files/image_struktur/<?php echo $ttg->gambar;?>" alt="welcome medix">
      </div>
    </div>
  </div>
</section> 


<!-- Struktur
<section id="specialists" class="bg_grey padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <h2 class="heading">Struktur RSUD Kota Kendari</h2>
      <hr class="heading_space">
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="slider_wrap">
          <div id="our-specialist" class="owl-carousel">
		  
			<?php foreach($struktur as $str){ ?>
            <div class="item">
              <div class="specialist_wrap">
               <img src="<?php echo base_url();?>files/image_struktur/<?php echo $str->gambar;?>" width="10" height="308" alt="Docotor">
               <h3><?php echo $str->nama;?></h3>
               <small><?php echo $str->jabatan;?></small>
               <p><?php echo $str->deskripsi;?></p>
              </div>
            </div>
			<?php } ?>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
 -->




