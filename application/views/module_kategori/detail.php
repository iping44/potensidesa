<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="left-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Jenis kategori</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("", 
                            "class='form-horizontal' row-border")?> 
                      
                      <input type="hidden" name="type_id" value="<?php echo $entry->id_kategori?>">
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Name Jenis Kategori</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_jenis" 
                                 value="<?php echo $entry->nama_jenis?>" readonly="">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="keterangan" 
                                 value="<?php echo $entry->keterangan?>" readonly="">
                        </div>
                      </div>
                      
                      
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   <?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

