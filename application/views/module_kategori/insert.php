<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="left-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Kategori Kategori</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("kategori/create_data", 
                            "class='form-horizontal' row-border")?> 
                      <div class="form-group">
                
                        <label class="col-md-2 control-label">Kategori Perizinan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_jenis" placeholder="Kategori Perizinan">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
                            <textarea name="keterangan" placeholder="Keterangan" class="form-control"></textarea>
                          
                        </div>
                      </div>
                      
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   