<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Halaman</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><i class="fa fa-long-arrow-right"></i><a href="#">Halaman</a><i class="fa fa-long-arrow-right"></i><span>Layanan</span></div>
      </div>
    </div>
  </div>
</div>  
</section>

<section id="service" class="padding">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
      <h2 class="heading">Layanan</h2>
      <hr class="heading_space">
      <div class="service_wrapper">
      <ul class="items">
        
		<?php foreach($layanan as $lay){ ?>
        <li>
          <a href="#"><?php echo $lay->jenis_layanan;?></a>
          <ul class="sub-items bg_grey">
            <li>
              <div class="row">
                <div class="col-md-12 accordion-text">
                  <h4><?php echo $lay->judul;?></h4>
                  <p class="half_space"><?php echo $lay->deskripsi;?></p>
                  
                </div>
              </div>
            </li>
          </ul>
        </li>
        <?php } ?>       
        
      </ul>
    </div>
      </div>
    </div>
  </div>
</section>