<section class="toggler">
<h3 class="hidden">hidden</h3>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        
      </div>
    </div>
  </div>
</section>



<!--Blue Section-->
<section class="bg_blue padding-half">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4 white_content">
        <i class="fa fa-search"></i>
        <h3>Penelitian</a></h3>
        <p> Melakukan Penelitian</p>
      </div>
      <div class="col-md-4 col-sm-4 white_content">
        <i class="fa fa-file"></i>
        <h3>Pengembangan</a></h3>
        <p> Melakukan Pengembangan</p>
      </div>
      <div class="col-md-4 col-sm-4 white_content">
        <i class="fa fa-hand-paper-o"></i>
        <h3>Pembinaan</a></h3>
        <p> Melakukan Pembinaan. </p>
      </div>
    </div>
  </div>
</section>


<!--Services plus working hours-->
<section class="padding" id="services">
  <div class="container">
    <div class="row">
      <div class="col-md-8">
         <h2 class="heading">Link</h2>
         <hr class="heading_space">
         <div class="slider_wrap">
        <div id="service-slider" class="owl-carousel">
		
		<?php foreach($fasilitas as $fas){ ?>
          <div class="item">
            <div class="item_inner">
            <div class="image">
              <img src="<?php echo base_url();?>files/image_struktur/<?php echo $fas->gambar;?>" 
			  width="30px" height="300px" alt="Services Image">
              <a href="<?php echo $fas->deskripsi?>" target="_blank" class="overlay"></a>
            </div>
              <a href="<?php echo $fas->deskripsi?>" target="_blank"><h3><?php echo $fas->judul;?></a></h3>
            </div>
          </div>
		  <?php } ?>
          
        </div>
        </div>
      </div>
      <div class="col-md-4">
        <h2 class="heading">Jam Kerja</h2>
        <hr class="heading_space">
        <ul class="hours_wigdet">
			<?php foreach($jamker as $jam){ ?>
          <li>Senin<span><?php echo $jam->senin;?></span></li>
          <li>Selasa<span><?php echo $jam->selasa;?></span></li>
          <li>Rabu<span><?php echo $jam->rabu;?></span></li>
          <li>Kamis<span><?php echo $jam->kamis;?></span></li>
          <li>Jum'at<span><?php echo $jam->jumat;?></span></li>
		  <?php } ?>
        </ul>
      </div>
    </div>
  </div>
</section>