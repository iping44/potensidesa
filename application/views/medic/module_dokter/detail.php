<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Dokter</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Dokter</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_dokter" 
							value="<?php echo $entry->nama_dokter?>" maxlength="100" readonly>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Spesialisasi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="divisi" 
							value="<?php echo $entry->divisi?>" maxlength="100" readonly>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Deskripsi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="deskripsi" 
							value="<?php echo $entry->deskripsi?>" maxlength="100" readonly>
                        </div>
					   </div>
					    <div class="form-group">
							  <label class="col-md-2 control-label" >gambar</label>
							  <div class="col-md-6">
								  <?php     
				  
								$image = array(
								  'src' => '/files/image_struktur/'.($entry->gambar),
								  'class' => 'photo',
								  'width' => '200',
								  'height' => '200',
								);

								echo img($image); ?>
							  </div>
							</div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">FB</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="fb" 
							value="<?php echo $entry->fb?>" maxlength="100" readonly>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Twitter</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="twitter" 
							value="<?php echo $entry->twitter?>" maxlength="100" readonly>
                        </div>
					   </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Instagram</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="instagram" 
							value="<?php echo $entry->instagram?>" maxlength="100" readonly>
                        </div>
					   </div>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <?php echo anchor('/dokter/data', 
									'<button type="button" class="btn btn-success 
										" data-toggle="tooltip" data-placement="top" title="Kembali ke Data dokter">
											Kembali</button>'); ?>
                        </div>
						
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   