<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Desa</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("desa/update_data",
                            "class='form-horizontal' row-border")?>
                       <input type="hidden" name="desa_id" value="<?php echo $entry->desa_id?>">
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Desa</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_desa"
                            value="<?php echo $entry->nama_desa?>"  maxlength="100" readonly="">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="jenis">
                                  <?php
                                  if($entry->jenis=="Desa") {
                                      echo "
                                          <option value='Desa' selected> Desa</option>
                                          <option value='Kelurahan'>Kelurahan</option>"
                                      ;
                                  } else if($entry->jenis=="T") {
                                      echo "
                                          <option value='Desa' > Desa</option>
                                          <option value='Kelurahan' selected>Kelurahan</option>"
                                      ;
                                  }
                                  ?>
                              </select>
                                </div>
                            </div>
                        </div>
                      </div>
					                 <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Kecamatan</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="kec_id">

                                  <?php
                                  foreach ($kecamatan as $cb) {
                                    if($cb->kec_id == $entry->kec_id) {
                                      echo "<option value='$cb->kec_id' selected>
                                      $cb->nama_kecamatan</option>";
                                    } else {
                                      echo "<option value='$cb->kec_id'>
                                      $cb->nama_kecamatan</option>";
                                    }
                                  }?>
                              </select>
                                </div>
                            </div>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Luas</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="luas"
                          value="<?php echo $entry->luas?>" readonly="">
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
						  <textarea name="keterangan" class="textarea form-control" readonly=""><?php echo $entry->keterangan?></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/desa/data',
									'<button type="button" class="btn btn-success
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Desa">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
