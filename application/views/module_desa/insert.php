<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Tambah Desa</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-rocket"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("desa/create_data",
                            "class='form-horizontal' row-border")?>
                               
                      <div class="form-group">
                        <label class="col-md-2 control-label">Nama Desa</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nama_desa" placeholder="Nama Desa"
							 maxlength="100" required>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="jenis">

                                <option value="Desa">
                                  Desa
                                </option>
                                <option value="Kelurahan">
                                  Kelurahan
                                </option>
                              </select>
                                </div>
                            </div>
                        </div>
                      </div>
					                 <div class="form-group">
                        <label class="col-sm-2 control-label">Nama Kecamatan</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select id="active" class="form-control" name="kec_id">

                  									<option>
                                                    - Nama Kecamatan -
                  									</option>
                  									<?php
                  									foreach ($kecamatan as $cb) {
                  										echo "<option value='$cb->kec_id'>
                  									  $cb->nama_kecamatan</option>";
                  									}

                                                  ?>
                              </select>
                                </div>
                            </div>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Luas</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="luas" placeholder="Luas">
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Keterangan</label>
                        <div class="col-md-6">
						  <textarea name="keterangan" class="textarea form-control"></textarea>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
						  <?php echo anchor('/menu/data',
									'<button type="button" class="btn btn-success
										" data-toggle="tooltip" data-placement="top" title="Kembali Data Menu">
											Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
