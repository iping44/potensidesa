<body>
<!--Loader-->
<div class="loader">
  <div class="loader__figure"></div>
  <p class="loader__label"><img src="<?php echo base_url();?>images/kdi.png" alt="logo"></p>
</div>

<!--Topbar-->
<div class="topbar">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <p class="pull-left hidden-xs">Badan Penelitian & Pengembangan Provinsi Sulawesi Tenggara</p>
        <p class="pull-right"><i class="fa fa-phone"></i>Kontak Line (0401) 3008846</p>
      </div>
    </div>
  </div>
</div>

<!--Header-->
<header id="main-navigation">
  <div id="navigation" data-spy="affix" data-offset-top="20">
    <div class="container">
      <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-default">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar" aria-expanded="false"> 
            <span class="icon-bar top-bar"></span> <span class="icon-bar middle-bar"></span> <span class="icon-bar bottom-bar"></span> 
            </button>
           <a class="navbar-brand" href="<?php echo base_url();?>home/beranda"><img src="<?php echo base_url();?>images/rsud.png" alt="logo" class="img-responsive"></a> 
		  
         </div>
        
            <div id="fixed-collapse-navbar" class="navbar-collapse collapse navbar-right">
              <ul class="nav navbar-nav">               
				
				<li><a href="<?php echo base_url();?>home/beranda">Beranda</a></li>
				
				<li><a href="<?php echo base_url();?>home/tentang">Profil</a></li>  
				<li><a href="<?php echo base_url();?>home/agenda">Agenda</a></li>
				<li><a href="<?php echo base_url();?>home/info">Berita</a></li>
				<li><a href="<?php echo base_url();?>home/litbang">Litbang</a></li>
				<li><a href="<?php echo base_url();?>home/jasakip">JASAKIP</a></li> 		 
				<li><a href="<?php echo base_url();?>home/galeri">Galeri</a></li> 		 
                <li><a href="<?php echo base_url();?>home/hubungi">Kontak</a></li>
              </ul>
            </div>
         </nav>
         </div>
       </div>
     </div>
  </div>
</header>
