<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Pekerjaan</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">

                      <?php echo form_open("pekerjaan/update_data",
                            "class='form-horizontal' row-border")?>

                      <input type="hidden" name="desa_id" value="<?php echo $entry->desa_id?>">
          					  <input type="hidden" name="pekerjaan_id" value="<?php echo $entry->pekerjaan_id?>">

                      <div class="form-group">
                        <!-- <label class="col-sm-2 control-label">Id pekerjaan</label> -->
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="hidden" name="pekerjaan_id" value="<?php echo $entry->pekerjaan_id?>" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jenis Pekerjaan</label>
                        <div class="col-sm-10">
                            <div class="row">
                              <div class="col-md-4 col-sm-4 col-xs-4">
                                <select name="jenis_pekerjaan_id" class="form-control" >
                                  <?php
                                    foreach($data as $f){
                                      if($entry->jenis_pekerjaan_id == $f->jenis_pekerjaan_id){
                                      echo "<option value='$f->jenis_pekerjaan_id' selected>$f->jenis_pekerjaan</option>";
                                    }else{
                                      echo "<option value='$f->jenis_pekerjaan_id' >$f->jenis_pekerjaan</option>";
                                    }
                                    }
                                  ?>
                                </select>
                            </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Jumlah</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="jumlah" value="<?php echo $entry->jumlah?>" maxlength="100" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="col-sm-2 control-label">Tahun</label>
                        <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                  <input class="form-control" type="text" name="tahun" value="<?php echo $entry->tahun?>" maxlength="4" required>
                                </div>
                            </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" id="confirm">Simpan</button>
            						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
            						  <?php echo anchor('/pekerjaan/data?id='.$entry->desa_id,
                          '<button type="button" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Kembali Data Group">Kembali</button>'); ?>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>
</div>
