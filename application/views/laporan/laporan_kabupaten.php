<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Laporan Perdagangan Antar Pulau Kabupaten</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-plus"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("cetak/cetak", 
                            "class='form-horizontal' row-border")?> 
                      
					  <div class="form-group">
                        <label class="col-md-2 control-label">Kabupaten</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" required>
								
                                <?php
								if($group_id=="group1000"){?>
									<option value="">
									- Kabupaten -
									</option>
									<?php
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->nama_kabupaten'>
									$cb->nama_kabupaten</option>";
									}
								} else {
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->nama_kabupaten' selected>
									$cb->nama_kabupaten</option>";
									}
								}
								
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Bulan</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control" name="bulan" required>
								<option value="">- Pilih Bulan -</option>
								<option value="1">Januari</option>
								<option value="2">Februari</option>
								<option value="3">Maret</option>
								<option value="4">April</option>
								<option value="5">Mei</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">Agustus</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					   <div class="form-group">
                        <label class="col-md-2 control-label">Tahun</label>
                        <div class="col-md-2">
                          <select class="form-control selectpicker" data-live-search="true" name="tahun" required>
										<option value="">- Tahun -</option>
										<?php
										$thn = date("Y")+ 2;
											for($i=2016;$i<=$thn;$i++){
											  echo "<option value='".$i."'>".$i."</option>";
											}
										?>
						  </select>
                        </div>
					  </div>
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" >Cetak</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   