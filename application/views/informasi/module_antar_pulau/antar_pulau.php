<script>
        $(document).ready(function(){
			var id_kabupaten = $("#id_kabupaten").val();
			var id_pelabuhan = $("#id_pelabuhan").val();
            $.ajax({
				type : "POST",
				url  : "<?php echo base_url(); ?>antar_pulau/get",
				data : {id_kabupaten: id_kabupaten, id_pelabuhan: id_pelabuhan}, 
				success: function (data){
					$("#id_pelabuhan").html(data)
				}
            })
            $("#id_kabupaten").change(function (){
                var id_kabupaten = $("#id_kabupaten").val();
                $.ajax({
                 type : "POST",
                 url  : "<?php echo base_url(); ?>antar_pulau/get",
                 data : "id_kabupaten=" + id_kabupaten,
                 success: function (data){
                       $("#id_pelabuhan").html(data)
                 }
                })
            })	
        });
</script>
<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Perdagangan Antar Pulau</a>
                    </div>
					<span class="tools">
                      <i class="fa fa-list-ol"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/antar_pulau/insert', 
									'<button type="button" class="btn btn-info 
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Bahan Strategis">
											<i class="fa fa-plus"></i> Tambah</button>'); ?>
								</div>
							</div><br><br><br>
							
							<?php
									$message = $this->session->flashdata('notif');
									$message2 = $this->session->flashdata('notif2');
									
									if($message){
										echo '<p class="alert alert-success text-center">'.$message .'</p>';
									}else if($message2){
										echo '<p class="alert alert-danger text-center">'.$message2 .'</p>';
									}
							?>
							
					<div class="col-sm-12">
						<div class=row>
							
							<?php echo form_open("antar_pulau/result");?>
							<div class="col-lg-2 col-md-2 col-sm-6 col-sx-12"></div>
							<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
									  <select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" id="id_kabupaten" required>
										<?php
										if($group_id=="group1000"){?>
											<option value="">
											- Kabupaten -
											</option>
											<?php
											foreach ($kabupaten as $cb) {
											echo "<option value='$cb->id_kabupaten'>
											$cb->nama_kabupaten</option>";
											}
										} else {
											foreach ($kabupaten as $cb) {
											echo "<option value='$cb->id_kabupaten' selected>
											$cb->nama_kabupaten</option>";
											}
										}
										
										?>
                              </select>
								  </div>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-4">
								<div class="form-group">
								  <select class="form-control" name="id_pelabuhan" id="id_pelabuhan" required>
									<option value="" hidden>- Pelabuhan -</option>
									<?php
											foreach ($pelabuhan as $l) {
													echo "<option value='$l->id_pelabuhan'>$l->nama_pelabuhan</option>";
											}
									?>
								  </select>
								</div>
							</div>
							<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
									<div class='input-group date' id='datetimepicker1'>
										<input class="form-control" type="text"  name="tanggal" placeholder="Tanggal" id="datetimepicker1" maxlength="100" required>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
							  </div>	
							</div>
							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data User">
									<i class="fa fa-refresh"></i> Proses
								</button>
							</div>
							<?php echo form_close();?>
							
						</div>

					</div>
							
						</div>
					</div>
                    <div class="clearfix">
                    </div> 
                      
                    <div class="container">
                        <br>
                     <div class="clearfix">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   