<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Perdagangan Antar Pulau</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("antar_pulau/result", 
                            "class='form-horizontal' row-border")?> 
                      
                      <div class="form-group">
                        <label class="col-md-2 control-label">Komoditi Tetap</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control" name="id_komoditi_tetap" readonly>
								<option value="">
									- Komoditi Tetap -
								</option>
                                <?php
								foreach ($komoditi_tetap as $cb) {
									if ($cb->id_komoditi_tetap == $entry->id_komoditi_tetap) {
										echo "<option value='$cb->id_komoditi_tetap' selected>
											  $cb->nama_komoditi_tetap</option>";
									} else {
										echo "<option value='$cb->id_komoditi_tetap'>
											  $cb->nama_komoditi_tetap</option>";
									}
                                }
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="col-md-2 control-label">Kabupaten</label>
                          <div class="col-sm-10">
                            <div class="row">
                                <div class="col-md-4 col-sm-4 col-xs-4">
                                <select class="form-control" name="id_kabupaten" readonly>
                                <?php
								if($group_id=="group1000"){?>
									<option value="">
									- Kabupaten -
									</option>
									<?php
									foreach ($kabupaten as $cb) {
									if ($cb->id_kabupaten == $entry->id_kabupaten) {
										echo "<option value='$cb->id_kabupaten' selected>
											  $cb->nama_kabupaten</option>";
									} else {
										echo "<option value='$cb->id_kabupaten'>
											  $cb->nama_kabupaten</option>";
									}
									}
								} else {
									foreach ($kabupaten as $cb) {
                                    echo "<option value='$cb->id_kabupaten' selected>
									$cb->nama_kabupaten</option>";
									}
								}
								
                                ?>
                              </select>
                                </div>
                            </div>  
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="tanggal" 
							value="<?php echo $entry->tanggal?>" maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Volume</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="volume" 
							value="<?php echo $entry->volume?>" maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nilai per Satuan</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="nilai_persatuan" 
							value="<?php echo $entry->nilai_persatuan?>" maxlength="100" readonly>
                        </div>
                      </div>
                     <div class="form-group">
							<input class="form-control" type="hidden" name="id_kabupaten" value="<?php echo $entry->id_kabupaten;?>" maxlength="100" readonly>
							<input class="form-control" type="hidden" name="id_pelabuhan" value="<?php echo $entry->id_pelabuhan;?>" maxlength="100" readonly>
						  	
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-success" title="Kembali">
									Kembali
								</button>
							</div>
                      
					
						
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   