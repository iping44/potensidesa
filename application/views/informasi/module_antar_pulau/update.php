<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Edit Perdagangan Antar Pulau</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-edit"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("antar_pulau/update_data", 
                            "class='form-horizontal' row-border")?> 
                      <input type="hidden" name="id_antar_pulau" value="<?php echo $entry->id_antar_pulau?>">
					  <input class="form-control" type="hidden" name="id_kabupaten" value="<?php echo $entry->id_kabupaten;?>" maxlength="100" readonly>
					  <input class="form-control" type="hidden" name="id_pelabuhan" value="<?php echo $entry->id_pelabuhan;?>" maxlength="100" readonly>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Komoditi Tetap</label>
                        <div class="col-md-3">
                          <input class="form-control" type="text" name="" 
							value="<?php echo $entry->nama_komoditi_tetap?>" maxlength="100" readonly>
                        </div>
                      </div>
					  
					  <div class="form-group">
                        <label class="col-md-2 control-label">Kabupaten</label>
                        <div class="col-md-3">
                          <input class="form-control" type="text" name="" 
							value="<?php echo $entry->nama_kabupaten?>" maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Pelabuhan</label>
                        <div class="col-md-3">
                          <input class="form-control" type="text" name="" 
							value="<?php echo $entry->nama_pelabuhan?>" maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal</label>
                        <div class="col-md-3">
                          <input class="form-control" type="text" name="tanggal" 
							value="<?php echo $entry->tanggal?>" maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Volume</label>
                        <div class="col-md-3">
                          <input class="form-control" type="number" min='0'  name="volume" 
							value="<?php echo $entry->volume?>" maxlength="100" required>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Nilai per Satuan</label>
                        <div class="col-md-3">
                          <input class="form-control" type="number" min='0' name="nilai_persatuan" 
							value="<?php echo $entry->nilai_persatuan?>" maxlength="100" required>
                        </div>
                      </div>
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-info" title="Update Data">Update</button>
						  <button type="reset" class="btn btn-danger" title="Mengembalikan Data">Reset</button>
                        </div>
                      </div>
                   <?php echo form_close();?>
					 <?php echo form_open("antar_pulau/result", 
                            "class='form-horizontal' row-border")?> 
					  <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <input class="form-control" type="hidden" name="id_kabupaten" value="<?php echo $entry->id_kabupaten;?>" maxlength="100" readonly>
                          <input class="form-control" type="hidden" name="id_pelabuhan" value="<?php echo $entry->id_pelabuhan;?>" maxlength="100" readonly>
						  <input class="form-control" type="hidden" name="tanggal" value="<?php echo $entry->tanggal;?>" maxlength="100" readonly>
						  <button type="submit" class="btn btn-success" title="Kembali">Kembali</button>
					 	</div>
                      </div>  
						  
					 <?php echo form_close();?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   