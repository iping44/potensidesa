<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="inputs">Detail Bahan Strategis</a>
                    </div>
                    <span class="tools">
                      <i class="fa fa-book"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      
                      <?php echo form_open("strategis/result", 
                            "class='form-horizontal' row-border")?> 
                       <input class="form-control" type="hidden" name="id_kabupaten" value="<?php echo $entry->id_kabupaten;?>" maxlength="100" readonly>
					  <input class="form-control" type="hidden" name="id_pasar" value="<?php echo $entry->id_pasar;?>" maxlength="100" readonly>
                     <div class="form-group">
                        <label class="col-md-2 control-label">Komoditi</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="" 
							value="<?php echo $entry->nama_komoditi?>" maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Pasar</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="tanggal" 
							value="<?php echo $entry->nama_pasar?>" maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="tanggal" 
							value="<?php echo $entry->tanggal?>" maxlength="100" readonly>
                        </div>
                      </div>
					  <div class="form-group">
                        <label class="col-md-2 control-label">Harga</label>
                        <div class="col-md-6">
                          <input class="form-control" type="text" name="harga" 
							value="<?php echo $entry->harga?>" maxlength="100" readonly>
                        </div>
                      </div>
                      <div class="form-group">
							<input class="form-control" type="hidden" name="id_kabupaten" value="<?php echo $entry->id_kabupaten;?>" maxlength="100" readonly>
						  	
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-success" title="Kembali">
									Kembali
								</button>
							</div>
                      
                      </div>
                    <?php form_close()?>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   