<script>
        $(document).ready(function(){
			var id_kabupaten = $("#id_kabupaten").val();
			var id_pasar = $("#id_pasar").val();
            $.ajax({
				type : "POST",
				url  : "<?php echo base_url(); ?>strategis/get",
				data : {id_kabupaten: id_kabupaten, id_pasar: id_pasar}, 
				success: function (data){
					$("#id_pasar").html(data)
				}
            })
            $("#id_kabupaten").change(function (){
                var id_kabupaten = $("#id_kabupaten").val();
                $.ajax({
                 type : "POST",
                 url  : "<?php echo base_url(); ?>strategis/get",
                 data : "id_kabupaten=" + id_kabupaten,
                 success: function (data){
                       $("#id_pasar").html(data)
                 }
                })
            })	
        });
</script>
<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <!-- Left Sidebar Start -->
          <div class="center-sidebar">
      <!-- Row Start -->
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <div class="widget">
                  <div class="widget-header">
                    <div class="title">
                      <a id="dynamic-tables">Data Bahan Strategis</a>
                    </div>
					<span class="tools">
                      <i class="fa fa-list-ol"></i>
                    </span>
                  </div>
                  <div class="widget-body">
                      <div class="col-sm-12">
						<div class=row>
							<div class="col-md-8 col-sm-4 col-xs-4">
								<div class="btn-group">
									<?php echo anchor('/strategis/insert', 
									'<button type="button" class="btn btn-info 
										" data-toggle="tooltip" data-placement="top" title="Tambah Data Bahan Strategis">
											<i class="fa fa-plus"></i> Tambah</button>'); ?>
								</div>
							</div><br><br><br>
							
					<div class="col-sm-12">
						<div class=row>
							
							<?php echo form_open("strategis/result");?>
							<div class="col-lg-2 col-md-2 col-sm-6 col-sx-12"></div>
							<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
									  <select class="form-control selectpicker" data-live-search="true" name="id_kabupaten" id="id_kabupaten" required>
										<?php
										if($group_id=="group1000"){?>
											<option value="">
											- Kabupaten -
											</option>
											<?php
											foreach ($kabupaten as $cb) {
											echo "<option value='$cb->id_kabupaten'>
											$cb->nama_kabupaten</option>";
											}
										} else {
											foreach ($kabupaten as $cb) {
											echo "<option value='$cb->id_kabupaten' selected>
											$cb->nama_kabupaten</option>";
											}
										}
										
										?>
                              </select>
								  </div>
							</div>
							<div class="col-md-3 col-sm-4 col-xs-4">
								<div class="form-group">
								  <select class="form-control" name="id_pasar" id="id_pasar" required>
									<option value="" hidden>- Pasar -</option>
									<?php
											foreach ($pasar as $l) {
													echo "<option value='$l->id_pasar'>$l->nama_pasar</option>";
											}
									?>
								  </select>
								</div>
							</div>
							
							<div class="col-md-2 col-sm-4 col-xs-4">
								<div class="form-group">
									<div class='input-group date' id='datetimepicker1'>
										<input class="form-control" type="text"  name="tanggal" placeholder="Tanggal" id="datetimepicker1" maxlength="100" required>
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar"></span>
										</span>
									</div>
							  </div>	
							</div>
							<div class="btn-group">
								<button type="submit" class="btn btn-info" title="Cari Data User">
									<i class="fa fa-refresh"></i> Proses
								</button>
							</div>
							<?php echo form_close();?>
							
						</div>

					</div>
							
						</div>
					</div>
                    <div class="clearfix">
                    </div> 
                      
                    <div class="container">
                        <br>
						<h4><center> <?php echo $tempat2->nama_pasar?> Tanggal <?php  $tanggal = date('d-m-Y', strtotime($tanggal ));
							  echo $tanggal; ?></center></h4>
                      <table class="table table-condensed table-striped table-hover table-bordered pull-left">
                        
                        <thead>
                          <tr>
                            <th style="width:2%">
                              No
                            </th>
							<th style="width:25%">
                              Nama Komoditi
                            </th>
							<th style="width:15%">
                              Nama Pasar
                            </th>
                            <th style="width:15%">
                              Tanggal
                            </th>
							<th style="width:15%">
                              Harga
                            </th>
                            <th style="width:1%">
                              Pilihan
                            </th>
                          </tr>
                        </thead>
                        <tbody>
                        <?php
                            $no = $number + 1;
                            foreach ($strategis as $v) {
                        ?>    
                          <tr class="gradeA success">
                            <td>
                              <?php echo $no ?>
                            </td> 
							<td>
                              <?php echo $v->nama_komoditi ?>
                            </td>							
                            <td>
                              <?php echo $v->nama_pasar ?>
                            </td>
							<td>
                              <?php 
							  $tanggal = date('d-m-Y', strtotime($v->tanggal ));
							  echo $tanggal; ?>
                            </td>
							<td>
                               <?php 
							  echo number_format($v->harga,0,",",".");
							    ?>
                            </td>
                            <td class="hidden-xs">
                            <div class="btn-group">
                              <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">Pilihan 
                                  <span class="caret"></span></button>
                              <ul class="dropdown-menu pull-right" role="menu">
                                <li><?php echo anchor("/strategis/detail?id=".$v->id_harga_barang, "<i class='fa fa-check-square-o'></i> Detail"); ?></li>
                                <li><?php echo anchor("/strategis/update?id=".$v->id_harga_barang, "<i class='fa fa-pencil-square-o'></i> Edit"); ?></li>
                                <li class="divider"></li>
                                <li><?php echo anchor("/strategis/delete?id=".$v->id_harga_barang."&&id_kabupaten=".$v->id_kabupaten."&&id_pasar=".$v->id_pasar."&&tanggal=".$v->tanggal," 
                                    <i class='fa fa-trash-o'></i> Hapus
                                ", "onclick=\"return confirm('Anda Yakin Akan Menghapus?')\"");
                                ?></li>
                              </ul>
                            </div>
                          </td>
                          </tr>
                          <?php
                                $no++;
                            }
                          ?>
                        </tbody>
                      </table>
                      <div class="clearfix">
                      </div>
					  <div align="right"><?php echo $links?> </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Row End -->
     </div>       
</div>   