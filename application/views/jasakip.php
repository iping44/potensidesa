<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Rencana Kerja, Rencana Strategi dan Laporan Kinerja SKPD</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><span><i class="fa fa-long-arrow-right"></i>JASAKIP</span></div>
      </div>
    </div>
  </div>
</div>  
</section>


<!-- Blogs -->
<section id="blog" class="padding-top">
  <div class="container">
    <div class="row">
	<div class="col-md-4 col-sm-5">
        <aside class="sidebar">
           <div class="widget">
             <h3>JASAKIP</h3>
             <ul class="widget_links">
				<?php
					 foreach ($jenisjasakip as $cb) {
                                
				?>
					<li><a href="<?php echo base_url().'home/jasakip2/'.$cb->id_jenisjasakip;?>">
					<?php echo $cb->nama_jenisjasakip?></a></li>
               <?php
					}
			   ?>
               </ul>
           </div>
		   <div class="widget">
             
             <?php
			 foreach ($banner as $b) {
			 
						  $image = array(
							  'src' => '/files/banner/'.($b->download),
							  'class' => 'photo',
							  'width' => '320',
							  'height' => '320',
							);

							echo img($image);
							echo "<br>";
			}							?>
           </div>
           </aside>
    </div>
      <div class="col-md-8 col-sm-7">
		
		<?php 
		
		foreach($lit as $inf){ ?>
        <div class="blog_item padding-bottom">
           <h2><?php echo $inf->judul;?></h2>
           <ul class="comments">
             <li><a href="<?php echo base_url()."files/uploads/".$inf->download?>" target="_blank">
			 <i class="fa fa-book"></i>Download</a></li>
             
           </ul>
          <br><br>
		 <?php } ?>
        
      </div>
	  
    </div>
  </div>
</section>
