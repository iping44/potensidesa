<!--Page header & Title-->
<section id="page_header">
<div class="page_title">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
         <h2 class="title">Agenda</h2>
         <div class="page_link"><a href="<?php echo base_url();?>home/beranda">Beranda</a><span><i class="fa fa-long-arrow-right"></i>Agenda</span></div>
      </div>
    </div>
  </div>
</div>  
</section>


<!-- Blogs -->
<section id="blog" class="padding-top">
  <div class="container">
    <div class="row">
	<div class="col-md-4 col-sm-5">
        <aside class="sidebar">
           <div class="widget">
             <h3>Kategori</h3>
             <ul class="widget_links">
				<?php
					 foreach ($jenisagenda as $cb) {
                                
				?>
					<li><a href="<?php echo base_url().'home/agenda2/'.$cb->id_jenisagenda;?>">
					<?php echo $cb->nama_jenisagenda?></a></li>
               <?php
					}
			   ?>
               </ul>
           </div>
           </aside>
    </div>
	<div class="col-md-8 col-sm-7">
		
		<?php 
		$no=1;
		foreach($lit as $inf){ ?>
        <div class="blog_item padding-bottom">
           <h2><?php echo $inf->judul;?></h2>
           <ul class="comments">
             <li><a href="#"><i class="fa fa-clock-o"></i><?php echo $inf->tanggal;?></a></li>
             <li><a href="#"><i class="fa fa-building"></i><?php echo $inf->tempat;?></a></li>
			 <?php
				if($inf->download=="") {
			
				} else {
			?>
				<li><a href="<?php echo base_url()."files/uploads/".$inf->download?>" target="_blank">
				<i class="fa fa-file"></i>File</a></li>
			<?php	
			
				}
			 ?>
			 
			 
           </ul>
          
          <p><?php $content = $inf->kegiatan;
			$content=character_limiter($content,1000);?>
			<?php echo $content;?></p>
          
		  
		  <?php $no++; } ?>
        
      </div>
	  
    </div>
  </div>

	</div>
</section>
